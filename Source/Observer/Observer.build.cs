
using UnrealBuildTool;

public class Observer : ModuleRules
{
	public Observer(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "UMG", "UnrealEd"});
	}
}
