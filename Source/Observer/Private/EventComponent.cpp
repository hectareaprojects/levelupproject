// TODO: Copyright

#include "EventComponent.h"
#include "PrimitiveSceneProxy.h"
#include "SceneManagement.h"

FPrimitiveSceneProxy* UEventComponentBase::CreateSceneProxy()
{
	/** Represents a link to the scene manager. */
	class FEventSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		FEventSceneProxy(const UEventComponentBase* InComponent, const FColor& InColor)
			: FPrimitiveSceneProxy(InComponent)
			, Color(InColor)
		{
			bWillEverBeLit = false;
		}

		virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
		{
			QUICK_SCOPE_CYCLE_COUNTER(STAT_BoxSceneProxy_GetDynamicMeshElements);

			for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
			{
				if (VisibilityMap & (1 << ViewIndex))
				{
					FMatrix					 LocalToWorld = GetLocalToWorld();
					const FSceneView*		 View = Views[ViewIndex];
					FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);
					DrawRectangle(PDI, LocalToWorld.GetOrigin(), View->GetViewRight(), View->GetViewUp(), Color, 10.f, 10.f, SDPG_World, 2.f);
				}
			}
		}

		virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
		{
			FPrimitiveViewRelevance Result;
			Result.bDrawRelevance = IsShown(View) && View->Family->EngineShowFlags.Editor; // Tener cuidado con View->Family->EngineShowFlags.Editor!!!!!
			Result.bDynamicRelevance = true;
			Result.bShadowRelevance = IsShadowCast(View);
			Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
			return Result;
		}

		virtual uint32 GetMemoryFootprint(void) const override { return (sizeof(*this) + GetAllocatedSize()); }
		uint32		   GetAllocatedSize(void) const { return (FPrimitiveSceneProxy::GetAllocatedSize()); }

	private:
		FColor Color;
	};

	return new FEventSceneProxy(this, ProxyColor);
}

FBoxSphereBounds UEventComponentBase::CalcBounds(const FTransform& LocalToWorld) const
{
	return FBoxSphereBounds(FSphere(GetComponentLocation(), 5.f));
}

// -------------------------------------------------------------------------------------------------------------

UEventComponent::UEventComponent()
{
	ProxyColor = FColor::White;
}

void UEventComponent::Notify()
{
	OnNotify.Broadcast();
}

// -------------------------------------------------------------------------------------------------------------

UBoolEventComponent::UBoolEventComponent()
{
	ProxyColor = FColor::Red;
}

void UBoolEventComponent::Notify(bool bValue)
{
	OnBoolNotify.Broadcast(bValue);
}
