// TODO: Copyright

#include "ObserverComponent.h"
#include "EventComponent.h"
#include "PrimitiveSceneProxy.h"
#include "SceneManagement.h"
#include "TimerManager.h"
#if WITH_EDITOR
	#include "Editor.h"
#endif

FPrimitiveSceneProxy* UObserverComponentBase::CreateSceneProxy()
{
	/** Represents a link to the scene manager. */
	class FObserverSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		FObserverSceneProxy(const UObserverComponentBase* InComponent, const FVector& InStart, const FVector& InEnd, bool bInDrawArrow, const FColor& InColor)
			: FPrimitiveSceneProxy(InComponent)
			, ArrowStart(InStart)
			, ArrowEnd(InEnd)
			, bDrawArrow(bInDrawArrow)
			, Color(InColor)
		{
			bWillEverBeLit = false;
		}

		void DrawLineArrow(FPrimitiveDrawInterface* PDI, const FVector& Start, const FVector& End, const FColor& ArrowColor, float Mag) const
		{
			FVector Dir = End - Start;
			const float DirMag = Dir.Size();
			Dir /= DirMag;
			FVector YAxis, ZAxis;
			Dir.FindBestAxisVectors(YAxis, ZAxis);
			FMatrix ArrowTM(Dir, YAxis, ZAxis, Start);
			DrawDirectionalArrow(PDI, ArrowTM, ArrowColor, DirMag, Mag, SDPG_World, 2.f);
			// DrawConnectedArrow(PDI, ArrowTM, Color, 2.f, 2.f, SDPG_World);
		}

		virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
		{
			QUICK_SCOPE_CYCLE_COUNTER(STAT_BoxSceneProxy_GetDynamicMeshElements);

			for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
			{
				if (VisibilityMap & (1 << ViewIndex))
				{
					FMatrix LocalToWorld = GetLocalToWorld();
					const FSceneView* View = Views[ViewIndex];
					FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);
					if (bDrawArrow)
						DrawLineArrow(PDI, ArrowStart, ArrowEnd, Color, 0.f);
					DrawCircle(PDI, LocalToWorld.GetOrigin(), View->GetViewRight(), View->GetViewUp(), Color, 6.f, 10, SDPG_World, 2.f);
				}
			}
		}

		virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
		{
			FPrimitiveViewRelevance Result;
			Result.bDrawRelevance = IsShown(View) && View->Family->EngineShowFlags.Editor; // Tener cuidado con View->Family->EngineShowFlags.Editor!!!!!
			Result.bDynamicRelevance = true;
			Result.bShadowRelevance = IsShadowCast(View);
			Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
			return Result;
		}
		virtual uint32 GetMemoryFootprint(void) const override { return (sizeof(*this) + GetAllocatedSize()); }
		uint32 GetAllocatedSize(void) const { return (FPrimitiveSceneProxy::GetAllocatedSize()); }

	private:
		FVector ArrowStart;
		FVector ArrowEnd;
		bool bDrawArrow;
		FColor Color;
	};

	if (EventComponentReferencePtr)
	{
		if (USceneComponent* EventComponent = Cast<USceneComponent>(EventComponentReferencePtr->GetComponent(EventComponentReferencePtr->OtherActor.Get())))
		{
			return new FObserverSceneProxy(this, EventComponent->GetComponentLocation(), GetComponentLocation(), true, ProxyColor);
		}
	}
	return new FObserverSceneProxy(this, FVector::ZeroVector, FVector::ZeroVector, false, ProxyColor);
}

FBoxSphereBounds UObserverComponentBase::CalcBounds(const FTransform& LocalToWorld) const
{
	if (EventComponentReferencePtr)
	{
		if (AActor* SubjectOwner = EventComponentReferencePtr->OtherActor.Get())
		{
			return FBoxSphereBounds(FSphere(GetComponentLocation(), FVector::Distance(GetComponentLocation(), SubjectOwner->GetActorLocation())));
		}
	}

	return FBoxSphereBounds(FSphere(GetComponentLocation(), 5.f));
}

void UObserverComponentBase::OnRegister()
{
	Super::OnRegister();

#if WITH_EDITOR
	if (GEditor)
	{
		GEditor->GetTimerManager()->SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &UObserverComponentBase::UpdateRender), 0.5, true);
	}
	else
#endif // WITH_EDITOR
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &UObserverComponentBase::UpdateRender), 0.5, true);
	}
}

void UObserverComponentBase::OnUnregister()
{
#if WITH_EDITOR
	if (GEditor)
	{
		GEditor->GetTimerManager()->ClearTimer(TimerHandle);
	}
	else
#endif // WITH_EDITOR
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	}
	Super::OnUnregister();
}

void UObserverComponentBase::UpdateRender()
{
	MarkRenderStateDirty();
}

// -------------------------------------------------------------------------------------------------------------

UObserverComponent::UObserverComponent()
{
	ProxyColor = FColor::White;
	EventComponentReferencePtr = &EventComponentReference;
}

void UObserverComponent::BeginPlay()
{
	Super::BeginPlay();

	if (UEventComponent* EventComponent = Cast<UEventComponent>(EventComponentReference.GetComponent(EventComponentReference.OtherActor.Get())))
	{
		EventComponent->OnNotify.AddUObject(this, &UObserverComponent::OnNotifyFunction);
	}
}

#if WITH_EDITOR
void UObserverComponent::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.GetMemberPropertyName() == TEXT("EventComponentReference"))
	{
		EventComponentOwner = EventComponentReference.OtherActor.Get();
	}
}
#endif // WITH_EDITOR

void UObserverComponent::OnNotifyFunction()
{
	OnNotify.Broadcast();
}

// -------------------------------------------------------------------------------------------------------------

UBoolObserverComponent::UBoolObserverComponent()
{
	ProxyColor = FColor::Red;
	EventComponentReferencePtr = &EventComponentReference;
}

void UBoolObserverComponent::BeginPlay()
{
	Super::BeginPlay();

	if (UBoolEventComponent* EventComponent = Cast<UBoolEventComponent>(EventComponentReference.GetComponent(EventComponentReference.OtherActor.Get())))
	{
		EventComponent->OnBoolNotify.AddUObject(this, &UBoolObserverComponent::OnBoolNotifyFunction);
	}
}

#if WITH_EDITOR
void UBoolObserverComponent::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.GetMemberPropertyName() == TEXT("EventComponentReference"))
	{
		EventComponentOwner = EventComponentReference.OtherActor.Get();
	}
}
#endif // WITH_EDITOR

void UBoolObserverComponent::OnBoolNotifyFunction(bool bValue)
{
	OnNotify.Broadcast(bValue);
}
