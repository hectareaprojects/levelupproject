// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "EventComponent.generated.h"

UCLASS()
class OBSERVER_API UEventComponentBase : public UPrimitiveComponent
{
	GENERATED_BODY()

protected:
	/**
	 * @brief The color of the scene proxy
	 */
	UPROPERTY()
	FColor ProxyColor;

protected:
	// UPrimitiveComponent interface
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	// USceneComponent interface
	virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;
};

// -------------------------------------------------------------------------------------------------------------

DECLARE_MULTICAST_DELEGATE(FOnNotify);

UCLASS(ClassGroup = "Event", meta = (BlueprintSpawnableComponent))
class OBSERVER_API UEventComponent : public UEventComponentBase
{
	GENERATED_BODY()

public:
	/**
	 * @brief List of delegates to notify
	 */
	FOnNotify OnNotify;

public:
	/**
	 * @brief The UEventComponent constructor.
	 */
	UEventComponent();

public:
	/**
	 * @brief Notifies the event
	 */
	UFUNCTION(BlueprintCallable, Category = "EventComponent")
	void Notify();
};

// -------------------------------------------------------------------------------------------------------------

DECLARE_MULTICAST_DELEGATE_OneParam(FOnBoolNotify, bool);

UCLASS(ClassGroup = "Event", meta = (BlueprintSpawnableComponent))
class OBSERVER_API UBoolEventComponent : public UEventComponentBase
{
	GENERATED_BODY()

public:
	/**
	 * @brief List of delegates to notify
	 */
	FOnBoolNotify OnBoolNotify;

public:
	/**
	 * @brief The UEventComponent constructor.
	 */
	UBoolEventComponent();

public:
	/**
	 * @brief Notifies the event
	 */
	UFUNCTION(BlueprintCallable, Category = "EventComponent")
	void Notify(bool bValue);
};
