// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/EngineTypes.h"
#include "ObserverComponent.generated.h"

UCLASS()
class OBSERVER_API UObserverComponentBase : public UPrimitiveComponent
{
	GENERATED_BODY()

protected:
	/** Information to draw the scene proxy */
	FColor ProxyColor;
	FComponentReference* EventComponentReferencePtr;

private:
	FTimerHandle TimerHandle;

public:
	UObserverComponentBase()
		: EventComponentReferencePtr(nullptr) {}

protected:
	// UPrimitiveComponent interface
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	// USceneComponent interface
	virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;

	// UActorComponent interface
	virtual void OnRegister() override;
	virtual void OnUnregister() override;

private:
	void UpdateRender();
};

// -------------------------------------------------------------------------------------------------------------

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSubjectNotify);

UCLASS(ClassGroup = "Event", meta = (BlueprintSpawnableComponent))
class OBSERVER_API UObserverComponent : public UObserverComponentBase
{
	GENERATED_BODY()

public:
	/**
	 * @brief The subject component to observe
	 */
	UPROPERTY(EditInstanceOnly, Category = "ObserverComponent", meta = (UseComponentPicker, AllowAnyActor, AllowedClasses = "/Script/Observer.EventComponent"))
	FComponentReference EventComponentReference;

	/**
	 * @brief HACK! We need this to make EventComponentReference to work properly (see PostEditChangeProperty)
	 *              Also, it must be visible (WTF)!!.
	 */
	UPROPERTY(VisibleInstanceOnly, Category = "ObserverComponent")
	AActor* EventComponentOwner;

	/**
	 * @brief The event called when the subject notifies
	 */
	UPROPERTY(BlueprintAssignable, Category = "ObserverComponent")
	FOnSubjectNotify OnNotify;

public:
	/**
	 * @brief The UMyObject constructor.
	 */
	UObserverComponent();

protected:
	virtual void BeginPlay() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	/**
	 * @brief The function called when the subject notifies
	 */
	UFUNCTION(BlueprintCallable, Category = "ObserverComponent")
	void OnNotifyFunction();
};

// -------------------------------------------------------------------------------------------------------------

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSubjectBoolNotify, bool, bValue);

UCLASS(ClassGroup = "Event", meta = (BlueprintSpawnableComponent))
class OBSERVER_API UBoolObserverComponent : public UObserverComponentBase
{
	GENERATED_BODY()

public:
	/**
	 * @brief The subject component to observe
	 */
	UPROPERTY(EditInstanceOnly, Category = "ObserverComponent", meta = (UseComponentPicker, AllowAnyActor, AllowedClasses = "/Script/Observer.BoolEventComponent"))
	FComponentReference EventComponentReference;

	/**
	 * @brief HACK! We need this to make EventComponentReference to work properly (see PostEditChangeProperty)
	 *              Also, it must be visible (WTF)!!.
	 */
	UPROPERTY(VisibleInstanceOnly, Category = "ObserverComponent")
	AActor* EventComponentOwner;

	/**
	 * @brief The event called when the subject notifies
	 */
	UPROPERTY(BlueprintAssignable, Category = "ObserverComponent")
	FOnSubjectBoolNotify OnNotify;

public:
	/**
	 * @brief The UMyObject constructor.
	 */
	UBoolObserverComponent();

protected:
	virtual void BeginPlay() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	/**
	 * @brief The function called when the subject notifies
	 */
	UFUNCTION(BlueprintCallable, Category = "ObserverComponent")
	void OnBoolNotifyFunction(bool bValue);
};
