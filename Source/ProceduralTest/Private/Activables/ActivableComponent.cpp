// TODO: Copyright

#include "Activables/ActivableComponent.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Actor.h"
#include "Activables/ActivatorComponent.h"

void UActivableBaseComponent::UpdateActivationState()
{
	if (GetActivationState())
	{
		OnActivation.Broadcast();
	}
	else
	{
		OnDeactivation.Broadcast();
	}
}

bool UActivableBaseComponent::GetActivationState_Implementation()
{
	return false;
}

void UActivableBaseComponent::BeginPlay()
{
	Super::BeginPlay();
	OnActivatorActivated.BindDynamic(this, &UActivableBaseComponent::UpdateActivationState);
	OnActivatorDeactivated.BindDynamic(this, &UActivableBaseComponent::UpdateActivationState);

	if (bStartActivated)
	{
		OnActivation.Broadcast();
	}
	else
	{
		OnDeactivation.Broadcast();
	}
}

// --------------------------------------------------------------------------------------------------------

void USingleActivableComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Activator)
	{
		if (UActivatorComponent* NewActivatorComponent = Activator->FindComponentByClass<UActivatorComponent>())
		{
			NewActivatorComponent->AddOnActivated(OnActivatorActivated);
			NewActivatorComponent->AddOnDeactivated(OnActivatorDeactivated);
			ActivatorComponent = NewActivatorComponent;
		}
	}
}

// --------------------------------------------------------------------------------------------------------

void UMultiActivableComponent::BeginPlay()
{
	Super::BeginPlay();
	for (AActor* Activator : Activators)
	{
		if (Activator)
		{
			if (UActivatorComponent* NewActivatorComponent = Activator->FindComponentByClass<UActivatorComponent>())
			{
				NewActivatorComponent->AddOnActivated(OnActivatorActivated);
				NewActivatorComponent->AddOnDeactivated(OnActivatorDeactivated);
				ActivatorsComponents.Add(NewActivatorComponent);
			}
		}
	}
}

// -------------------------------------------------------------------------------------------------------

bool UActivableComponent::GetActivationState_Implementation()
{
	if (ActivatorComponent)
	{
		return ActivatorComponent->IsActivated();
	}
	return false;
}
