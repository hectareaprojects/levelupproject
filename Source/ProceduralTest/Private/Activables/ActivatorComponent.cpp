// TODO: Copyright

#include "Activables/ActivatorComponent.h"

UActivatorComponent::UActivatorComponent()
	: bActivated(false) {}

void UActivatorComponent::AddOnActivated(const FOnActivatedFunc& OnActivatedFunc)
{
	OnActivated.Add(OnActivatedFunc);
}

void UActivatorComponent::AddOnDeactivated(const FOnDeactivatedFunc& OnDeactivatedFunc)
{
	OnDeactivated.Add(OnDeactivatedFunc);
}

void UActivatorComponent::ActivateAndNotify()
{
	bActivated = true;
	OnActivated.Broadcast();
}

void UActivatorComponent::DeactivateAndNotify()
{
	bActivated = false;
	OnDeactivated.Broadcast();
}

bool UActivatorComponent::IsActivated() const
{
	return bActivated;
}
