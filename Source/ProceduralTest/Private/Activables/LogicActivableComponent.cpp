// TODO: Copyright

#include "Activables/LogicActivableComponent.h"
#include "Activables/ActivatorComponent.h"
#include "GameFramework/Actor.h"

bool UAndActivableComponent::GetActivationState_Implementation()
{
	for (UActivatorComponent* ActivatorComponent : ActivatorsComponents)
	{
		if (ActivatorComponent && !ActivatorComponent->IsActivated())
		{
			return false;
		}
	}
	return true;
}

// ----------------------------------------------------------------------------------------------------

bool UOrActivableComponent::GetActivationState_Implementation()
{
	for (UActivatorComponent* ActivatorComponent : ActivatorsComponents)
	{
		if (ActivatorComponent && ActivatorComponent->IsActivated())
		{
			return true;
		}
	}
	return false;
}

// -----------------------------------------------------------------------------------------------------

bool UNotActivableComponent::GetActivationState_Implementation()
{
	if (ActivatorComponent)
	{
		return !ActivatorComponent->IsActivated();
	}
	return false;
}
