// TODO: Copyright

#include "Activables/LogicActivator.h"
#include "Activables/ActivatorComponent.h"
#include "Activables/ActivableComponent.h"
#include "Activables/LogicActivableComponent.h"
#include "WrapperValues.h"

ALogicActivator::ALogicActivator()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	ActivatorComponent = CreateDefaultSubobject<UActivatorComponent>(TEXT("ActivatorComponent"));
}

void ALogicActivator::BeginPlay()
{
	Super::BeginPlay();

	ActivableComponent->OnActivation.AddDynamic(this, &ALogicActivator::OnActivation);
	ActivableComponent->OnDeactivation.AddDynamic(this, &ALogicActivator::OnDeactivation);
}

void ALogicActivator::OnActivation()
{
	ActivatorComponent->ActivateAndNotify();
}

void ALogicActivator::OnDeactivation()
{
	ActivatorComponent->DeactivateAndNotify();
}

// ----------------------------------------------------------------------------------------------------

AAndActivator::AAndActivator()
{
	ActivableComponent = CreateDefaultSubobject<UAndActivableComponent>(TEXT("AndActivableComponent"));
}

// ---------------------------------------------------------------------------------------------------

AOrActivator::AOrActivator()
{
	ActivableComponent = CreateDefaultSubobject<UOrActivableComponent>(TEXT("OrActivableComponent"));
}

// ---------------------------------------------------------------------------------------------------

ANotActivator::ANotActivator()
{
	ActivableComponent = CreateDefaultSubobject<UNotActivableComponent>(TEXT("NotActivableComponent"));
}
