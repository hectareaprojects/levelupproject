// TODO: Copyright

#include "AsynchronousInterp/AsynchronousInterp.h"
#include "Containers/Ticker.h"
#include "TimerManager.h"

UAsyncInterpFloat* UAsyncInterpFloat::InterpolationFloat(float InStart, float InEnd, float InTime)
{
	UAsyncInterpFloat* AsyncInterpFloat = NewObject<UAsyncInterpFloat>();
	AsyncInterpFloat->InterpInfo.Start = InStart;
	AsyncInterpFloat->InterpInfo.End = InEnd;
	AsyncInterpFloat->Time = InTime;

	return AsyncInterpFloat;
}

void UAsyncInterpFloat::Activate()
{
	Current = 0.f;
	Update.Broadcast(InterpInfo.Start);
	FTSTicker::GetCoreTicker().AddTicker(FTickerDelegate::CreateUObject(this, &UAsyncInterpFloat::UpdateValue));
}

bool UAsyncInterpFloat::UpdateValue(float DeltaTime)
{
	Current += DeltaTime;
	if (Current > Time)
	{
		Completed.Broadcast(InterpInfo.End);
		return false;
	}
	else
	{
		Update.Broadcast(InterpInfo.GetValue(Current / Time));
		return true;
	}
}

// -------------------------------------------------------------------------------------------------------------

UAsyncInterpVector* UAsyncInterpVector::InterpolationVector(const FVector& InStart, const FVector& InEnd, float InTime)
{
	UAsyncInterpVector* AsyncInterpVector = NewObject<UAsyncInterpVector>();
	AsyncInterpVector->InterpInfo.Start = InStart;
	AsyncInterpVector->InterpInfo.End = InEnd;
	AsyncInterpVector->Time = InTime;

	return AsyncInterpVector;
}

void UAsyncInterpVector::Activate()
{
	Current = 0.f;
	Update.Broadcast(InterpInfo.Start);
	FTSTicker::GetCoreTicker().AddTicker(FTickerDelegate::CreateUObject(this, &UAsyncInterpVector::UpdateValue));
}

bool UAsyncInterpVector::UpdateValue(float DeltaTime)
{
	Current += DeltaTime;
	if (Current > Time)
	{
		Completed.Broadcast(InterpInfo.End);
		return false;
	}
	else
	{
		Update.Broadcast(InterpInfo.GetValue(Current / Time));
		return true;
	}
}

// -------------------------------------------------------------------------------------------------------------

FQuat FInterpInfoFQuat::GetValue(float Alpha)
{
	return FQuat(Axis, Alpha * Angle) * Start;
}

UAsyncInterpQuat* UAsyncInterpQuat::InterpolationQuat(const FQuat& InStart, const FQuat& InEnd, float InTime)
{
	UAsyncInterpQuat* AsyncInterpQuat = NewObject<UAsyncInterpQuat>();
	AsyncInterpQuat->InterpInfo.Start = InStart;
	AsyncInterpQuat->InterpInfo.End = InEnd;
	FQuat DeltaQuat = InStart.Inverse() * InEnd;
	AsyncInterpQuat->InterpInfo.Axis = DeltaQuat.GetRotationAxis();
	AsyncInterpQuat->InterpInfo.Angle = DeltaQuat.GetAngle();
	AsyncInterpQuat->Time = InTime;

	return AsyncInterpQuat;
}

void UAsyncInterpQuat::Activate()
{
	Current = 0.f;
	Update.Broadcast(InterpInfo.Start);
	FTSTicker::GetCoreTicker().AddTicker(FTickerDelegate::CreateUObject(this, &UAsyncInterpQuat::UpdateValue));
}

bool UAsyncInterpQuat::UpdateValue(float DeltaTime)
{
	Current += DeltaTime;
	if (Current > Time)
	{
		Completed.Broadcast(InterpInfo.End);
		return false;
	}
	else
	{
		Update.Broadcast(InterpInfo.GetValue(Current / Time));
		return true;
	}
}
