// TODO: Copyright

#include "Attachables/AttachableActorComponent.h"
#include "GameFramework/Actor.h"

DEFINE_LOG_CATEGORY(LogAttachable);

bool UAttachableActorComponent::IsActorAttached()
{
	return GetOwner()->GetAttachParentActor() != nullptr;
}

void UAttachableActorComponent::AttachToComponent(USceneComponent* InComponentToAttach)
{
	if (!IsActorAttached())
	{
		if (InComponentToAttach)
		{
			GetOwner()->AttachToComponent(InComponentToAttach, FAttachmentTransformRules::KeepRelativeTransform);
			GetOwner()->SetActorRelativeLocation(RelativeLocation);
			GetOwner()->SetActorRelativeRotation(RelativeRotation);
		}
		else
		{
			UE_LOG(LogAttachable, Warning, TEXT("The component to attach argument is null!"));
		}
	}
	else
	{
		UE_LOG(LogAttachable, Warning, TEXT("The owner is already attached!"));
	}
}

void UAttachableActorComponent::AttachToActor(AActor* ActorToAttach)
{
	if (ActorToAttach)
	{
		AttachToComponent(ActorToAttach->GetRootComponent());
	}
	else
	{
		UE_LOG(LogAttachable, Warning, TEXT("The actor to attach argument is null!"));
	}
}

void UAttachableActorComponent::Detach()
{
	if (IsActorAttached())
	{
		GetOwner()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	}
	else
	{
		UE_LOG(LogAttachable, Warning, TEXT("The owner is not attached to anything!"));
	}
}
