// Fill out your copyright notice in the Description page of Project Settings.


#include "Attributes/HealthBarComponent.h"
#include "Attributes/HealthComponent.h"


UHealthBarComponent::UHealthBarComponent() {

    Space = EWidgetSpace::Screen;

}


void UHealthBarComponent::BeginPlay() {

    Super::BeginPlay();

    HealthComponent = GetOwner()->FindComponentByClass<UHealthComponent>();

    if (HealthComponent) {
        HealthComponent->OnDamaged.AddDynamic(this, &UHealthBarComponent::OnHealthChangedCaller);
        HealthComponent->OnHealed.AddDynamic(this, &UHealthBarComponent::OnHealthChangedCaller);
    }

}


void UHealthBarComponent::OnHealthChangedCaller(float HealthDiff) {

    OnHealthChanged.Broadcast(static_cast<float>(HealthComponent->GetCurrentHealth())/static_cast<float>(HealthComponent->GetMaxHealth()));

}
