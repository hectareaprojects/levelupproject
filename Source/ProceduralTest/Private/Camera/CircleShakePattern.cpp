// TODO: Copyright

#include "Camera/CircleShakePattern.h"
#include "Math/UnrealMathUtility.h"

void UCircleShakePattern::UpdateShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult)
{
	UpdateDeltas(Params.DeltaTime);
	UpdateCircles(OutResult);
}

void UCircleShakePattern::ScrubShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult)
{
	CurrentLocationDelta = 0.f;
	CurrentRotationDelta = 0.f;
	UpdateDeltas(Params.AbsoluteTime);
	UpdateCircles(OutResult);
}

static float GetUpdatedDelta(float CurrentDelta, float Delta)
{
	CurrentDelta += Delta;
	if (CurrentDelta >= 1.f)
	{
		float IntPart;
		CurrentDelta = FMath::Modf(CurrentDelta, &IntPart);
	}
	return CurrentDelta;
}

void UCircleShakePattern::UpdateDeltas(float Delta)
{
	CurrentLocationDelta = GetUpdatedDelta(CurrentLocationDelta, Delta * LocationFrequency);
	CurrentRotationDelta = GetUpdatedDelta(CurrentRotationDelta, Delta * RotationFrequency);
}

FVector UCircleShakePattern::GetLocation()
{
	FVector Location;
	Location.X = 0.f;
	Location.Y = LocationRadius * FMath::Cos(CurrentLocationDelta * 2.f * PI);
	Location.Z = LocationRadius * FMath::Sin(CurrentLocationDelta * 2.f * PI);

	return Location;
}

FRotator UCircleShakePattern::GetRotation()
{
	FRotator Rotator;
	Rotator.Roll = 0.f;
	Rotator.Yaw = RotationAngle * FMath::Cos(CurrentRotationDelta * 2.f * PI);
	Rotator.Pitch = RotationAngle * FMath::Sin(CurrentRotationDelta * 2.f * PI);

	return Rotator;
}

void UCircleShakePattern::UpdateCircles(FCameraShakeUpdateResult& OutResult)
{
	OutResult.Location = GetLocation();
	OutResult.Rotation = GetRotation();
}
