// TODO: Copyright

#include "Camera/CurveShakePattern.h"
#include "Math/UnrealMathUtility.h"
#include "Curves/CurveFloat.h"

void UCurvePattern::Initialize()
{
	CurrentDelta = Offset;
}

bool UCurvePattern::IsValid()
{
	return Curve != nullptr;
}

void UCurvePattern::Update(float Delta)
{
	CurrentDelta += Delta * Frequency;

	TryCallNotifiers();

	if (CurrentDelta >= 1.f)
	{
		float IntPart;
		CurrentDelta = FMath::Modf(CurrentDelta, &IntPart);
		ResetIterator();
	}
}

float UCurvePattern::GetCurveValue()
{
	return GetCurveValueAt(CurrentDelta);
}

float UCurvePattern::GetCurveValueAt(float Delta)
{
	if (Curve)
	{
		return Amplitude * Curve->GetFloatValue(Delta);
	}
	return 0.f;
}

void UCurvePattern::ResetIterator()
{
	NextNotifierIndex = 0;
}

void UCurvePattern::TryCallNotifiers()
{
	while (NextNotifierIndex < Notifiers.Num() && CurrentDelta > Notifiers[NextNotifierIndex].Key)
	{
		Notifiers[NextNotifierIndex].Value.ExecuteIfBound();
		NextNotifierIndex++;
	}
}

void UCurvePattern::AddNotifier(float Time, const FShakeNotifier& Notifier)
{
	Notifiers.Add(TPair<float, FShakeNotifier>(Time, Notifier));
	Notifiers.Sort([](const TPair<float, FShakeNotifier>& V1, const TPair<float, FShakeNotifier>& V2) {
		return V1.Key < V2.Key;
	});
}

// --------------------------------------------------------------------------------------------------

UCurveShakePattern::UCurveShakePattern(const FObjectInitializer& ObjInit)
	: Super(ObjInit)
{
	X = CreateDefaultSubobject<UCurvePattern>(TEXT("X Pattern"));
	Y = CreateDefaultSubobject<UCurvePattern>(TEXT("Y Pattern"));
	Z = CreateDefaultSubobject<UCurvePattern>(TEXT("Z Pattern"));
	Pitch = CreateDefaultSubobject<UCurvePattern>(TEXT("Pitch Pattern"));
	Yaw = CreateDefaultSubobject<UCurvePattern>(TEXT("Yaw Pattern"));
	Roll = CreateDefaultSubobject<UCurvePattern>(TEXT("Roll Pattern"));
	FOV = CreateDefaultSubobject<UCurvePattern>(TEXT("POV Pattern"));
	FOV->Amplitude = 0.f;
}

void UCurveShakePattern::StartPlayerShakePatternImpl(const FCameraShakeStartParams& Params)
{
	if (!Params.bIsRestarting)
	{
		InitializeCurves();
	}
}

void UCurveShakePattern::UpdatePlayerShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult)
{
	UpdateCurves(Params.DeltaTime, OutResult);
}

void UCurveShakePattern::ScrubPlayerShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult)
{
	InitializeCurves();
	UpdateCurves(Params.AbsoluteTime, OutResult);
}

void UCurveShakePattern::InitializeCurves()
{
	X->Initialize();
	Y->Initialize();
	Z->Initialize();

	Pitch->Initialize();
	Yaw->Initialize();
	Roll->Initialize();

	FOV->Initialize();
}

void UCurveShakePattern::UpdateCurves(float DeltaTime, FCameraShakeUpdateResult& OutResult)
{
	X->Update(DeltaTime * LocationFrequencyMultiplier);
	Y->Update(DeltaTime * LocationFrequencyMultiplier);
	Z->Update(DeltaTime * LocationFrequencyMultiplier);

	Pitch->Update(DeltaTime * RotationFrequencyMultiplier);
	Yaw->Update(DeltaTime * RotationFrequencyMultiplier);
	Roll->Update(DeltaTime * RotationFrequencyMultiplier);

	FOV->Update(DeltaTime);

	OutResult.Location.X = LocationAmplitudeMultiplier * X->GetCurveValue();
	OutResult.Location.Y = LocationAmplitudeMultiplier * Y->GetCurveValue();
	OutResult.Location.Z = LocationAmplitudeMultiplier * Z->GetCurveValue();

	OutResult.Rotation.Pitch = RotationAmplitudeMultiplier * Pitch->GetCurveValue();
	OutResult.Rotation.Yaw = RotationAmplitudeMultiplier * Yaw->GetCurveValue();
	OutResult.Rotation.Roll = RotationAmplitudeMultiplier * Roll->GetCurveValue();

	OutResult.FOV = FOV->GetCurveValue();
}
