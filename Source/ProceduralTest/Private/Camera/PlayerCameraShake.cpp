// TODO: Copyright

#include "Camera/PlayerCameraShake.h"
#include "Camera/PlayerCameraManager.h"
#include "GameFramework/PlayerController.h"

UPlayerCameraShake::UPlayerCameraShake(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {}

APlayerController* UPlayerCameraShake::GetControllerOwner()
{
	return GetCameraManager()->GetOwningPlayerController();
}

APawn* UPlayerCameraShake::GetPawnOwner()
{
	return GetCameraManager()->GetOwningPlayerController()->GetPawn();
}

#if WITH_EDITOR
void UPlayerCameraShake::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	if (e.GetPropertyName() == GET_MEMBER_NAME_CHECKED(UPlayerCameraShake, PlayerCameraShakePattern))
	{
		SetRootShakePattern(PlayerCameraShakePattern);
	}
}
#endif

// -------------------------------------------------------------------------------------------------------

void UPlayerCameraShakePattern::StartShakePatternImpl(const FCameraShakeStartParams& Params)
{
	UE_LOG(LogTemp, Warning, TEXT("Hola?"));
	if (UPlayerCameraShake* Shake = GetShakeInstance<UPlayerCameraShake>())
	{
		Shake->ReceiveInitialize();
	}
	StartPlayerShakePatternImpl(Params);
}

void UPlayerCameraShakePattern::UpdateShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult)
{
	if (UPlayerCameraShake* Shake = GetShakeInstance<UPlayerCameraShake>())
	{
		Shake->ReceiveUpdate();
	}
	UpdatePlayerShakePatternImpl(Params, OutResult);
}

void UPlayerCameraShakePattern::ScrubShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult)
{
	ScrubPlayerShakePatternImpl(Params, OutResult);
}

bool UPlayerCameraShakePattern::IsFinishedImpl() const
{
	return IsPlayerShakeFinishedImpl();
}

void UPlayerCameraShakePattern::StopShakePatternImpl(const FCameraShakeStopParams& Params)
{
	if (UPlayerCameraShake* Shake = GetShakeInstance<UPlayerCameraShake>())
	{
		Shake->ReceiveStop();
	}
	StopPlayerShakePatternImpl(Params);
}

void UPlayerCameraShakePattern::TeardownShakePatternImpl()
{
	TeardownPlayerShakePatternImpl();
}
