// TODO: Copyright

#include "Camera/ProceduralPointsOfInterest.h"
#include "LevelGenerator/LevelGenerator.h"
#include "Algo/Reverse.h"
#include "Camera/SplineCamera.h"
#include "Math/MathUtilsLibrary.h"
#include "Math/UnrealMathUtility.h"

TArray<FRoomPointsOfInterest> UProceduralPointsOfInterest::GetLevelPointsOfInterest(ALevelGenerator* LevelGenerator)
{
	TArray<FRoomPointsOfInterest> PointsOfInterest;

	if (LevelGenerator)
	{
		for (ALevelInfo* FixedRoom : LevelGenerator->GetFixedRooms())
		{
			TArray<UPointOfInterestComponent*> PointOfInterestComponents;
			FixedRoom->GetComponents<UPointOfInterestComponent>(PointOfInterestComponents);
			if (!PointOfInterestComponents.IsEmpty())
			{
				TArray<FPointOfInterestInfo> RoomPointsOfInterest;
				for (UPointOfInterestComponent* PointOfInterestComponent : PointOfInterestComponents)
				{
					RoomPointsOfInterest.Add(PointOfInterestComponent->GetPointOfInterestInfo());
				}
				RoomPointsOfInterest.Sort([](const FPointOfInterestInfo& P1, const FPointOfInterestInfo& P2) {
					return P1.ExtraInfo.Priority < P2.ExtraInfo.Priority;
				});
				PointsOfInterest.Add(FRoomPointsOfInterest{ MoveTemp(RoomPointsOfInterest) });
			}
		}
	}

	return PointsOfInterest;
}

TArray<FPointOfInterestInfo> UProceduralPointsOfInterest::GetRoomPointsOfInterest(const TArray<FRoomPointsOfInterest>& PointsOfInterest, int Index)
{
	return PointsOfInterest[Index % PointsOfInterest.Num()].RoomPointsOfInterest;
}

TArray<FPointOfInterestInfo> UProceduralPointsOfInterest::GetBeginningTravelPointsOfInterest(const TArray<FRoomPointsOfInterest>& LevelPointsOfInterest, int NumberOfRooms)
{
	TArray<FPointOfInterestInfo> BeginningPointsOfInterest;
	for (int i = 0; i < LevelPointsOfInterest.Num() && i < NumberOfRooms; i++)
	{
		const FRoomPointsOfInterest& RoomPointsOfInterest = LevelPointsOfInterest[i];
		BeginningPointsOfInterest.Append(RoomPointsOfInterest.RoomPointsOfInterest);
	}
	return BeginningPointsOfInterest;
}

TArray<FPointOfInterestInfo> UProceduralPointsOfInterest::GetEndingTravelPointsOfInterest(const TArray<FRoomPointsOfInterest>& LevelPointsOfInterest, int NumberOfRooms)
{
	TArray<FPointOfInterestInfo> EndingPointsOfInterest;
	for (int i = 0; i < LevelPointsOfInterest.Num() && i < NumberOfRooms; i++)
	{
		const FRoomPointsOfInterest& RoomPointsOfInterest = LevelPointsOfInterest[LevelPointsOfInterest.Num() - i - 1];
		EndingPointsOfInterest.Append(RoomPointsOfInterest.RoomPointsOfInterest);
	}

	Algo::Reverse(EndingPointsOfInterest);
	return EndingPointsOfInterest;
}

static TArray<FPointOfInterestInfo> FlattenPointOfInterestArray(const TArray<FRoomPointsOfInterest>& PointsOfInterest)
{
	TArray<FPointOfInterestInfo> FlattenedPointsOfInterest;
	for (const FRoomPointsOfInterest& RoomPointsOfInterest : PointsOfInterest)
	{
		FlattenedPointsOfInterest.Append(RoomPointsOfInterest.RoomPointsOfInterest);
	}
	return FlattenedPointsOfInterest;
}

static float GetMaxDistanceFromPoint(const TArray<FVector>& Locations, const FVector& Point)
{
	if (!Locations.IsEmpty())
	{
		float MaxDistance = FVector::Dist(Locations[0], Point);
		for (int i = 1; i < Locations.Num(); i++)
		{
			float Distance = FVector::Dist(Locations[i], Point);
			MaxDistance = MaxDistance < Distance ? Distance : MaxDistance;
		}
		return MaxDistance;
	}

	return 0.f;
}

static float AngleVectors(const FVector& V1, const FVector& V2)
{
	if (!V1.IsNearlyZero() && !V2.IsNearlyZero())
		return FMath::Acos(V1 | V2);
	return 0.f;
}

static bool ParallelVectors(const FVector& V1, const FVector& V2)
{
	return AngleVectors(V1, V2) < FMath::Min(UE_KINDA_SMALL_NUMBER, PI - UE_KINDA_SMALL_NUMBER);
}

// Returns a value between -90 and 90
static float VectorPitch(const FVector& V)
{
	return FMath::RadiansToDegrees(FMath::Atan2(V.Z, FMath::Sqrt(V.X * V.X + V.Y * V.Y)));
}

static bool IsDangerousTravel(const FVector& InitialDirection, const FVector& RotationAxis, float Angle, int Steps, float PitchTolerance = 10.f)
{
	for (int i = 0; i <= Steps; i++)
	{
		float CurrentAngle = static_cast<float>(i) * (Angle / static_cast<float>(Steps));
		FVector CurrentDirection = InitialDirection.RotateAngleAxis(CurrentAngle, RotationAxis);
		if (FMath::Abs(VectorPitch(CurrentDirection)) > (90.f - PitchTolerance))
		{
			return true;
		}
	}
	return false;
}

TArray<FPointOfInterestInfo> UProceduralPointsOfInterest::GetShowAllLevelPointsOfInterest(const TArray<FRoomPointsOfInterest>& LevelPointsOfInterest, float AngleDeg)
{
	// Get all points of interest and their mean up direction
	TArray<FPointOfInterestInfo> PointsOfInterest = FlattenPointOfInterestArray(LevelPointsOfInterest);
	TArray<FVector> PointOfInterestLocations;
	for (FPointOfInterestInfo& PointOfInterestInfo : PointsOfInterest)
	{
		PointOfInterestLocations.Add(PointOfInterestInfo.Transform.GetLocation());
	}

	// Get the mean up direction of points of interest
	FVector UpDirection;
	for (const FRoomPointsOfInterest& RoomPointsOfInterest : LevelPointsOfInterest)
	{
		FVector RoomUpDirection;
		for (const FPointOfInterestInfo& PointOfInterestInfo : RoomPointsOfInterest.RoomPointsOfInterest)
		{
			RoomUpDirection += PointOfInterestInfo.Transform.GetRotation().GetUpVector();
		}
		UpDirection += RoomUpDirection.GetSafeNormal();
	}
	UpDirection.Normalize();

	// Get the regression line of points of interest
	FVector Origin;
	FVector Direction;
	UMathUtilsLibrary::RegressionLine3D(PointOfInterestLocations, Origin, Direction);

	// Get the farthest point of interest from the origin of all of them
	float MaxDistanceFromOrigin = GetMaxDistanceFromPoint(PointOfInterestLocations, Origin);

	// Get the angle between the regression line and the up direction
	float AngleDirections = AngleVectors(Direction, UpDirection);

	// Get the initial direction and update the angle
	FVector InitialDirection = AngleDirections < PI / 2.f ? Direction : -Direction;
	AngleDirections = FMath::Min(AngleDirections, PI - AngleDirections);

	// Get the rotation axis
	FVector RotationAxis;
	if (AngleDirections < UE_KINDA_SMALL_NUMBER)
	{
		FVector UpVector(0.f, 0.f, 1.f);
		FVector DownVector(0.f, 0.f, -1.f);
		RotationAxis = ParallelVectors(InitialDirection, UpVector)
			? FVector(InitialDirection.Z > 0 ? -1.f : 1.f, 0.f, 0.f)
			: ((InitialDirection.Z > 0 ? UpVector : DownVector) ^ InitialDirection).GetUnsafeNormal();
	}
	else
	{
		RotationAxis = (InitialDirection ^ UpDirection).GetUnsafeNormal();
		if (IsDangerousTravel(InitialDirection, RotationAxis, AngleDeg, static_cast<int>(AngleDeg) / 2))
		{
			RotationAxis = FVector(Direction.Z > 0 ? -1.f : 1.f, 0.f, 0.f);
		}
	}

	// Get the directions of the desired points
	FVector MiddleDirection = InitialDirection.RotateAngleAxis(AngleDeg / 2.f, RotationAxis);
	FVector UpperDirection = InitialDirection.RotateAngleAxis(AngleDeg, RotationAxis);

	// Get the points
	FVector P1 = Origin + InitialDirection * (MaxDistanceFromOrigin + 2000.f);
	FVector P2 = Origin + MiddleDirection * (MaxDistanceFromOrigin + 2000.f);
	FVector P3 = Origin + UpperDirection * (MaxDistanceFromOrigin + 2000.f);

	// Create the array
	TArray<FPointOfInterestInfo> TravelPointsOfInterest;
	TravelPointsOfInterest.Add(FPointOfInterestInfo{ FTransform((Origin - P1).Rotation(), P1), FPointOfInterestExtraInfo() });
	TravelPointsOfInterest.Add(FPointOfInterestInfo{ FTransform((Origin - P2).Rotation(), P2), FPointOfInterestExtraInfo() });
	TravelPointsOfInterest.Add(FPointOfInterestInfo{ FTransform((Origin - P3).Rotation(), P3), FPointOfInterestExtraInfo() });

	return TravelPointsOfInterest;
}
