// TODO: Copyright

#include "Camera/SplineCamera.h"
#include "Components/SplineComponent.h"
#include "Curves/CurveFloat.h"
#include "Camera/CameraComponent.h"
#include "Math/RotationMatrix.h"

FPointOfInterestInfo UPointOfInterestComponent::GetPointOfInterestInfo()
{
	return FPointOfInterestInfo{ GetComponentTransform(), PointOfInterestExtraInfo };
}

// --------------------------------------------------------------------------------------------

ASplineCamera::ASplineCamera()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	ForwardSplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("ForwardSplineComponent"));
	ForwardSplineComponent->SetUnselectedSplineSegmentColor(FLinearColor::Red);
	ForwardSplineComponent->SetSelectedSplineSegmentColor(FLinearColor::Red);
	ForwardSplineComponent->SetTangentColor(FLinearColor::Red);
	UpSplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("UpSplineComponent"));
	UpSplineComponent->SetUnselectedSplineSegmentColor(FLinearColor::Blue);
	UpSplineComponent->SetSelectedSplineSegmentColor(FLinearColor::Blue);
	UpSplineComponent->SetTangentColor(FLinearColor::Blue);

	GetCameraComponent()->SetupAttachment(RootComponent);
}

void ASplineCamera::Tick(float DeltaSeconds)
{
	float CurrentSplinePosition = CurrentPosition;
	if (PositionCurve)
	{
		CurrentSplinePosition = PositionCurve->GetFloatValue(CurrentSplinePosition);
	}

	FVector CurrentLocation = SplineComponent->GetLocationAtTime(CurrentSplinePosition, ESplineCoordinateSpace::Type::World);
	FVector CurrentForwardLocation = ForwardSplineComponent->GetLocationAtTime(CurrentSplinePosition, ESplineCoordinateSpace::Type::World);
	FVector CurrentUpLocation = UpSplineComponent->GetLocationAtTime(CurrentSplinePosition, ESplineCoordinateSpace::Type::World);
	FVector CurrentForwardDirection = CurrentForwardLocation - CurrentLocation;
	FVector CurrentUpDirection = CurrentUpLocation - CurrentLocation;
	FQuat CurrentRotation = FRotationMatrix::MakeFromXZ(CurrentForwardDirection, CurrentUpDirection).ToQuat();
	// FRotator CurrentRotation = (CurrentForwardLocation - CurrentTransform.GetLocation()).Rotation();

	SetActorLocation(CurrentLocation);
	SetActorRotation(CurrentRotation);

	CurrentPosition += DeltaSeconds;
	if (CurrentPosition > Duration)
	{
		PauseTravel();
		ResetTravel();
		OnTravelEnded.ExecuteIfBound();
	}
}

const TArray<FPointOfInterestInfo>& ASplineCamera::GetPointsOfInterest() const
{
	return PointsOfInterest;
}

void ASplineCamera::SetPointsOfInterest(const TArray<FPointOfInterestInfo>& InPointsOfInterest)
{
	PointsOfInterest = InPointsOfInterest;
	// TODO: This is temporary
	UpdateSpline(InPointsOfInterest);
}

void ASplineCamera::UpdateSpline(const TArray<FPointOfInterestInfo>& NewPointsOfInterest)
{
	SplineComponent->ClearSplinePoints();
	ForwardSplineComponent->ClearSplinePoints();
	UpSplineComponent->ClearSplinePoints();

	TArray<FSplinePoint> SplinePoints;
	TArray<FSplinePoint> ForwardSplinePoints;
	TArray<FSplinePoint> UpSplinePoints;

	float AccTime = 0.f;
	FVector InitialPosition = NewPointsOfInterest[0].Transform.GetLocation();
	FVector InitialForwardPosition = InitialPosition + NewPointsOfInterest[0].Transform.GetRotation().GetForwardVector() * 100.f;
	FVector InitialUpPosition = InitialPosition + NewPointsOfInterest[0].Transform.GetRotation().GetUpVector() * 100.f;
	SplinePoints.Add(FSplinePoint(AccTime, InitialPosition, ESplinePointType::Curve));
	ForwardSplinePoints.Add(FSplinePoint(AccTime, InitialForwardPosition, ESplinePointType::Curve));
	UpSplinePoints.Add(FSplinePoint(AccTime, InitialUpPosition, ESplinePointType::Curve));

	for (int i = 1; i < NewPointsOfInterest.Num(); i++)
	{
		const FPointOfInterestInfo& PointOfInterestInfo = NewPointsOfInterest[i];

		AccTime += (NewPointsOfInterest[i - 1].ExtraInfo.TimeDilation + NewPointsOfInterest[i].ExtraInfo.TimeDilation) / 2.f;
		FVector Position = PointOfInterestInfo.Transform.GetLocation();
		FVector ForwardPosition = Position + PointOfInterestInfo.Transform.GetRotation().GetForwardVector() * 100.f;
		FVector UpPosition = Position + PointOfInterestInfo.Transform.GetRotation().GetUpVector() * 100.f;
		SplinePoints.Add(FSplinePoint(AccTime, Position, ESplinePointType::Curve));
		ForwardSplinePoints.Add(FSplinePoint(AccTime, ForwardPosition, ESplinePointType::Curve));
		UpSplinePoints.Add(FSplinePoint(AccTime, UpPosition, ESplinePointType::Curve));
	}
	SplineLength = AccTime;
	SplineComponent->AddPoints(SplinePoints);
	ForwardSplineComponent->AddPoints(ForwardSplinePoints);
	UpSplineComponent->AddPoints(UpSplinePoints);
}

USplineComponent* ASplineCamera::GetSplineComponent()
{
	return SplineComponent;
}

float ASplineCamera::GetDuration() const
{
	return Duration;
}

void ASplineCamera::SetDuration(float InDuration)
{
	Duration = InDuration;
	SplineComponent->Duration = Duration;
	ForwardSplineComponent->Duration = Duration;
	UpSplineComponent->Duration = Duration;
}

void ASplineCamera::ResetTravel()
{
	CurrentPosition = 0.f;
}

void ASplineCamera::PauseTravel()
{
	bPlaying = false;
	SetActorTickEnabled(false);
}

void ASplineCamera::PlayTravel(const FOnTravelEnded& OnTravelEndedFunc)
{
	SetActorTickEnabled(true);
	bPlaying = true;
	OnTravelEnded = OnTravelEndedFunc;
}

// ---------------------------------------------------------------------------------------------------------

UAsyncPlayTravel* UAsyncPlayTravel::PlayTravel(ASplineCamera* SplineCamera, float Duration)
{
	UAsyncPlayTravel* AsyncPlayTravel = NewObject<UAsyncPlayTravel>();
	AsyncPlayTravel->SplineCamera = SplineCamera;
	AsyncPlayTravel->Duration = Duration;

	return AsyncPlayTravel;
}

void UAsyncPlayTravel::Activate()
{
	if (SplineCamera)
	{
		SplineCamera->ResetTravel();
		SplineCamera->SetDuration(Duration);
		SplineCamera->PlayTravel(FOnTravelEnded::CreateUObject(this, &UAsyncPlayTravel::OnTravelEnded));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UAsyncPlayTravel::PlayTravel : Spline camera is null"));
	}
}

void UAsyncPlayTravel::OnTravelEnded()
{
	Completed.Broadcast();
}
