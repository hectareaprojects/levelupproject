// TODO: Copyright

#include "Gravity/GravityBoxEmitterComponent.h"
#include "Gravity/GravityReceiverComponent.h"
#include "Gravity/GravityGenerator.h"
#include "Components/ArrowComponent.h"

UGravityBoxEmitterComponent::UGravityBoxEmitterComponent()
{
	ShapeColor = FColor::Cyan;
}

FPrimitiveSceneProxy* UGravityBoxEmitterComponent::CreateSceneProxy()
{
	if (GravityGenerator)
	{
		return GravityGenerator->CreateSceneProxy(this);
	}
	return Super::CreateSceneProxy();
}

void UGravityBoxEmitterComponent::BeginPlay()
{
	OnComponentBeginOverlap.AddDynamic(this, &UGravityBoxEmitterComponent::HandleBeginComponentOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UGravityBoxEmitterComponent::HandleEndComponentOverlap);
}

#if WITH_EDITOR
void UGravityBoxEmitterComponent::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	if (e.GetPropertyName() == GET_MEMBER_NAME_CHECKED(UGravityBoxEmitterComponent, GravityGenerator))
	{
		if (GravityGenerator)
		{
			GravityGenerator->SetGravityEmitterOwner(this);
		}
	}
}
#endif

TArray<FArrowInfo> UGravityBoxEmitterComponent::CreateGravityArrows()
{
	TArray<FArrowInfo> Arrows;
	if (GravityGenerator)
	{
		for (int i = 0; i < ArrowsPerDim; i++)
		{
			for (int j = 0; j < ArrowsPerDim; j++)
			{
				for (int k = 0; k < ArrowsPerDim; k++)
				{
					// if (!(((i > 0 && i < (ArrowsPerDim - 1)) && (j > 0 && j < (ArrowsPerDim - 1)) && (k > 0 && k < (ArrowsPerDim - 1)))
					// 		|| (!(i > 0 && i < (ArrowsPerDim - 1)) && !(j > 0 && j < (ArrowsPerDim - 1)) && !(k > 0 && k < (ArrowsPerDim - 1)))))
					// 	continue;
					// if (ArrowCount >= Arrows.Num())
					// 	continue;

					float XAnchor = static_cast<float>(i) / static_cast<float>(ArrowsPerDim - 1) * 2.f - 1.f;
					float YAnchor = static_cast<float>(j) / static_cast<float>(ArrowsPerDim - 1) * 2.f - 1.f;
					float ZAnchor = static_cast<float>(k) / static_cast<float>(ArrowsPerDim - 1) * 2.f - 1.f;

					FVector EmitterExtent = GetScaledBoxExtent();
					FVector LocalLocation = EmitterExtent * FVector(XAnchor, YAnchor, ZAnchor);
					FVector WorldLocation = GetComponentLocation() + GetComponentRotation().RotateVector(LocalLocation);
					FVector Direction = GravityGenerator->GetGravity(WorldLocation);

					// UArrowComponent* Arrow = Arrows[ArrowCount];
					// if (!Direction.IsZero())
					// {
					// 	Arrow->SetVisibility(true);
					// 	Arrow->SetRelativeLocation(Location);
					// 	Arrow->SetWorldRotation(Direction.Rotation());
					// }
					// else
					// {
					// 	Arrow->SetVisibility(false);
					// }
					if (!Direction.IsZero())
					{
						Arrows.Add(FArrowInfo{ WorldLocation, Direction });
					}
				}
			}
		}
	}
	return Arrows;
}

void UGravityBoxEmitterComponent::HandleBeginComponentOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor*				 OtherActor,
	UPrimitiveComponent* OtherComp,
	int32				 OtherBodyIndex,
	bool				 bFromSweep,
	const FHitResult&	 SweepResult)
{
	if (UGravityReceiverComponent* ReceiverComponent = Cast<UGravityReceiverComponent>(OtherComp))
	{
		if (GravityGenerator)
		{
			ReceiverComponent->AddGravityInfo(GravityGenerator, Priority);
		}
	}
}

void UGravityBoxEmitterComponent::HandleEndComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (UGravityReceiverComponent* ReceiverComponent = Cast<UGravityReceiverComponent>(OtherComp))
	{
		if (GravityGenerator)
		{
			ReceiverComponent->RemoveGravityInfo(GravityGenerator);
		}
	}
}
