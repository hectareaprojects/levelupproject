// TODO: Copyright

#include "Gravity/GravityGenerator.h"
#include "Gravity/GravityReceiverComponent.h"
#include "Gravity/GravityBoxEmitterComponent.h"
#include "Gravity/GravitySceneProxy.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"

FVector UGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	return FVector::ZeroVector;
}

void UGravityGenerator::SetGravityEmitterOwner(UGravityBoxEmitterComponent* NewGravityEmitterOwner)
{
	GravityEmitterOwner = NewGravityEmitterOwner;
}

UGravityBoxEmitterComponent* UGravityGenerator::GetGravityEmitterOwner() const
{
	return GravityEmitterOwner;
}

FPrimitiveSceneProxy* UGravityGenerator::CreateSceneProxy(UGravityBoxEmitterComponent* InComponent)
{
	return new FGravityBoxEmitterSceneProxy(InComponent);
}

// -----------------------------------------------------------------------------------------------------

FVector UConstantGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	if (UGravityBoxEmitterComponent* GravityBoxEmitter = GetGravityEmitterOwner())
	{
		FRotator EmitterRotation = GravityBoxEmitter->GetComponentRotation();
		return EmitterRotation.RotateVector(GravityDirection);
	}
	return FVector::ZeroVector;
}

// -----------------------------------------------------------------------------------------------------

FVector UPointGravityGenerator::GetCenterOfGravity(bool& bSuccess) const
{
	if (UGravityBoxEmitterComponent* GravityBoxEmitter = GetGravityEmitterOwner())
	{
		FVector	 EmitterLocation = GravityBoxEmitter->GetComponentLocation();
		FRotator EmitterRotation = GravityBoxEmitter->GetComponentRotation();
		FVector	 EmitterExtent = GravityBoxEmitter->GetScaledBoxExtent();

		FVector Location = EmitterLocation + EmitterRotation.RotateVector(EmitterExtent * PointAnchor);

		bSuccess = true;
		return Location;
	}
	bSuccess = false;
	return FVector::ZeroVector;
}

FVector UPointAtractorGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	bool	bSuccess;
	FVector CenterOfGravity = GetCenterOfGravity(bSuccess);
	if (bSuccess)
	{
		FVector GravityDirection = (CenterOfGravity - Point).GetSafeNormal();
		return GravityDirection;
	}
	return FVector::ZeroVector;
}

FPrimitiveSceneProxy* UPointAtractorGravityGenerator::CreateSceneProxy(UGravityBoxEmitterComponent* InComponent)
{
	return new FGravityPointSceneProxy(InComponent, this);	
}

FVector UPointRetractorGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	bool	bSuccess;
	FVector CenterOfGravity = GetCenterOfGravity(bSuccess);

	if (bSuccess)
	{
		FVector GravityDirection = (Point - CenterOfGravity).GetSafeNormal();
		return GravityDirection;
	}
	return FVector::ZeroVector;
}

FPrimitiveSceneProxy* UPointRetractorGravityGenerator::CreateSceneProxy(UGravityBoxEmitterComponent* InComponent)
{
	return new FGravityPointSceneProxy(InComponent, this);
}

// -----------------------------------------------------------------------------------------------------

FVector ULineGravityGenerator::GetPointProjection(const FVector& Point, bool& bSuccess) const
{
	if (UGravityBoxEmitterComponent* GravityBoxEmitter = GetGravityEmitterOwner())
	{
		FVector	 EmitterLocation = GravityBoxEmitter->GetComponentLocation();
		FRotator EmitterRotation = GravityBoxEmitter->GetComponentRotation();
		FVector	 EmitterExtent = GravityBoxEmitter->GetScaledBoxExtent();

		FVector LineLocation = EmitterLocation + EmitterRotation.RotateVector(EmitterExtent * LineAnchor);
		FVector LineDirection = EmitterRotation.RotateVector(FVector::ForwardVector);

		FVector PointProjection = LineLocation + LineDirection * ((Point - LineLocation) | LineDirection);

		bSuccess = true;
		return PointProjection;
	}
	bSuccess = false;
	return FVector::ZeroVector;
}

FVector ULineAtractorGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	bool	bSuccess;
	FVector PointProjection = GetPointProjection(Point, bSuccess);

	if (bSuccess)
	{
		FVector GravityDirection = (PointProjection - Point).GetSafeNormal();
		return GravityDirection;
	}
	return FVector::ZeroVector;
}


FPrimitiveSceneProxy* ULineAtractorGravityGenerator::CreateSceneProxy(UGravityBoxEmitterComponent* InComponent)
{
	return new FGravityLineSceneProxy(InComponent, this);	
}

FVector ULineRetractorGravityGenerator::GetGravity_Implementation(const FVector& Point) const
{
	bool	bSuccess;
	FVector PointProjection = GetPointProjection(Point, bSuccess);

	if (bSuccess)
	{
		FVector GravityDirection = (Point - PointProjection).GetSafeNormal();
		return GravityDirection;
	}
	return FVector::ZeroVector;
}


FPrimitiveSceneProxy* ULineRetractorGravityGenerator::CreateSceneProxy(UGravityBoxEmitterComponent* InComponent)
{
	return new FGravityLineSceneProxy(InComponent, this);	
}
