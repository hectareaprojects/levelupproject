// TODO: Copyright

#include "Gravity/GravityPhysicsComponent.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
#include "Components/SceneComponent.h"

UGravityPhysicsComponent::UGravityPhysicsComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	GravityDirection = FVector(0.f, 0.f, -1.f);
	GravityScale = 1.f;
}

void UGravityPhysicsComponent::UpdateRotation(USceneComponent* UpdatedComponent)
{
	const FVector UpVector = UpdatedComponent->GetUpVector();
	const FVector DesiredUpVector = GravityDirection * -1.f;

	const float AngleDiff = FMath::Acos(UpVector | DesiredUpVector);
	FVector RotationAxis = UpVector ^ DesiredUpVector;
	if (!RotationAxis.IsNearlyZero())
	{
		if (RotationAxis.Normalize())
		{
			FVector OldComponentLocation = UpdatedComponent->GetComponentLocation();
			UpdatedComponent->AddRelativeRotation(FQuat(RotationAxis, AngleDiff));
			FVector NewComponentLocation = UpdatedComponent->GetComponentLocation();
			AActor* Owner = UpdatedComponent->GetOwner();
			if (Owner)
			{
				// Owner->AddActorWorldOffset(OldComponentLocation - NewComponentLocation);
			}
		}
	}
}

void UGravityPhysicsComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (AActor* Owner = GetOwner())
	{
		if (UPrimitiveComponent* PrimitiveComponent = Cast<UPrimitiveComponent>(Owner->GetRootComponent()))
		{
			// UpdateRotation(PrimitiveComponent);
			PrimitiveComponent->AddForce(GravityDirection * GravityScale, NAME_None, true);
		}
	}
}
