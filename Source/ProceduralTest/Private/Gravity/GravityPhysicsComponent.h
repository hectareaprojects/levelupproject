// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GravityPhysicsComponent.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UGravityPhysicsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityPhysicsComponent")
	FVector GravityDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityPhysicsComponent")
	float GravityScale;

public:
	UGravityPhysicsComponent();

private:
	void UpdateRotation(class USceneComponent* UpdatedComponent);	

protected:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
