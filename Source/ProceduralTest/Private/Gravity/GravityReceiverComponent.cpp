// TODO: Copyright

#include "Gravity/GravityReceiverComponent.h"
#include "Gravity/GravityGenerator.h"
#include "GameFramework/Actor.h"

UGravityReceiverComponent::UGravityReceiverComponent()
{
	SphereRadius = 5.f;
}

FVector UGravityReceiverComponent::GetGravity() const
{
	if (!GravityInfos.IsEmpty())
	{
		FVector CurrentGravity = GravityInfos[0].GravityGenerator->GetGravity(GetComponentLocation());
		int		CurrentPriority = GravityInfos[0].Priority;
		for (int i = 1; i < GravityInfos.Num(); i++)
		{
			const FGravityInfo& CurrentGravityInfo = GravityInfos[i];
			if (CurrentGravityInfo.Priority == CurrentPriority)
			{
				CurrentGravity += CurrentGravityInfo.GravityGenerator->GetGravity(GetComponentLocation());
			}
			else if (CurrentGravityInfo.Priority > CurrentPriority)
			{
				FVector NewGravityVector = CurrentGravityInfo.GravityGenerator->GetGravity(GetComponentLocation());
				if (!NewGravityVector.IsZero())
				{
					CurrentGravity = NewGravityVector;
					CurrentPriority = CurrentGravityInfo.Priority;
				}
			}
		}
		if (!CurrentGravity.IsZero())
		{
			LastCalculatedGravity = CurrentGravity.GetSafeNormal();
		}
	}
	return LastCalculatedGravity;
}

void UGravityReceiverComponent::SetDefaultGravity(const FVector& DefaultGravity)
{
	LastCalculatedGravity = DefaultGravity;
}

void UGravityReceiverComponent::AddGravityInfo(UGravityGenerator* GravityInfo, int Priority)
{
	if (GravityInfo)
	{
		GravityInfos.Add(FGravityInfo{ Priority, GravityInfo });
	}
}

void UGravityReceiverComponent::RemoveGravityInfo(UGravityGenerator* GravityGenerator)
{
	if (GravityGenerator)
	{
		GravityInfos.RemoveAllSwap([GravityGenerator](const FGravityInfo& GravityInfo) {
			return GravityInfo.GravityGenerator == GravityGenerator;
		});
	}
}
