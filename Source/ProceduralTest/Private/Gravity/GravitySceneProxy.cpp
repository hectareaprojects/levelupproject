// TODO: Copyright

#include "Gravity/GravitySceneProxy.h"
#include "Gravity/GravityBoxEmitterComponent.h"
#include "Gravity/GravityGenerator.h"
#include "SceneManagement.h"

FGravityBoxEmitterSceneProxy::FGravityBoxEmitterSceneProxy(UGravityBoxEmitterComponent* InComponent)
	: FPrimitiveSceneProxy(InComponent)
	, bDrawOnlyIfSelected(InComponent->bDrawOnlyIfSelected)
	, BoxExtents(InComponent->BoxExtent)
	, BoxColor(InComponent->ShapeColor)
	, LineThickness(InComponent->LineThickness)
	, Arrows(InComponent->CreateGravityArrows())
{
	bWillEverBeLit = false;
}

void FGravityBoxEmitterSceneProxy::DrawLineArrow(FPrimitiveDrawInterface* PDI, const FVector& Start, const FVector& End, const FColor& Color, float Mag) const
{
	// draw a pretty arrow
	FVector		Dir = End - Start;
	const float DirMag = Dir.Size();
	Dir /= DirMag;
	FVector YAxis, ZAxis;
	Dir.FindBestAxisVectors(YAxis, ZAxis);
	FMatrix ArrowTM(Dir, YAxis, ZAxis, Start);
	DrawDirectionalArrow(PDI, ArrowTM, Color, DirMag, Mag, SDPG_World, 2.f);
	// DrawDirectionalArrow(PDI, InLocalToWorld, Color, DirMag, Mag, SDPG_World, 5.f);
}

void FGravityBoxEmitterSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const
{
	QUICK_SCOPE_CYCLE_COUNTER(STAT_BoxSceneProxy_GetDynamicMeshElements);

	const FMatrix& LocalToWorldM = GetLocalToWorld();

	for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
	{
		if (VisibilityMap & (1 << ViewIndex))
		{
			const FSceneView* View = Views[ViewIndex];

			// const FLinearColor DrawColor = GetViewSelectionColor(BoxColor, *View, IsSelected(), IsHovered(), false, IsIndividuallySelected());
			const FLinearColor DrawColor = FLinearColor::Blue;

			FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);
			DrawOrientedWireBox(PDI, LocalToWorldM.GetOrigin(), LocalToWorldM.GetScaledAxis(EAxis::X), LocalToWorldM.GetScaledAxis(EAxis::Y), LocalToWorldM.GetScaledAxis(EAxis::Z), BoxExtents, DrawColor, SDPG_World, LineThickness);
			// DrawDirectionalArrow(PDI, LocalToWorld, FLinearColor::Blue, 10.f, 5.f, SDPG_World);
			for (const FArrowInfo& ArrowInfo : Arrows)
			{
				DrawLineArrow(PDI, ArrowInfo.Location, ArrowInfo.Location + ArrowInfo.Direction * 50.f, FColor::Cyan, 10.f);
			}
		}
	}
}

FPrimitiveViewRelevance FGravityBoxEmitterSceneProxy::GetViewRelevance(const FSceneView* View) const
{
	const bool bProxyVisible = !bDrawOnlyIfSelected || IsSelected();
	// Should we draw this because collision drawing is enabled, and we have collision
	const bool				bShowForCollision = View->Family->EngineShowFlags.Collision && IsCollisionEnabled();
	FPrimitiveViewRelevance Result;
	Result.bDrawRelevance = (IsShown(View) && bProxyVisible) || bShowForCollision;
	Result.bDynamicRelevance = true;
	Result.bShadowRelevance = IsShadowCast(View);
	Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
	return Result;
}

SIZE_T FGravityBoxEmitterSceneProxy::GetTypeHash() const
{
	static size_t UniquePointer;
	return reinterpret_cast<size_t>(&UniquePointer);
}

uint32 FGravityBoxEmitterSceneProxy::GetMemoryFootprint(void) const
{
	return (sizeof(*this) + GetAllocatedSize());
}

uint32 FGravityBoxEmitterSceneProxy::GetAllocatedSize(void) const
{
	return (FPrimitiveSceneProxy::GetAllocatedSize());
}

// -------------------------------------------------------------------------------------------------------------

FGravityPointSceneProxy::FGravityPointSceneProxy(UGravityBoxEmitterComponent* InComponent, class UPointGravityGenerator* GravityGenerator)
	: FGravityBoxEmitterSceneProxy(InComponent)
	, PointLocation(FVector::ZeroVector)
	, SphereRadius(GravityGenerator->SphereRadius)
	, bDrawVisualHelp(GravityGenerator->bDrawVisualHelp)
{
	bool bSuccess;
	PointLocation = GravityGenerator->GetCenterOfGravity(bSuccess);
}

void FGravityPointSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& Views,
	const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const
{
	FGravityBoxEmitterSceneProxy::GetDynamicMeshElements(Views, ViewFamily, VisibilityMap, Collector);

	const FMatrix& LocalToWorldM = GetLocalToWorld();

	for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
	{
		if (VisibilityMap & (1 << ViewIndex))
		{
			const FSceneView* View = Views[ViewIndex];

			// const FLinearColor DrawColor = GetViewSelectionColor(BoxColor, *View, IsSelected(), IsHovered(), false, IsIndividuallySelected());
			const FLinearColor DrawColor = FLinearColor::Blue;

			FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);

			DrawWireSphere(PDI, PointLocation, FLinearColor::Red, 3.0, 10, SDPG_World, 2.f);

			if (bDrawVisualHelp)
			{
				const int32 SphereSides = FMath::Clamp<int32>(SphereRadius / 4.f, 16, 64);
				DrawWireSphere(PDI, PointLocation, FLinearColor::Yellow, SphereRadius, SphereSides, SDPG_World, 1.f);
			}
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

FGravityLineSceneProxy::FGravityLineSceneProxy(UGravityBoxEmitterComponent* InComponent, class ULineGravityGenerator* GravityGenerator)
	: FGravityBoxEmitterSceneProxy(InComponent)
	, EmitterLocation(InComponent->GetComponentLocation())
	, EmitterRotation(InComponent->GetComponentRotation())
	, EmitterExtent(InComponent->GetScaledBoxExtent())
	, LineAnchor(GravityGenerator->LineAnchor)
	, NumCircles(GravityGenerator->NumCircles)
	, CircleRadius(GravityGenerator->CircleRadius)
	, bDrawVisualHelp(GravityGenerator->bDrawVisualHelp)
{
}

void FGravityLineSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const
{
	FGravityBoxEmitterSceneProxy::GetDynamicMeshElements(Views, ViewFamily, VisibilityMap, Collector);

	const FMatrix& LocalToWorldM = GetLocalToWorld();

	FVector LineLocation = EmitterLocation + EmitterRotation.RotateVector(EmitterExtent * LineAnchor);
	FVector LineDirection = EmitterRotation.RotateVector(FVector::ForwardVector);
	float	LineLength = EmitterExtent.GetMax() * 2.0;

	for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
	{
		if (VisibilityMap & (1 << ViewIndex))
		{
			const FSceneView* View = Views[ViewIndex];

			// const FLinearColor DrawColor = GetViewSelectionColor(BoxColor, *View, IsSelected(), IsHovered(), false, IsIndividuallySelected());
			const FLinearColor DrawColor = FLinearColor::Blue;

			FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);

			FVector Start = LineLocation - LineDirection * LineLength * 0.5;
			FVector End = LineLocation + LineDirection * LineLength * 0.5;
			DrawLineArrow(PDI, Start, End, FColor::Red, 0);

			if (bDrawVisualHelp && NumCircles >= 3)
			{
				FVector CircleYAxis;
				FVector CircleZAxis;
				LineDirection.FindBestAxisVectors(CircleYAxis, CircleZAxis);
				const int32 CircleSides = FMath::Clamp<int32>(CircleRadius / 4.f, 16, 64);
				for (int i = 0; i < NumCircles; i++)
				{
					float CircleOffsetMultiplier = static_cast<float>(i) / static_cast<float>(NumCircles - 1);
					FVector CircleLocation = Start + LineDirection * LineLength * CircleOffsetMultiplier;
					DrawCircle(PDI, CircleLocation, CircleYAxis, CircleZAxis, FLinearColor::Yellow, CircleRadius, CircleSides, SDPG_World, 1.f);
				}
			}
		}
	}
}
