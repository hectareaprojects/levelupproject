// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "PrimitiveSceneProxy.h"
// #include "GravitySceneProxy.generated.h"

struct FArrowInfo;

class FGravityBoxEmitterSceneProxy : public FPrimitiveSceneProxy
{
private:
	const uint32			 bDrawOnlyIfSelected : 1;
	const FVector			 BoxExtents;
	const FColor			 BoxColor;
	const float				 LineThickness;
	const TArray<FArrowInfo> Arrows;

public:
	FGravityBoxEmitterSceneProxy(class UGravityBoxEmitterComponent* InComponent);

	void DrawLineArrow(FPrimitiveDrawInterface* PDI, const FVector& Start,
		const FVector& End, const FColor& Color, float Mag) const;

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily,
		uint32 VisibilityMap, FMeshElementCollector& Collector) const override;

	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override;

	SIZE_T		   GetTypeHash() const override;
	virtual uint32 GetMemoryFootprint(void) const override;
	uint32		   GetAllocatedSize(void) const;
};

// -------------------------------------------------------------------------------------------------------------

class FGravityPointSceneProxy : public FGravityBoxEmitterSceneProxy
{
private:
	FVector PointLocation;
	float SphereRadius;
	bool bDrawVisualHelp;

public:
	FGravityPointSceneProxy(UGravityBoxEmitterComponent* InComponent, class UPointGravityGenerator* GravityGenerator);

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily,
		uint32 VisibilityMap, FMeshElementCollector& Collector) const override;
};

// -------------------------------------------------------------------------------------------------------------

class FGravityLineSceneProxy : public FGravityBoxEmitterSceneProxy
{
private:
	FVector EmitterLocation;
	FRotator EmitterRotation;
	FVector EmitterExtent;
	FVector LineAnchor;
	int NumCircles;
	float CircleRadius;
	bool bDrawVisualHelp;

public:
	FGravityLineSceneProxy(UGravityBoxEmitterComponent* InComponent, class ULineGravityGenerator* GravityGenerator);

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily,
		uint32 VisibilityMap, FMeshElementCollector& Collector) const override;
};
