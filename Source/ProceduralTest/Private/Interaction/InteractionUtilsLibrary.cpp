// TODO: Copyright

#include "Interaction/InteractionUtilsLibrary.h"
#include "Interaction/LookAtBoxComponent.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"

static void PrintVector(FString Text, FVector Vector)
{
	UE_LOG(LogTemp, Warning, TEXT("%s, (%g,%g,%g)"), *Text, Vector.X, Vector.Y, Vector.Z);
}

void UInteractionUtilsLibrary::AdjustLookAtBoxToActorBounds(ULookAtBoxComponent* LookAtBox, AActor* Item)
{
	if (LookAtBox && Item)
	{
		FVector Origin;
		FVector BoxExtent;
		Item->GetActorBounds(false, Origin, BoxExtent);
		PrintVector(TEXT("ItemBefore"), Item->GetActorLocation());
		LookAtBox->SetBoxExtent(BoxExtent);
		FVector RelativeCenter = Origin - LookAtBox->Bounds.GetBox().GetCenter();
		//Item->GetRootComponent()->SetRelativeLocation(-RelativeCenter*100.f);
		Item->AddActorLocalOffset(-RelativeCenter);
		PrintVector(TEXT("ItemAfter"), Item->GetActorLocation());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("The LookAtBox couldn't adjust its size to the item!"));
	}
}
