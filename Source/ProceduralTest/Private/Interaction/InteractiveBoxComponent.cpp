// Fill out your copyright notice in the Description page of Project Settings.

#include "Interaction/InteractiveBoxComponent.h"
#include "Interaction/LookAtBoxComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EnhancedInputLibrary.h"
#include "EnhancedInput/Public/EnhancedInputSubsystems.h"
#include "Blueprint/UserWidget.h"
#include "InputMappingContext.h"

UInteractiveBoxComponent* UInteractiveBoxComponent::ScreenOwner = nullptr;

UInteractiveBoxComponent::UInteractiveBoxComponent()
	: InteractingActor(nullptr), bInside(false), bDisplayed(false), bEnabled(true)
{

	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	SetCollisionProfileName(TEXT("Trigger"));
}

static float GetMaxDistanceFromLookAtBoxComponent(const UInteractiveBoxComponent* InteractiveBox, const ULookAtBoxComponent* LookAtBox)
{
	return FMath::Sqrt(3.0f) * 0.5f * (InteractiveBox->GetUnscaledBoxExtent().GetMax() + LookAtBox->GetUnscaledBoxExtent().GetMax())
		+ LookAtBox->GetRelativeLocation().Length();
}

void UInteractiveBoxComponent::Show()
{

	// If InteractiveBox is not enabled or is already displayed, return.
	if (!bEnabled || bDisplayed)
		return;

	// If there is already an screen owner, hide it.
	if (IsScreenOwned())
		ScreenOwner->Hide();

	// Set this InteractiveBox as the screen owner.
	SetScreenOwner();

	// Enable input
	if (InputMapping)
	{
		if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetOwner(), 0))
		{
			if (ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer())
			{
				if (UEnhancedInputLocalPlayerSubsystem* InputSystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
				{
					GetOwner()->EnableInput(PlayerController);
					FModifyContextOptions ContextOptions;
					ContextOptions.bIgnoreAllPressedKeysUntilRelease = false;
					InputSystem->AddMappingContext(InputMapping, 0, ContextOptions);
				}
			}
		}
	}

	// Show widget
	if (Widget)
		Widget->AddToViewport();

	// Now the widget is shown
	bDisplayed = true;
}

void UInteractiveBoxComponent::Hide()
{

	// If the InteractiveBox is already hidden, return.
	if (!bDisplayed)
		return;

	// Remove InputMappingContext
	if (InputMapping)
	{
		if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetOwner(), 0))
		{
			if (ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer())
			{
				if (UEnhancedInputLocalPlayerSubsystem* InputSystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
				{
					GetOwner()->DisableInput(PlayerController);
					FModifyContextOptions ContextOptions;
					ContextOptions.bIgnoreAllPressedKeysUntilRelease = false;
					ContextOptions.bForceImmediately = false;
					InputSystem->RemoveMappingContext(InputMapping, ContextOptions);
				}
			}
		}
	}

	// Remove ScreenOwner
	ScreenOwner = nullptr;

	// Hide widget
	if (Widget)
		Widget->RemoveFromParent();

	// Now this component is disabled
	bDisplayed = false;
}

bool UInteractiveBoxComponent::IsScreenOwner() const
{
	return this == ScreenOwner;
}

bool UInteractiveBoxComponent::IsScreenOwned()
{
	return UInteractiveBoxComponent::ScreenOwner != nullptr;
}

bool UInteractiveBoxComponent::CanBeScreenOwner() const
{
	return IsScreenOwner() || !IsScreenOwned() || (IsScreenOwned() && (Priority > ScreenOwner->Priority));
}

void UInteractiveBoxComponent::SetScreenOwner()
{
	ScreenOwner = this;
}

void UInteractiveBoxComponent::BeginPlay()
{

	Super::BeginPlay();

	// Check if we have a valid WidgetClass
	if (!WidgetClass && GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("WidgetClass is not a valid class."));

	// Create the widget
	if (WidgetClass)
		Widget = NewObject<UUserWidget>(this, WidgetClass);

	// Link this component with every immediate child LookAtBoxComponent
	TArray<USceneComponent*> AttachedChildren = GetAttachChildren();
	for (USceneComponent* AttachedChild : AttachedChildren)
	{
		if (AttachedChild->GetClass()->IsChildOf<ULookAtBoxComponent>())
		{
			LookAtBoxComponents.Add(Cast<ULookAtBoxComponent>(AttachedChild));
		}
	}

	// Modify MaxDistFromLookAtBox
	float PossiblyMaxDistance = 0.0f;
	for (const ULookAtBoxComponent* LookAtBoxComponent : LookAtBoxComponents)
	{
		float NewPossiblyMaxDistance = GetMaxDistanceFromLookAtBoxComponent(this, LookAtBoxComponent);
		if (NewPossiblyMaxDistance > PossiblyMaxDistance)
		{
			PossiblyMaxDistance = NewPossiblyMaxDistance;
		}
	}
	MaxDistFromLookAtBox = PossiblyMaxDistance;

	// Register overlap event
	OnComponentBeginOverlap.AddDynamic(this, &UInteractiveBoxComponent::OnBoxBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UInteractiveBoxComponent::OnBoxEndOverlap);

	// Initialize ScreenOwner
	ScreenOwner = nullptr;
}

void UInteractiveBoxComponent::TickComponent(float Delta, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (CameraComponent && CanBeScreenOwner() && CanInteract())
	{

		// Calculate the LineTrace
		FHitResult HitResult;
		FVector Start = CameraComponent->GetComponentLocation();
		FVector End = Start + MaxDistFromLookAtBox * CameraComponent->GetForwardVector();
		FCollisionQueryParams CollisionParams(FName("Looking at box"));

		// Enable the widget if ViewTraget is looking at a LookAtBox
		for (ULookAtBoxComponent* LookAtBoxComponent : LookAtBoxComponents)
		{
			if (LookAtBoxComponent->IsEnabled() && LookAtBoxComponent->LineTraceComponent(HitResult, Start, End, CollisionParams))
			{
				Show();
				return;
			}
		}

		// Otherwise, hide the widget
		Hide();
	}
	else if (IsScreenOwner())
	{
		Hide();
	}
}

void UInteractiveBoxComponent::Enable()
{

	// If InteractiveBox is already enabled, return
	if (bEnabled)
		return;

	// Enable tick
	SetComponentTickEnabled(true);

	// Register overlap event
	OnComponentBeginOverlap.AddDynamic(this, &UInteractiveBoxComponent::OnBoxBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UInteractiveBoxComponent::OnBoxEndOverlap);

	// Enable this component
	bEnabled = true;
}

void UInteractiveBoxComponent::Disable()
{

	// If InteractiveBox is already disabled, return
	if (!bEnabled)
		return;

	// Disbale tick
	SetComponentTickEnabled(false);

	// Remove delegates
	OnComponentBeginOverlap.RemoveDynamic(this, &UInteractiveBoxComponent::OnBoxBeginOverlap);
	OnComponentEndOverlap.RemoveDynamic(this, &UInteractiveBoxComponent::OnBoxEndOverlap);

	// Hide widget
	Hide();

	// Disable this component
	bEnabled = false;
}

bool UInteractiveBoxComponent::IsEnabled() const
{
	return bEnabled;
}

bool UInteractiveBoxComponent::CanInteract_Implementation() const
{
	return true;
}

AActor* UInteractiveBoxComponent::GetInteractingActor() const
{
	return InteractingActor;
}

void UInteractiveBoxComponent::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	// If the overlapped actor is the view target, enable the tick
	AActor* ViewTarget = UGameplayStatics::GetPlayerCameraManager(GetOwner(), 0)->GetViewTarget();
	if (OtherActor == ViewTarget)
	{
		bInside = true;
		SetComponentTickEnabled(true);
		CameraComponent = OtherActor->FindComponentByClass<UCameraComponent>();
		InteractingActor = ViewTarget;
	}
}

void UInteractiveBoxComponent::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	// If the overlapped actor is the view target, disbale the tick and hide the widget
	if (APlayerCameraManager* PlayerCameraManager = UGameplayStatics::GetPlayerCameraManager(GetOwner(), 0))
	{
		if (OtherActor == PlayerCameraManager->GetViewTarget())
		{
			bInside = false;
			SetComponentTickEnabled(false);
			InteractingActor = nullptr;
			Hide();
		}
	}
}
