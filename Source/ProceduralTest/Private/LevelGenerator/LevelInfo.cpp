// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelGenerator/LevelInfo.h"
#include "Kismet/KismetMathLibrary.h"
#include "LevelGenerator/LevelGeneratedTransform.h"
#include "LevelGenerator/LevelEntryComponent.h"
#include "LevelGenerator/LevelStartComponent.h"
#include "Engine/LevelStreamingDynamic.h"
#include "Engine/World.h"
// #include "LevelInstance/LevelInstanceActor.h"

#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

// Sets default values
ALevelInfo::ALevelInfo()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
}

// Called when the game starts or when spawned
void ALevelInfo::BeginPlay()
{
	Super::BeginPlay();

	// Asignamos el radio de accion de las esferas
	// ToActivateRoomSphere->SetSphereRadius(ActivateSphereRadius);
	// ToDeactivateRoomSphere->SetSphereRadius(DeactivateSphereRadius);

	// Desactivamos la colision de las esferas para el generador.
	// ToActivateRoomSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	// ToDeactivateRoomSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Obtenemos los componentes que describen los puntos de union.
	GetComponents<ULevelEntryComponent>(this->Entries, false);

	// Obtenemos los componentes que describen los puntos de spawn.
	GetComponents<ULevelStartComponent>(this->Starts, false);
}

// void ALevelInfo::OnActivateSphereOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
// 							int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){

//   ACharacter* player = Cast<ACharacter>(OtherActor);

//   // Si el actor es el jugador...
//   if (player && SpawnedLevel){
//     // Activamos la sala
//     SpawnedLevel->SetActorHiddenInGame(false);
//     SpawnedLevel->SetActorEnableCollision(true);
//     SpawnedLevel->SetActorTickEnabled(true); //(ver mejor esto)
//   }

// }

// void ALevelInfo::OnDeactivateSphereOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex){

//   ACharacter* player = Cast<ACharacter>(OtherActor);

//   // Si el actor es el jugador...
//   if (player && SpawnedLevel){
//     // Desactivamos la sala
//     SpawnedLevel->SetActorHiddenInGame(true);
//     SpawnedLevel->SetActorEnableCollision(false);
//     SpawnedLevel->SetActorTickEnabled(false); //(ver mejor esto)
//   }

// }

template <typename T>
static TArray<T> getShuffledArray2(const TArray<T>& srcArray)
{

	std::vector<size_t> shuffledIndices;
	unsigned			seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffledIndices.resize(srcArray.Num());
	for (size_t i = 0; i < shuffledIndices.size(); i++)
		shuffledIndices[i] = i;
	std::shuffle(shuffledIndices.begin(), shuffledIndices.end(), std::default_random_engine(seed));

	TArray<T> shuffledArray;
	for (size_t i = 0; i < shuffledIndices.size(); i++)
	{
		shuffledArray.Add(srcArray[shuffledIndices[i]]);
	}

	return shuffledArray;
}

bool ALevelInfo::GetMatchLevelTransform(const TArray<ALevelInfo*>& FixedLevels, const TArray<ULevelEntryComponent*>& FixedEntries, FTransform& Result,
	TArray<ULevelEntryComponent*>& UsedEntries, TArray<ULevelEntryComponent*>& NewOpenEntries)
{
	TArray<ULevelEntryComponent*> ShuffledEntries = getShuffledArray2<ULevelEntryComponent*>(Entries);
	TArray<ULevelEntryComponent*> ShuffledFixedEntries = getShuffledArray2<ULevelEntryComponent*>(FixedEntries);
	for (ULevelEntryComponent* movArrow : ShuffledEntries)
	{
		for (ULevelEntryComponent* FixedArrow : ShuffledFixedEntries)
		{
			if (FixedArrow->HasSuitableTags(*movArrow))
			{
				FTransform levelTransform = FLevelGeneratedTransform::getMatchTransform(FixedArrow->GetComponentTransform(), movArrow->GetRelativeTransform());

				SetActorTransform(levelTransform, false, nullptr, ETeleportType::ResetPhysics);

				bool valid = true;
				for (ALevelInfo* fixedLevel2 : FixedLevels)
				{
					if (this->IsOverlappingActor(fixedLevel2))
					{
						valid = false;
						break;
					}
				}

				TArray<ULevelEntryComponent*> possibleUsedEntries;
				TArray<ULevelEntryComponent*> possibleOpenEntries;
				for (size_t i = 0; (i < Entries.Num()) && valid; i++)
				{
					ULevelEntryComponent* movEntry = Entries[i];
					bool				  isMovEntryOpen = true;
					for (size_t j = 0; (j < FixedEntries.Num()) && valid; j++)
					{
						ULevelEntryComponent* fixEntry = FixedEntries[j];
						if (fixEntry->IsNear(*movEntry))
						{
							if (fixEntry->HasSuitableTags(*movEntry) && fixEntry->HasOppositeDirection(*movEntry, 0.1f))
							{
								possibleUsedEntries.Add(FixedEntries[j]);
								isMovEntryOpen = false;
								break;
							}
							else
							{
								valid = false;
							}
						}
					}
					if (isMovEntryOpen)
					{
						possibleOpenEntries.Add(movEntry);
					}
				}
				if (valid)
				{
					// DrawDebugDirectionalArrow(GetWorld(), FixedArrow->GetComponentLocation()+FixedArrow->GetUpVector()*100.f, levelTransform.GetLocation()+levelTransform.GetRotation().GetUpVector()*100.f, 5000.f, FColor::Black,true,-1.f,0,8.f);
					// UE_LOG(LogTemp, Warning, TEXT("Colocado: %s"),*movArrow->GetOwner()->GetHumanReadableName());
					UsedEntries = MoveTemp(possibleUsedEntries);
					NewOpenEntries = MoveTemp(possibleOpenEntries);
					Result = levelTransform;
					return true;
				}
			}
		}
	}

	return false;
}

void ALevelInfo::SpawnLevel()
{

	check(SpawnableLevels.Num() > 0);
	// if (SpawnableLevels.Num() == 0){
	//   UE_LOG(LogTemp, Warning, TEXT("No levels assigned to this level info!"));
	//   return;
	// }

	if (!SpawnedLevel)
	{
		// Spawneamos el nivel
		int32 randIndex = FMath::RandRange(0, SpawnableLevels.Num() - 1);
		// TSoftObjectPtr<UWorld> Level = SpawnableLevels[randIndex].LoadSynchronous();
		TSubclassOf<AActor> LevelClass = SpawnableLevels[randIndex];
		FTransform			transform = GetActorTransform();

		// Spawneamos de verdad el nivel
		SpawnedLevel = GetWorld()->SpawnActor<ALevelInstance>(LevelClass, transform);

		// Bloqueamos hasta que el nivel se cargue completamente (esto rompe los BeginPlay)
		// ULevelInstanceSubsystem* LevelInstanceSubsystem = GetWorld()->GetSubsystem<ULevelInstanceSubsystem>();
		// LevelInstanceSubsystem->BlockLoadLevelInstance(SpawnedLevel);

		if (!SpawnedLevel)
		{
			UE_LOG(LogTemp, Warning, TEXT("ALevelInfo::SpawnLevel - Error: The level did not spawn!"));
			return;
		}

		// Lo pegamos a este actor (no entiendo la diferencia de los parametros)
		// FAttachmentTransformRules attachmentRules(EAttachmentRule::KeepWorld,true);
		// this->LevelSpawned->AttachToActor(this,attachmentRules);

		// Desactivamos los overlap de todos
		TArray<UPrimitiveComponent*> primitiveComponents;
		GetComponents<UPrimitiveComponent>(primitiveComponents, false);
		for (UPrimitiveComponent* primComponent : primitiveComponents)
		{
			primComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		// Activamos los overlaps de las esferas
		// ToActivateRoomSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		// ToDeactivateRoomSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		// ToActivateRoomSphere->OnComponentBeginOverlap.AddDynamic(this,&ALevelInfo::OnActivateSphereOverlapBegin);
		// ToDeactivateRoomSphere->OnComponentEndOverlap.AddDynamic(this,&ALevelInfo::OnDeactivateSphereOverlapEnd);
	}
}

void ALevelInfo::DestroyLevel()
{
	if (SpawnedLevel)
	{
		// SpawnedLevel->SetIsRequestingUnloadAndRemoval(true);
		SpawnedLevel->Destroy();
		SpawnedLevel = nullptr;
	}
}

TArray<FTransform> ALevelInfo::GetInitialTransforms()
{

	TArray<FTransform> levelStarts;
	for (ULevelStartComponent* levelStart : Starts)
	{
		levelStarts.Push(levelStart->GetComponentTransform());
	}
	return levelStarts;
}
