// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelGenerator/LevelPlayerStart.h"
#include "Components/ArrowComponent.h"

// Sets default values
ALevelPlayerStart::ALevelPlayerStart()
{
	UArrowComponent* ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("RootComponent"));
	ArrowComponent->ArrowColor = FColor::Blue;

	RootComponent = ArrowComponent;
}

