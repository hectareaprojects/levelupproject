// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelGenerator/LevelSpace.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"

ALevelBoxSpace::ALevelBoxSpace()
{
	SpaceComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	RootComponent = SpaceComponent;
}

// ----------------------------------------------------------------------------------------------------------

ALevelCapsuleSpace::ALevelCapsuleSpace()
{
	SpaceComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
	RootComponent = SpaceComponent;
}

// ----------------------------------------------------------------------------------------------------------

ALevelSphereSpace::ALevelSphereSpace()
{
	SpaceComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SpaceComponent;
}
