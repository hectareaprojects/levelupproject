// TODO: Copyright

#include "Math/MathUtilsLibrary.h"

float UMathUtilsLibrary::Max(const TArray<float>& X)
{
	check(!X.IsEmpty());

	float MaxValue = X[0];
	for (int i = 1; i < X.Num(); i++)
	{
		MaxValue = MaxValue < X[i] ? X[i] : MaxValue;
	}

	return MaxValue;
}

float UMathUtilsLibrary::Min(const TArray<float>& X)
{
	check(!X.IsEmpty());

	float MinValue = X[0];
	for (int i = 1; i < X.Num(); i++)
	{
		MinValue = MinValue > X[i] ? X[i] : MinValue;
	}

	return MinValue;
}

float UMathUtilsLibrary::Range(const TArray<float>& X)
{
	if (X.IsEmpty())
	{
		return 0;
	}
	return UMathUtilsLibrary::Max(X) - UMathUtilsLibrary::Min(X);
}

float UMathUtilsLibrary::Mean(const TArray<float>& X)
{
	if (!X.IsEmpty())
	{
		float Sum = 0;
		for (float x : X)
		{
			Sum += x;
		}
		return Sum / static_cast<float>(X.Num());
	}
	return 0.f;
}

FVector UMathUtilsLibrary::MeanVector(const TArray<FVector>& X)
{
	if (!X.IsEmpty())
	{
		FVector Sum = FVector::ZeroVector;
		for (const FVector& V : X)
		{
			Sum += V;
		}
		return Sum / static_cast<float>(X.Num());
	}

	return FVector::ZeroVector;
}

float UMathUtilsLibrary::Variance(const TArray<float>& X)
{
	float XMean = UMathUtilsLibrary::Mean(X);

	float Sum = 0;
	for (float x : X)
	{
		Sum += x * x;
	}
	float X2Mean = Sum / static_cast<float>(X.Num());

	return X2Mean - XMean * XMean;
}

float UMathUtilsLibrary::Covariance(const TArray<float>& X, const TArray<float>& Y)
{
	float XMean = UMathUtilsLibrary::Mean(X);
	float YMean = UMathUtilsLibrary::Mean(Y);

	float Sum = 0;
	int MinSize = std::min(X.Num(), Y.Num());
	for (int i = 0; i < MinSize; i++)
	{
		Sum += X[i] * Y[i];
	}
	float XYMean = Sum / MinSize;

	return XYMean - XMean * YMean;
}

void UMathUtilsLibrary::RegressionLine(const TArray<float>& X, const TArray<float>& Y, float& OutXOrigin, float& OutYOrigin, float& OutSlope)
{
	OutXOrigin = UMathUtilsLibrary::Mean(X);
	OutYOrigin = UMathUtilsLibrary::Mean(Y);
	float XYCov = UMathUtilsLibrary::Covariance(X, Y);
	float XVar = UMathUtilsLibrary::Variance(X);
	OutSlope = XYCov / XVar;
}

void UMathUtilsLibrary::RegressionLine2D(const TArray<FVector2D>& P, FVector2D& OutOrigin, FVector2D& OutDirection)
{
	// Transform data
	TArray<float> X;
	TArray<float> Y;
	for (const FVector2D& V : P)
	{
		X.Add(V.X);
		Y.Add(V.Y);
	}

	// Choose between Y over X or X over Y and calculate the line
	float XRange = UMathUtilsLibrary::Range(X);
	float YRange = UMathUtilsLibrary::Range(Y);

	float XOrigin;
	float YOrigin;
	float XDirection;
	float YDirection;
	if (XRange > YRange)
	{
		UMathUtilsLibrary::RegressionLine(X, Y, XOrigin, YOrigin, YDirection);
		XDirection = 1.f;
	}
	else
	{
		UMathUtilsLibrary::RegressionLine(Y, X, YOrigin, XOrigin, XDirection);
		YDirection = 1.f;
	}
	OutOrigin.X = XOrigin;
	OutOrigin.Y = YOrigin;
	OutDirection.X = XDirection;
	OutDirection.Y = YDirection;
	OutDirection.Normalize();
}

void UMathUtilsLibrary::RegressionLine3D(const TArray<FVector>& P, FVector& OutOrigin, FVector& OutDirection)
{
	// Transform data
	TArray<float> X;
	TArray<float> Y;
	TArray<float> Z;
	for (const FVector& V : P)
	{
		X.Add(V.X);
		Y.Add(V.Y);
		Z.Add(V.Z);
	}

	// Choose how to calculate the line
	float XRange = UMathUtilsLibrary::Range(X);
	float YRange = UMathUtilsLibrary::Range(Y);
	float ZRange = UMathUtilsLibrary::Range(Z);

	float XOrigin;
	float YOrigin;
	float ZOrigin;
	float XDirection;
	float YDirection;
	float ZDirection;
	if (XRange > YRange && XRange > ZRange)
	{
		UMathUtilsLibrary::RegressionLine(X, Y, XOrigin, YOrigin, YDirection);
		UMathUtilsLibrary::RegressionLine(X, Z, XOrigin, ZOrigin, ZDirection);
		XDirection = 1.f;
	}
	else if (YRange > XRange && YRange > ZRange)
	{
		UMathUtilsLibrary::RegressionLine(Y, X, YOrigin, XOrigin, XDirection);
		UMathUtilsLibrary::RegressionLine(Y, Z, YOrigin, ZOrigin, ZDirection);
		YDirection = 1.f;
	}
	else
	{
		UMathUtilsLibrary::RegressionLine(Z, X, ZOrigin, XOrigin, XDirection);
		UMathUtilsLibrary::RegressionLine(Z, Y, ZOrigin, YOrigin, YDirection);
		ZDirection = 1.f;
	}
	OutOrigin.X = XOrigin;
	OutOrigin.Y = YOrigin;
	OutOrigin.Z = ZOrigin;
	OutDirection.X = XDirection;
	OutDirection.Y = YDirection;
	OutDirection.Z = ZDirection;
	OutDirection.Normalize();
}
