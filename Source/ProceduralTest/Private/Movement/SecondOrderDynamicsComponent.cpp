// TODO: Copyright

#include "Movement/SecondOrderDynamicsComponent.h"
#include "MathUtil.h"

void FSecondOrderDynamicInfo::GetKValues(float& K1, float& K2, float& K3)
{
	K1 = Damping / (PI * Frequency);
	K2 = 1.f / FMath::Pow(2.f * PI * Frequency, 2.f);
	K3 = (InitialResponse * Damping) / (2.f * PI * Frequency);
}

// -------------------------------------------------------------------------------------------------------------

USecondOrderDynamicsComponent::USecondOrderDynamicsComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

// /** Returns a vector with the same direction as NewTarget, but being the closest as possible to
// 	LastTarget incrementing or decrementing the NewTarget's norm by multiples of 2*PI. */
// static FVector GetNearestVector(const FVector& LastTarget, const FVector& NewTarget)
// {
// 	if (LastTarget.IsNearlyZero())
// 	{
// 		if (NewTarget.IsNearlyZero())
// 		{
// 			//			UE_LOG(LogTemp, Warning, TEXT("Cero ambos"));
// 			return NewTarget;
// 		}
// 		else
// 		{
// 			FVector UnitNewTarget = NewTarget.GetUnsafeNormal();
// 			float	NewTargetNorm = NewTarget.Length();
// 			float	NewTargetMultiplier = FMath::Floor(NewTargetNorm / (2.f * PI));
// 			float	NewTargetNormMod = NewTargetNorm - NewTargetMultiplier * 2.f * PI;
// 			//			UE_LOG(LogTemp, Warning, TEXT("Cero LastTarget"));
// 			return UnitNewTarget * (NewTargetNormMod < PI ? NewTargetNormMod : NewTargetNormMod - 2.f * PI);
// 		}
// 	}
// 	else
// 	{
// 		if (NewTarget.IsNearlyZero())
// 		{
// 			FVector UnitNewTarget = LastTarget.GetUnsafeNormal();
// 			float	LastTargetNorm = LastTarget.Length();
// 			float	LastTargetMultiplier = FMath::Floor(LastTargetNorm / (2.f * PI));
// 			float	LastTargetNormMod = LastTargetNorm - LastTargetMultiplier * 2.f * PI;
// 			//			UE_LOG(LogTemp, Warning, TEXT("Cero NewTarget"));
// 			return UnitNewTarget * (2.f * PI * (LastTargetMultiplier + (LastTargetNormMod < PI ? 0.f : 1.f)));
// 		}
// 		else
// 		{
// 			// Change direction of new target to be close to last target
// 			FVector FixedNewTarget = (NewTarget | LastTarget) > 0.f ? NewTarget : NewTarget.GetUnsafeNormal() * (NewTarget.Length() - 2.f * PI);

// 			FVector UnitNewTarget = FixedNewTarget.GetUnsafeNormal();
// 			float	NewTargetNorm = FixedNewTarget.Length();
// 			FVector Projection = LastTarget.ProjectOnToNormal(UnitNewTarget);
// 			float	ProjectionNorm = Projection.Length();

// 			float ProjectionMultiplier = FMath::Floor(ProjectionNorm / (2.f * PI));
// 			float ProjectionNormMod = ProjectionNorm - ProjectionMultiplier * 2.f * PI;
// 			float NewTargetMultiplier = FMath::Floor(NewTargetNorm / (2.f * PI));
// 			float NewTargetNormMod1 = NewTargetNorm - NewTargetMultiplier * 2.f * PI;

// 			float	NewTargetNormMod2 = NewTargetNormMod1 + 2.f * PI * (NewTargetNormMod1 < ProjectionNormMod ? 1.f : -1.f);
// 			FVector NewTargetValue1 = UnitNewTarget * (NewTargetNormMod1 + 2.f * PI * ProjectionMultiplier);
// 			FVector NewTargetValue2 = UnitNewTarget * (NewTargetNormMod2 + 2.f * PI * ProjectionMultiplier);
// 			float	Distance1 = FVector::Distance(LastTarget, NewTargetValue1);
// 			float	Distance2 = FVector::Distance(LastTarget, NewTargetValue2);
// 			return Distance1 < Distance2 ? NewTargetValue1 : NewTargetValue2;
// 		}
// 	}
// }

static float GetNearestValue(float LastValue, float NewValue, float Module)
{
	float LastValueMultiplier = FMath::Floor(LastValue / Module);
	float LastValueMod = LastValue - LastValueMultiplier * Module;
	float NewValueMultiplier = FMath::Floor(NewValue / Module);
	float NewValueMod1 = NewValue - NewValueMultiplier * Module;
	float NewValueMod2 = NewValueMod1 < LastValueMod ? NewValueMod1 + Module : NewValueMod1 - Module;
	float NewValue1 = NewValueMod1 + LastValueMultiplier * Module;
	float NewValue2 = NewValueMod2 + LastValueMultiplier * Module;
	float Distance1 = FMath::Abs(NewValue1 - LastValue);
	float Distance2 = FMath::Abs(NewValue2 - LastValue);

	return Distance1 < Distance2 ? NewValue1 : NewValue2;
}

static FVector GetNearestVector(const FVector& LastTarget, const FVector& NewTarget)
{
	float X = GetNearestValue(LastTarget.X, NewTarget.X, 360.f);
	float Y = GetNearestValue(LastTarget.Y, NewTarget.Y, 360.f);
	float Z = GetNearestValue(LastTarget.Z, NewTarget.Z, 360.f);

	return FVector(X, Y, Z);
}

// static void DecrementVectorsIfPossible(const FVector& V1, const FVector& V2, float ToleranceDiff, FVector& O1, FVector& O2)
// {
// 	if (V1.Length() > 2.f * PI && V2.Length() > 2.f * PI)
// 	{
// 		FVector PO1 = V1.GetSafeNormal() * (V1.Length() - 2.f * PI);
// 		FVector PO2 = V2.GetSafeNormal() * (V2.Length() - 2.f * PI);
// 		float	LastDistance = FVector::Distance(V1, V2);
// 		float	NewDistante = FVector::Distance(PO1, PO2);
// 		if (FMath::Abs(LastDistance - NewDistante) < ToleranceDiff)
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("Decrementing"));
// 			O1 = PO1;
// 			O2 = PO2;
// 			return;
// 		}
// 	}
// 	O1 = V1;
// 	O2 = V2;
// }

static void DecrementValuesIfPossible(double V1, double V2, double Module, double& O1, double& O2)
{
	if (V1 > Module && V2 > Module)
	{
		O1 = V1 - Module;
		O2 = V2 - Module;
	}
	else if (V1 <= 0.0 && V2 <= 0.0)
	{
		O1 = V1 + Module;
		O2 = V2 + Module;
	}
	else
	{
		O1 = V1;
		O2 = V2;
	}
}

static void DecrementVectorsIfPossible(const FVector& V1, const FVector& V2, double Module, FVector& O1, FVector& O2)
{
	DecrementValuesIfPossible(V1.X, V2.X, Module, O1.X, O2.X);
	DecrementValuesIfPossible(V1.Y, V2.Y, Module, O1.Y, O2.Y);
	DecrementValuesIfPossible(V1.Z, V2.Z, Module, O1.Z, O2.Z);
}

void USecondOrderDynamicsComponent::ComputeNewTransform(float Delta)
{
	if (USceneComponent* ParentComponent = GetAttachParent())
	{
		if (!bLockLocation)
		{
			float K1, K2, K3;
			LocationParameters.GetKValues(K1, K2, K3);
			FVector ParentLocation = ParentComponent->GetComponentLocation();
			FVector NewLocation = SecondOrderLocation.ComputeNewValue(ParentLocation, Delta, K1, K2, K3);
			SetWorldLocation(NewLocation);
		}
		if (!bLockRotation)
		{
			float K1, K2, K3;
			RotationParameters.GetKValues(K1, K2, K3);
			// FQuat	ParentQuat = ParentComponent->GetComponentQuat();
			// FVector ParentAxis = ParentQuat.GetRotationAxis();
			// float	ParentAngle = ParentQuat.GetAngle();
			// // FVector ParentQuatAxisAngle = ParentAxis * ParentAngle;
			// FVector ParentQuatAxisAngle = GetNearestVector(SecondOrderRotation.LastTargetValue, ParentQuat.ToRotationVector());
			// FVector NewQuatAxisAngle = SecondOrderRotation.ComputeNewValue(ParentQuatAxisAngle, Delta, K1, K2, K3);
			// if (NewQuatAxisAngle.IsNearlyZero())
			// {
			// 	SetWorldRotation(FQuat(FVector(1.f, 0.f, 0.f), 0.f));
			// }
			// else
			// {
			// 	FQuat NewQuat(NewQuatAxisAngle.GetSafeNormal(), NewQuatAxisAngle.Length());
			// 	SetWorldRotation(NewQuat);
			// }

			FVector ParentRotation = ParentComponent->GetComponentRotation().Euler();
			FVector FixedParentRotation = GetNearestVector(SecondOrderRotation.LastTargetValue, ParentRotation);
			UE_LOG(LogTemp, Warning, TEXT("-----"));
			UE_LOG(LogTemp, Warning, TEXT("LastTarget: %s"), *SecondOrderRotation.LastTargetValue.ToString());
			UE_LOG(LogTemp, Warning, TEXT("NewTarget : %s"), *FixedParentRotation.ToString());
			UE_LOG(LogTemp, Warning, TEXT("-----"));
			FVector NewRotation = SecondOrderRotation.ComputeNewValue(FixedParentRotation, Delta, K1, K2, K3);
			SetWorldRotation(FRotator::MakeFromEuler(NewRotation));
			DecrementVectorsIfPossible(SecondOrderRotation.LastTargetValue, SecondOrderRotation.Value, 360.0, SecondOrderRotation.LastTargetValue, SecondOrderRotation.Value);
		}
	}
}

void USecondOrderDynamicsComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	ComputeNewTransform(DeltaTime);
}

void USecondOrderDynamicsComponent::BeginPlay()
{
	Super::BeginPlay();

	// Needed to avoid weird travels and rotations at the start
	SecondOrderLocation.LastTargetValue = GetComponentLocation();
	SecondOrderLocation.Value = GetComponentLocation();
	SecondOrderRotation.LastTargetValue = GetComponentQuat().GetRotationAxis() * GetComponentQuat().GetAngle();
	SecondOrderRotation.Value = GetComponentQuat().GetRotationAxis() * GetComponentQuat().GetAngle();

	SetUsingAbsoluteLocation(!bLockLocation);
	SetUsingAbsoluteRotation(!bLockRotation);
}
