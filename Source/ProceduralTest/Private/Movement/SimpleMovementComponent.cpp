
#include "Movement/SimpleMovementComponent.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"

void USimpleMovementComponent::Move(const FVector2D& Direction)
{
	// 			UE_LOG(LogTemp, Warning, TEXT("Aqui?"));
	if (ACharacter* CharacterOwner = Cast<ACharacter>(GetOwner()))
	{
		if (CharacterOwner->Controller != nullptr)
		{
			// 		// Try to modify velocity
			// 		float VelocityMultiplier = 1.f;
			// 		if (CharacterOwner->Implements<UCarrierInterface>() && ICarrierInterface::Execute_HasItem(CharacterOwner))
			// 		{
			// 			if (AActor* PickedUpItem = ICarrierInterface::Execute_GetItem(CharacterOwner))
			// 			{
			// 				if (PickedUpItem->Implements<UCarriableItemInterface>())
			// 				{
			// 					VelocityMultiplier = (1.f - ICarriableItemInterface::Execute_ItemSlowDown(PickedUpItem));
			// 				}
			// 			}
			// 		}
			// 		UE_LOG(LogTemp, Warning, TEXT("Llega?"));

			// add movement
			float VelocityMultiplier = 1.f;
			FVector2D NewDirection = Direction.GetSafeNormal() * VelocityMultiplier;

			CharacterOwner->AddMovementInput(CharacterOwner->GetActorForwardVector(), NewDirection.Y);
			CharacterOwner->AddMovementInput(CharacterOwner->GetActorRightVector(), NewDirection.X);
		}
	}
}

void USimpleMovementComponent::AddLookRotation(const FVector2D& Direction)
{

	if (ACharacter* CharacterOwner = Cast<ACharacter>(GetOwner()))
	{
		CurrentPitch -= Direction.Y;
		CurrentPitch = FMath::Max(-90.f, FMath::Min(90.f, CurrentPitch));

		// add yaw and pitch input to controller
		CharacterOwner->AddActorWorldRotation(FQuat(CharacterOwner->GetActorUpVector(), FMath::DegreesToRadians(Direction.X)));
		// CharacterOwner->AddControllerYawInput(Direction.X);
		if (UCameraComponent* CameraComponent = CharacterOwner->FindComponentByClass<UCameraComponent>())
		{
			CameraComponent->SetRelativeRotation(FRotator(CurrentPitch, 0.f, 0.f));
		}

		// CharacterOwner->AddControllerPitchInput(Direction.Y);
		if (CharacterOwner->Controller != nullptr)
		{
		}
	}
}

void USimpleMovementComponent::Jump()
{

	if (ACharacter* CharacterOwner = Cast<ACharacter>(GetOwner()))
	{
		CharacterOwner->Jump();
	}
}
