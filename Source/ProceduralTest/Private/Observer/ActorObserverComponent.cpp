// TODO: Copyright

#include "Observer/ActorObserverComponent.h"
#include "Engine/World.h"
#include "Subsystems/WorldSubsystem.h"
#include "Observer/ActorObserverSubsystem.h"

void UActorObserverComponent::BeginPlay()
{
	Super::BeginPlay();
	if (UActorObserverSubsystem* ActorObserverSubsystem = GetWorld()->GetSubsystem<UActorObserverSubsystem>())
	{
		ActorObserverSubsystem->Observe(GetOwner());
	}
}
