// TODO: Copyright

#include "Observer/ActorObserverSubsystem.h"
#include "GameFramework/Actor.h"

void UActorObserverSubsystem::Observe(AActor* Actor)
{
	if (Actor)
	{
		Actor->OnDestroyed.AddDynamic(this, &UActorObserverSubsystem::HandleOnActorDestroyed);
		ObservedActors.AddUnique(Actor);
	}
}

void UActorObserverSubsystem::HandleOnActorDestroyed(AActor* DestroyedActor)
{
	if (DestroyedActor)
	{
		ObservedActors.Remove(DestroyedActor);
		if (ObservedActors.IsEmpty())
		{
			OnAllActorsDestroyed.Broadcast();
		}
	}
}
