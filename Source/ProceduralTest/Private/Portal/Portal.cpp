// Fill out your copyright notice in the Description page of Project Settings.


#include "Portal/Portal.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Camera/CameraComponent.h"

// Sets default values
APortal::APortal() : bEnabled(true) {

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	Plane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plane"));
	Plane->SetupAttachment(RootComponent);
	//Plane->SetRelativeRotation(FQuat(FVector(0.f,1.f,0.f),-PI/2.0));

	DefaultPlaneMaterial = Plane->GetMaterial(0);

	// This will be attached to the other portal
	OtherPortalCamera = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("Other Portal Camera"));
	OtherPortalCamera->SetupAttachment(RootComponent);
	OtherPortalCamera->bEnableClipPlane = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();

	checkf(TextureRenderTarget, TEXT("APortal: TextureRenderTarget is nullptr.")); // Seria mejor crear esto en el constructor
	checkf(PlaneMaterial, TEXT("APortal: PlaneMaterial is nullptr."));

	OtherPortalCamera->TextureTarget = TextureRenderTarget;
	Plane->SetMaterial(0,PlaneMaterial);
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bEnabled && LinkedPortal) {

		UCameraComponent* ViewTargetCamera = GetWorld()->GetFirstPlayerController()->GetViewTarget()->FindComponentByClass<UCameraComponent>();

		// Movemos la camara
		FVector PortalToViewTarget = ViewTargetCamera->GetComponentLocation() - GetActorLocation();
		FVector PortalToViewTargetRel = GetActorQuat().Inverse() * PortalToViewTarget;
		FVector OtherPortalToOtherCameraRel = FQuat(LinkedPortal->GetActorUpVector(), PI) * LinkedPortal->GetActorQuat() * PortalToViewTargetRel;
		FVector OtherCameraLocation = LinkedPortal->GetActorLocation() + OtherPortalToOtherCameraRel;
		OtherPortalCamera->SetWorldLocation(OtherCameraLocation);

		// Giramos la camara
		FQuat PortalToViewTargetOri = GetActorQuat().Inverse() * ViewTargetCamera->GetComponentQuat();
		FQuat OtherPortalToOtherCameraOri = FQuat(LinkedPortal->GetActorUpVector(), PI) * LinkedPortal->GetActorQuat() * PortalToViewTargetOri;
		OtherPortalCamera->SetWorldRotation(OtherPortalToOtherCameraOri);

		// Asignamos el clip panel (Solo hace falta una vez si el portal esta quieto)
		OtherPortalCamera->ClipPlaneBase = LinkedPortal->GetActorLocation();
		OtherPortalCamera->ClipPlaneNormal = LinkedPortal->GetActorForwardVector();

	}

}


void APortal::SetPortalEnabled(bool bNewEnabled) {

	if (!bEnabled && bNewEnabled) {
		Plane->SetMaterial(0,PlaneMaterial);
		OtherPortalCamera->Activate();
		SetActorTickEnabled(true);
	} else if (bEnabled && !bNewEnabled) {
		Plane->SetMaterial(0,DefaultPlaneMaterial);
		OtherPortalCamera->Deactivate();
		SetActorTickEnabled(false);
	}

	bEnabled = bNewEnabled;

}
