// TODO: Copyright

#include "RayCast/SingleRayCastPattern.h"
#include "Components/SceneComponent.h"
#include "MathUtil.h"

TArray<FRayCastInfo> USingleRayCastPattern::CreateRayCastInfo()
{
	TArray<FRayCastInfo> RayCastInfos;
	if (USceneComponent* RayCastSource = GetRayCastSource())
	{
		FVector Start = RayCastSource->GetComponentLocation();
		FVector Direction = RayCastSource->GetForwardVector();
		FVector End = Start + FMath::VRandCone(Direction, (RayAngleDispersion / 360.f) * 2.f * PI) * RayLength;
		RayCastInfos.Emplace(Start, End);
	}
	return RayCastInfos;
}
