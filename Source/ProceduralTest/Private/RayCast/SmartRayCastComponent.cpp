// TODO: Copyright

#include "RayCast/SmartRayCastComponent.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"

USceneComponent* URayCastPattern::GetRayCastSource()
{
	if (RayCastComponentOwner)
	{
		if (IsValid(RayCastComponentOwner->RayCastSource))
		{
			return RayCastComponentOwner->RayCastSource;
		}
		return RayCastComponentOwner->GetOwner()->GetRootComponent();
	}
	return nullptr;
}

void USmartRayCastComponent::Trace()
{
	if (RayCastPattern)
	{
		TArray<FRayCastInfo>  RayCastInfos = RayCastPattern->CreateRayCastInfo();
		FCollisionQueryParams QueryParams;
		QueryParams.bIgnoreTouches = true;
		QueryParams.bFindInitialOverlaps = false;
		for (AActor* IgnoredActor : IgnoredActors)
		{
			QueryParams.AddIgnoredActor(IgnoredActor);
		}
		for (const FRayCastInfo& RayCastInfo : RayCastInfos)
		{
			FHitResult HitResult;
			FVector	   Start = RayCastInfo.Start;
			FVector	   End = RayCastInfo.End;
			bool	   HitSomething = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, CollisionChannel, QueryParams);
			OnRayCast.Broadcast(Start, End, HitSomething, HitResult);
		}
	}
}

void USmartRayCastComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!IsValid(RayCastSource))
	{
		RayCastSource = GetOwner()->GetRootComponent();
	}
}

void USmartRayCastComponent::OnRegister()
{
	Super::OnRegister();
	if (IsValid(RayCastPattern))
	{
		RayCastPattern->RayCastComponentOwner = this;
	}
}
