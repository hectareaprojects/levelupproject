// TODO: Copyright

#include "Shake/ComponentReboundShakePattern.h"

UComponentReboundShakePattern::UComponentReboundShakePattern()
	: TurningPoint(0.5f)
{
}

void UComponentReboundShakePattern::Initialize(float InTotalTime)
{
	TotalTime = InTotalTime <= 0.f ? 1.f : InTotalTime;

	FVector GoalLocation;
	GoalLocation.X = Location.X.Value + FMath::FRandRange(-Location.X.Deviation, Location.X.Deviation);
	GoalLocation.Y = Location.Y.Value + FMath::FRandRange(-Location.Y.Deviation, Location.Y.Deviation);
	GoalLocation.Z = Location.Z.Value + FMath::FRandRange(-Location.Z.Deviation, Location.Z.Deviation);

	FRotator GoalRotator;
	GoalRotator.Pitch = Rotation.Pitch.Value + FMath::FRandRange(-Rotation.Pitch.Deviation, Rotation.Pitch.Deviation);
	GoalRotator.Yaw = Rotation.Yaw.Value + FMath::FRandRange(-Rotation.Yaw.Deviation, Rotation.Yaw.Deviation);
	GoalRotator.Roll = Rotation.Roll.Value + FMath::FRandRange(-Rotation.Roll.Deviation, Rotation.Roll.Deviation);
	UE_LOG(LogTemp, Warning, TEXT("X= %g"),GoalLocation.X);

	GoalTransform.SetLocation(GoalLocation);
	GoalTransform.SetRotation(GoalRotator.Quaternion());
}

FTransform UComponentReboundShakePattern::Update(float DeltaTime, float ElapsedTime)
{
	UE_LOG(LogTemp, Warning, TEXT("Adios: %g"), ElapsedTime);	
	float CurrentTime = ElapsedTime / TotalTime;
	float InterpValue = (CurrentTime < TurningPoint) ? (CurrentTime / TurningPoint) : (1.f - CurrentTime) / (1.f - TurningPoint);
	return FTransform(GoalTransform.GetRotation() * InterpValue, GoalTransform.GetLocation() * InterpValue);
}
