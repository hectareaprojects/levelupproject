// TODO: Copyright

#include "Shake/ComponentShakeSubsystem.h"
#include "Components/SceneComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"

FComponentShakeInfo::FComponentShakeInfo(float InTotalTime, USceneComponent* InSceneComponent, UComponentShakePattern* InShakePattern)
{
	TotalTime = InTotalTime;
	SceneComponent = InSceneComponent;
	ShakePattern = InShakePattern;
}

FComponentShakeInfo::FComponentShakeInfo(float InTotalTime, USceneComponent* InSceneComponent, UComponentShakePattern* InShakePattern,
	const FOnShakeFinished& OnShakeCompletedFunc, const FOnShakeFinished& OnShakeRemovedFunc)
	: FComponentShakeInfo(InTotalTime, InSceneComponent, InShakePattern)
{
	OnShakeCompleted = OnShakeCompletedFunc;
	OnShakeRemoved = OnShakeRemovedFunc;
}

void FComponentShakeInfo::Update(float DeltaTime)
{
	if (SceneComponent && ShakePattern)
	{
		UWorld* World = SceneComponent->GetWorld();
		if (World)
		{
			ElapsedTime += DeltaTime;
			FTransform NewTransform = ShakePattern->Update(DeltaTime, ElapsedTime);
			UE_LOG(LogTemp, Warning, TEXT("(%g,%g,%g)"), NewTransform.GetLocation().X, NewTransform.GetLocation().Y, NewTransform.GetLocation().Z);
			SceneComponent->SetRelativeLocation(NewTransform.GetLocation());
			SceneComponent->SetRelativeRotation(NewTransform.GetRotation().Rotator());
		}
	}
}

void FComponentShakeInfo::Restart()
{
	ElapsedTime = 0.f;
	if (ShakePattern)
	{
		ShakePattern->Restart();
	}
}

bool FComponentShakeInfo::IsCompleted()
{
	return TotalTime > 0.f && (ElapsedTime >= TotalTime);
}

void FComponentShakeInfo::NotifyCompletion()
{
	OnShakeCompleted.ExecuteIfBound();
}

void FComponentShakeInfo::NotifyRemoval()
{
	OnShakeRemoved.ExecuteIfBound();
}

void UComponentShakeSubsystem::Tick(float DeltaTime)
{
	for (TTuple<USceneComponent*, FComponentShakeInfos>& ArrayShakeInfos : ShakeInfos)
	{
		for (FComponentShakeInfo& ShakeInfo : ArrayShakeInfos.Value.ComponentShakeInfos)
		{
			ShakeInfo.Update(DeltaTime);
		}
	}
	for (TTuple<USceneComponent*, FComponentShakeInfos>& ArrayShakeInfos : ShakeInfos)
	{
		ArrayShakeInfos.Value.ComponentShakeInfos.RemoveAll([](FComponentShakeInfo& ShakeInfo) {
			bool bIsCompleted = ShakeInfo.IsCompleted();
			if (bIsCompleted)
			{
				ShakeInfo.NotifyCompletion();
			}
			return bIsCompleted;
		});
	}
}

void UComponentShakeSubsystem::AddShake(float TotalTime, USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass,
	const FOnShakeFinished& OnShakeCompletedFunc, const FOnShakeFinished& OnShakeRemovedFunc)
{
	if (ShakePatternClass && SceneComponent)
	{
		if (FComponentShakeInfo* ShakeInfo = FindShakeInfo(SceneComponent, ShakePatternClass))
		{
			ShakeInfo->Restart();
		}
		else if (UComponentShakePattern* ShakePattern = NewObject<UComponentShakePattern>(this, ShakePatternClass))
		{
			ShakePattern->Initialize(TotalTime);
			FComponentShakeInfos& ComponentShakeInfos = ShakeInfos.FindOrAdd(SceneComponent);
			ComponentShakeInfos.ComponentShakeInfos.Emplace(TotalTime, SceneComponent, ShakePattern);
		}
	}
}

FComponentShakeInfo* UComponentShakeSubsystem::FindShakeInfo(USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass)
{
	if (FComponentShakeInfos* ComponentShakeInfos = ShakeInfos.Find(SceneComponent))
	{
		return ComponentShakeInfos->ComponentShakeInfos.FindByPredicate([ShakePatternClass](FComponentShakeInfo& ShakeInfo) {
			return ShakeInfo.ShakePattern->StaticClass() == ShakePatternClass;
		});
	}
	return nullptr;
}

bool UComponentShakeSubsystem::HasShake(USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass)
{
	return FindShakeInfo(SceneComponent, ShakePatternClass) != nullptr;
}

void UComponentShakeSubsystem::RemoveShake(USceneComponent* const& SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass, bool& bSuccess)
{
	if (SceneComponent && ShakePatternClass)
	{
		if (FComponentShakeInfos* ComponentShakeInfos = ShakeInfos.Find(SceneComponent))
		{
			int ElementsRemoved = ComponentShakeInfos->ComponentShakeInfos.RemoveAll([SceneComponent, ShakePatternClass](FComponentShakeInfo& ShakeInfo) {
				return ShakeInfo.ShakePattern->StaticClass() == ShakePatternClass;
			});
			bSuccess = ElementsRemoved > 0;
			return;
		}
	}
	bSuccess = false;
}

void UAddAsyncShakeNode::OnCompletion()
{
	Completed.Broadcast();
}

void UAddAsyncShakeNode::OnRemoval()
{
	Removed.Broadcast();
}

UAddAsyncShakeNode* UAddAsyncShakeNode::AddShake(UComponentShakeSubsystem* const& InShakeSubsystem, float InTotalTime, USceneComponent* const& InSceneComponent, TSubclassOf<UComponentShakePattern> InShakePatternClass)
{
	UAddAsyncShakeNode* NewNode = NewObject<UAddAsyncShakeNode>();
	NewNode->ShakeSubsystem = InShakeSubsystem;
	NewNode->TotalTime = InTotalTime;
	NewNode->ShakePatternClass = InShakePatternClass;
	NewNode->SceneComponent = InSceneComponent;

	return NewNode;
}

void UAddAsyncShakeNode::Activate()
{
	if (ShakeSubsystem)
	{
		ShakeSubsystem->AddShake(TotalTime, SceneComponent, ShakePatternClass,
			FOnShakeFinished::CreateUObject(this, &UAddAsyncShakeNode::OnCompletion), FOnShakeFinished::CreateUObject(this, &UAddAsyncShakeNode::OnRemoval));
	}
}
