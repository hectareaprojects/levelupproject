// TODO: Copyright

#include "Shake/SmartBoomerangShakePattern.h"

USmartBoomerangShakePattern::USmartBoomerangShakePattern()
	: TurningPoint(0.5f)
{
}

void USmartBoomerangShakePattern::Start()
{
	FVector GoalLocation;
	GoalLocation.X = Location.X.Value + FMath::FRandRange(-Location.X.Deviation, Location.X.Deviation);
	GoalLocation.Y = Location.Y.Value + FMath::FRandRange(-Location.Y.Deviation, Location.Y.Deviation);
	GoalLocation.Z = Location.Z.Value + FMath::FRandRange(-Location.Z.Deviation, Location.Z.Deviation);

	FRotator GoalRotator;
	GoalRotator.Pitch = Rotation.Pitch.Value + FMath::FRandRange(-Rotation.Pitch.Deviation, Rotation.Pitch.Deviation);
	GoalRotator.Yaw = Rotation.Yaw.Value + FMath::FRandRange(-Rotation.Yaw.Deviation, Rotation.Yaw.Deviation);
	GoalRotator.Roll = Rotation.Roll.Value + FMath::FRandRange(-Rotation.Roll.Deviation, Rotation.Roll.Deviation);

	GoalTransform.SetLocation(GoalLocation);
	GoalTransform.SetRotation(GoalRotator.Quaternion());
}

bool USmartBoomerangShakePattern::Update(const FShakeInfo& ShakeInfo, FShakeResult& ShakeResult)
{
	float CurrentTime = ShakeInfo.ElapsedTime / TotalTime;
	if (CurrentTime < 1.f)
	{
		float InterpValue = (CurrentTime < TurningPoint) ? (CurrentTime / TurningPoint) : (1.f - CurrentTime) / (1.f - TurningPoint);
		ShakeResult.Transform.SetLocation(GoalTransform.GetLocation() * InterpValue);
		ShakeResult.Transform.SetRotation(GoalTransform.GetRotation() * InterpValue);
		return true;
	}
	return false;
}
