// TODO: Copyright

#include "Shake/SmartShakeComponent.h"
#include "Components/SceneComponent.h"

USmartShakeComponent::USmartShakeComponent()
	: bActive(false), ElapsedTime(0.f), SceneComponent(nullptr), SmartShakePattern(nullptr)
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void USmartShakeComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (SmartShakePattern)
	{
		ElapsedTime += DeltaTime;
		FShakeInfo ShakeInfo;
		ShakeInfo.DeltaTime = DeltaTime;
		ShakeInfo.ElapsedTime = ElapsedTime;
		FShakeResult ShakeResult;
		bool		 bCompleted = SmartShakePattern->Update(ShakeInfo, ShakeResult);
		UpdateSceneComponent(ShakeResult);
		if (bCompleted)
		{
			SetComponentTickEnabled(false);
			UpdateSceneComponent(FShakeResult());
			OnCompleted.ExecuteIfBound();
			bActive = false;
		}
	}
}

void USmartShakeComponent::OnRegister()
{
	Super::OnRegister();
	if (SmartShakePattern)
	{
		SmartShakePattern->ShakeComponentOwner = this;
	}
}

void USmartShakeComponent::UpdateSceneComponent(const FShakeResult& ShakeResult)
{
	if (SceneComponent)
	{
		SceneComponent->SetRelativeTransform(ShakeResult.Transform);
	}
}

void USmartShakeComponent::StartShake()
{
	if (SmartShakePattern)
	{
		if (bActive)
		{
			SmartShakePattern->Restart();
			OnRestarted.ExecuteIfBound();
		}
		else
		{
			SmartShakePattern->Start();
			SetComponentTickEnabled(true);
			bActive = true;
		}
		ElapsedTime = 0.f;
	}
}

void USmartShakeComponent::StartShake(const FShakeDelegate& InOnRestarted, const FShakeDelegate& InOnCompleted,
	const FShakeDelegate& InOnStopped)
{
	StartShake();
	OnRestarted = InOnRestarted;
	OnCompleted = InOnCompleted;
	OnStopped = InOnStopped;
}

void USmartShakeComponent::StopShake()
{
	if (SmartShakePattern)
	{
		if (bActive)
		{
			SmartShakePattern->Stop();
			SetComponentTickEnabled(false);
			UpdateSceneComponent(FShakeResult());
			OnStopped.ExecuteIfBound();
			bActive = false;
		}
	}
}

bool USmartShakeComponent::IsActivated()
{
	return bActive;
}

void USmartShakeComponent::SetComponentToShake(USceneComponent* InSceneComponent)
{
	StopShake();
	SceneComponent = InSceneComponent;
	StartShake();
}

// -------------------------------------------------------------------------------------------------------------

void UAsyncStartShakeNode::OnCompleted()
{
	Completed.Broadcast();
}

void UAsyncStartShakeNode::OnStopped()
{
	Stopped.Broadcast();
}

void UAsyncStartShakeNode::OnRestarted()
{
	Restarted.Broadcast();
}

UAsyncStartShakeNode* UAsyncStartShakeNode::StartShake(USmartShakeComponent* const& InShakeComponent)
{
	UAsyncStartShakeNode* StartShakeNode = NewObject<UAsyncStartShakeNode>();
	StartShakeNode->ShakeComponent = InShakeComponent;

	return StartShakeNode;
}

void UAsyncStartShakeNode::Activate()
{
	if (IsValid(ShakeComponent))
	{
		ShakeComponent->StartShake(
			FShakeDelegate::CreateUObject(this, &UAsyncStartShakeNode::OnRestarted),
			FShakeDelegate::CreateUObject(this, &UAsyncStartShakeNode::OnCompleted),
			FShakeDelegate::CreateUObject(this, &UAsyncStartShakeNode::OnStopped));
	}
}
