// TODO: Copyright

#include "SmartSpawner/SimpleSpawnInfoPattern.h"
#include "Engine/Public/StaticMeshSceneProxy.h"

TArray<FSpawnInfo> USimpleSpawnInfoPattern::GetSpawnInfo_Implementation()
{
	TArray<FSpawnInfo> SpawnInfoData;
	if (ActorClass)
	{
		SpawnInfoData.Emplace(ActorClass);
	}
	return SpawnInfoData;
}
