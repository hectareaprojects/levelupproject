// TODO: Copyright

#include "SmartSpawner/SmartSpawnerComponent.h"
#include "Engine/World.h"
#include "SceneManagement.h"
#include "SmartSpawner/SimpleSpawnInfoPattern.h"

FSpawnInfo::FSpawnInfo(TSubclassOf<AActor> InActorClass)
	: ActorClass(InActorClass)
{
}

FSpawnInfo::FSpawnInfo(TSubclassOf<AActor> InActorClass, const FTransform& InActorTransform)
	: ActorClass(InActorClass), ActorTransform(InActorTransform)
{
}

// -------------------------------------------------------------------------------------------------------------

TArray<FSpawnInfo> USpawnInfoPattern::GetSpawnInfo_Implementation()
{
	return TArray<FSpawnInfo>();
}

// -------------------------------------------------------------------------------------------------------------

USmartSpawnerComponent::USmartSpawnerComponent()
{
	SpawnInfo = NewObject<USimpleSpawnInfoPattern>();
}

TArray<AActor*> USmartSpawnerComponent::SpawnActors()
{
	TArray<AActor*> SpawnedActors;	
	if (SpawnInfo)
	{
		for (const FSpawnInfo& SpawnInfoData : SpawnInfo->GetSpawnInfo())
		{
			if (SpawnInfoData.ActorClass)
			{
				AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(SpawnInfoData.ActorClass, GetComponentTransform() * SpawnInfoData.ActorTransform);
				SpawnedActors.Add(SpawnedActor);
			}
		}
	}
	return SpawnedActors;
}

FPrimitiveSceneProxy* USmartSpawnerComponent::CreateSceneProxy()
{
	class FSmartSpawnerSceneProxy : public FPrimitiveSceneProxy
	{
	private:
		TArray<FTransform> SpawnTransforms;

	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		FSmartSpawnerSceneProxy(const USmartSpawnerComponent* InComponent, const TArray<FTransform>& InSpawnTransforms)
			: FPrimitiveSceneProxy(InComponent)
			, SpawnTransforms(InSpawnTransforms)
		{
			bWillEverBeLit = false;
		}

		virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
		{
			QUICK_SCOPE_CYCLE_COUNTER(STAT_BoxSceneProxy_GetDynamicMeshElements);

			for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
			{
				if (VisibilityMap & (1 << ViewIndex))
				{
					for (const FTransform& SpawnTransform : SpawnTransforms)
					{
						FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);
						FVector					 SpawnLocation = SpawnTransform.GetLocation();
						FVector					 XDir = SpawnTransform.GetRotation().GetForwardVector();
						FVector					 YDir;
						FVector					 ZDir;
						XDir.FindBestAxisVectors(YDir, ZDir);
						FMatrix ArrowMatrix(XDir, YDir, ZDir, SpawnLocation);
						DrawDirectionalArrow(PDI, ArrowMatrix, FLinearColor::Yellow, 20.f, 5.f, SDPG_World, 2.f);
						DrawWireSphere(PDI, SpawnLocation, FLinearColor::Yellow, 5.0, 8, SDPG_World, 2.f);
					}
				}
			}
		}

		virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
		{
			FPrimitiveViewRelevance Result;
			Result.bDrawRelevance = IsShown(View) && View->Family->EngineShowFlags.Editor; // Tener cuidado con View->Family->EngineShowFlags.Editor!!!!!
			Result.bDynamicRelevance = true;
			Result.bShadowRelevance = IsShadowCast(View);
			Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
			return Result;
		}

		virtual uint32 GetMemoryFootprint(void) const override { return (sizeof(*this) + GetAllocatedSize()); }
		uint32		   GetAllocatedSize(void) const { return (FPrimitiveSceneProxy::GetAllocatedSize()); }
	};

	if (SpawnInfo)
	{
		TArray<FSpawnInfo> SpawnInfoData = SpawnInfo->GetSpawnInfo();
		TArray<FTransform> SpawnTransforms;
		for (FSpawnInfo& SpawnInfoElem : SpawnInfoData)
		{
			SpawnTransforms.Add(GetComponentTransform() * SpawnInfoElem.ActorTransform);
		}
		return new FSmartSpawnerSceneProxy(this, SpawnTransforms);
	}
	return nullptr;
}

FBoxSphereBounds USmartSpawnerComponent::CalcBounds(const FTransform& LocalToWorld) const
{
	if (SpawnInfo)
	{
		TArray<FSpawnInfo> SpawnInfoData = SpawnInfo->GetSpawnInfo();
		if (!SpawnInfoData.IsEmpty())
		{
			TArray<FVector> SpawnLocations;
			for (FSpawnInfo& SpawnInfoElem : SpawnInfoData)
			{
				SpawnLocations.Add((GetComponentTransform() * SpawnInfoElem.ActorTransform).GetLocation());
			}
			return FBoxSphereBounds(SpawnLocations.GetData(), SpawnInfoData.Num());
		}
	}
	return FBoxSphereBounds();
}
