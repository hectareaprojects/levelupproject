// TODO: Copyright

#include "Timer/SimpleTimerPattern.h"
#include "Engine/World.h"
#include "TimerManager.h"

void USimpleTimerPattern::InitTimer()
{
	if (Time == 0.f && bLoop)
	{
		ActivateTicker();
	}
	else
	{
		ActivateTimer();
	}
}

void USimpleTimerPattern::ClearTimer()
{
	DeactivateTicker();
	DeactivateTimer();
}

void USimpleTimerPattern::SetTime(float InTime)
{
	float ClampedInTime = FMath::Max(0.f, InTime);
	if (ClampedInTime != Time)
	{
		ClearTimer();
		Time = InTime;
		InitTimer();
	}
}

void USimpleTimerPattern::SetLoop(bool bInLoop)
{
	if (bInLoop != bLoop)
	{
		ClearTimer();
		bLoop = bInLoop;
		InitTimer();
	}
}

bool USimpleTimerPattern::TickerNotify(float Delta)
{
	Notify();
	return true;
}

void USimpleTimerPattern::ActivateTicker()
{
	if (!bUsingTicker)
	{
		TickerHandle = FTSTicker::GetCoreTicker().AddTicker(FTickerDelegate::CreateUObject(this, &USimpleTimerPattern::TickerNotify));
		bUsingTicker = true;
	}
}

void USimpleTimerPattern::DeactivateTicker()
{
	if (bUsingTicker)
	{
		FTSTicker::GetCoreTicker().RemoveTicker(TickerHandle);
		bUsingTicker = false;
	}
}

void USimpleTimerPattern::ActivateTimer()
{
	if (!bUsingTimer)
	{
		if (Time == 0.f)
		{
			GetWorld()->GetTimerManager().SetTimerForNextTick(FTimerDelegate::CreateUObject(this, &USimpleTimerPattern::Notify));
		}
		else
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &USimpleTimerPattern::Notify), Time, bLoop);
			bUsingTimer = true;
		}
		bUsingTimer = true;
	}
}

void USimpleTimerPattern::DeactivateTimer()
{
	if (bUsingTimer)
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		bUsingTimer = false;
	}
}
