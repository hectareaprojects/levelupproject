// TODO: Copyright

#include "Timer/SmartTimerComponent.h"

void UTimerPattern::Notify()
{
	if (SmartTimerComponent)
	{
		SmartTimerComponent->OnNotify.Broadcast();
	}
}

void USmartTimerComponent::OnRegister()
{
	Super::OnRegister();	
	if (IsValid(TimerPattern))
	{
		TimerPattern->SmartTimerComponent = this;
	}
}

void USmartTimerComponent::InitializeTimer()
{
	if (!bTimerInitialized && TimerPattern)
	{
		bTimerInitialized = true;
		TimerPattern->InitTimer();
	}
}

void USmartTimerComponent::ClearTimer()
{
	if (bTimerInitialized && TimerPattern)
	{
		bTimerInitialized = false;
		TimerPattern->ClearTimer();
	}
}

bool USmartTimerComponent::IsTimerInitialized() const
{
	return bTimerInitialized;
}
