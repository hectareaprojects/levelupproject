// TODO: Copyright

#include "Trail/SingleTrailPattern.h"
#include "MathUtil.h"

FBatchLineInfo USingleTrailPattern::CreateBatchLines()
{
	FBatchLineInfo BatchLineInfo;
	FVector		   Location = GetTrailComponentOwner()->GetComponentLocation();
	float		   Offset = FMath::FRandRange(0.f, Deviation);
	FVector		   NewLocation = Location + FMath::VRand() * Offset;
	if (!bFirstLine)
	{
		BatchLineInfo.Lines.Emplace(LastLocation, NewLocation, TrailColor, TrailThickness);
	}
	else
	{
		bFirstLine = false;
	}
	LastLocation = NewLocation;
	return BatchLineInfo;
}
