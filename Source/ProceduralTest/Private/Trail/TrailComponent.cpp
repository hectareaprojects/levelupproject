// TODO: Copyright

#include "Trail/TrailComponent.h"
#include "SceneManagement.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "MathUtil.h"

class FTrailSceneProxy : public FPrimitiveSceneProxy
{
private:
	TArray<FLineInfo> Lines;

public:
	SIZE_T
	GetTypeHash() const override
	{
		static size_t UniquePointer;
		return reinterpret_cast<size_t>(&UniquePointer);
	}

	FTrailSceneProxy(const UTrailComponent* InComponent, const TArray<FLineInfo>& InLines)
		: FPrimitiveSceneProxy(InComponent)
		, Lines(InLines)
	{
		bWillEverBeLit = false;
	}

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
	{
		const FMatrix& LocalToWorldM = GetLocalToWorld();
		FVector		   RightVector = LocalToWorldM.GetScaledAxis(EAxis::Y);
		FVector		   UpVector = LocalToWorldM.GetScaledAxis(EAxis::Z);
		for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
		{
			if (VisibilityMap & (1 << ViewIndex))
			{
				FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);

				for (const FLineInfo& Line : Lines)
				{
					FVector		 Start = Line.Start;
					FVector		 End = Line.End;
					FLinearColor Color = Line.Color;
					float		 Thickness = Line.Thickness;
					PDI->DrawLine(Start, End, Color, SDPG_World, Thickness);
				}
			}
		}
	}

	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
	{
		FPrimitiveViewRelevance Result;
		Result.bDrawRelevance = IsShown(View);
		Result.bDynamicRelevance = true;
		// Result.bShadowRelevance = IsShadowCast(View);
		// Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
		// Result.bVelocityRelevance = DrawsVelocity() && Result.bOpaque && Result.bRenderInMainPass;
		return Result;
	}

	virtual uint32 GetMemoryFootprint(void) const override
	{
		return (sizeof(*this) + GetAllocatedSize());
	}
	uint32 GetAllocatedSize(void) const
	{
		return (FPrimitiveSceneProxy::GetAllocatedSize());
	}
};

void UTrailComponent::ActivateTrail()
{
	bIsTrailActivated = true;
	if (!GetWorld()->GetTimerManager().IsTimerActive(TimerHandle))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &UTrailComponent::UpdateLines), GenerationTime, true);
	}
}

void UTrailComponent::DeactivateTrail()
{
	bIsTrailActivated = false;
	// The timer will be removed by UpdateLines
}

bool UTrailComponent::IsTrailActive()
{
	return bIsTrailActivated;
}

void UTrailComponent::OnRegister()
{
	Super::OnRegister();
	if (IsValid(TrailPattern))
	{
		TrailPattern->TrailComponentOwner = this;
	}
}

void UTrailComponent::BeginPlay()
{
	Super::BeginPlay();
	if (bStartActivated)
	{
		ActivateTrail();
	}
}

FPrimitiveSceneProxy* UTrailComponent::CreateSceneProxy()
{
	TArray<FLineInfo> LinesToDraw;
	for (int i = 0; i < CurrentLength; i++)
	{
		int Index = (NextIndex + TrailLength - i - 1) % TrailLength;
		LinesToDraw.Append(Lines[Index].Lines);
	}
	return new FTrailSceneProxy(this, LinesToDraw);
}

FBoxSphereBounds UTrailComponent::CalcBounds(const FTransform& LocalToWorld) const
{
	TArray<FVector> Points;
	for (const FBatchLineInfo& BatchLineInfo : Lines)
	{
		for (const FLineInfo& LineInfo : BatchLineInfo.Lines)
		{
			Points.Add(LineInfo.Start);
			Points.Add(LineInfo.End);
		}
	}
	return FBoxSphereBounds(Points.GetData(), Points.Num());
}

void UTrailComponent::UpdateLines()
{
	if (IsValid(TrailPattern))
	{
		for (int i = 0; i < FMath::Min(CurrentLength, TrailLength - 1); i++)
		{
			int Index = (NextIndex + TrailLength - i - 1) % TrailLength;
			TrailPattern->UpdateBatchLines(Lines[Index]);
		}
		if (bIsTrailActivated)
		{
			if (Lines.Num() < TrailLength)
			{
				Lines.Emplace(TrailPattern->CreateBatchLines());
			}
			else
			{
				TrailPattern->DestroyBatchLines(Lines[NextIndex]);
				Lines[NextIndex] = TrailPattern->CreateBatchLines();
			}
			CurrentLength = FMath::Min(CurrentLength + 1, TrailLength);
			NextIndex = (NextIndex + 1) % TrailLength;
		}
		else if (CurrentLength > 0)
		{
			TrailPattern->DestroyBatchLines(Lines[(NextIndex + TrailLength - CurrentLength) % TrailLength]);
			CurrentLength--;
			if (CurrentLength == 0)
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
				OnTrailEnded.Broadcast();
				return;
			}
		}
		MarkRenderStateDirty();
	}
}
