// TODO: Copyright

#include "Weapons/AutoGunStyle.h"
#include "Weapons/GunComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"

void UAutoGunStyle::PressTriggerImpl()
{
	GunComponentOwner->GetWorld()->GetTimerManager().SetTimerForNextTick(FTimerDelegate::CreateUObject(this, &UAutoGunStyle::NotifyShoot));
	GunComponentOwner->GetWorld()->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &UAutoGunStyle::NotifyShoot), 1.f / FireRate, true);
}

void UAutoGunStyle::ReleaseTriggerImpl()
{
	UE_LOG(LogTemp, Warning, TEXT("Release"));	
	GunComponentOwner->GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	TimerHandle.Invalidate();
}
