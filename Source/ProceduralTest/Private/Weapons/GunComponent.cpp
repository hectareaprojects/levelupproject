// TODO: Copyright

#include "Weapons/GunComponent.h"
#include "Weapons/ShootStyle.h"
#include "Weapons/GunStyle.h"

void UGunComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get the source
	ShootSource = GetOwner()->GetRootComponent();

	// Create the shoot style
	if (ShootStyleClass)
	{
		ShootStyle = NewObject<UShootStyle>(this, ShootStyleClass);
		if (ShootStyle)
		{
			ShootStyle->GunComponentOwner = this;
			ShootStyle->StartShootStyle();
		}
	}

	// Create the gun style
	if (GunStyleClass)
	{
		GunStyle = NewObject<UGunStyle>(this, GunStyleClass);
		if (GunStyle)
		{
			GunStyle->GunComponentOwner = this;
			GunStyle->StartGunStyle();
		}
	}
}

void UGunComponent::Shoot()
{
	if (ShootStyle && ShootSource)
	{
		FVector SourceLocation = ShootSource->GetComponentLocation();
		FVector SourceDirection = ShootSource->GetForwardVector();
		ShootStyle->ShootImpl(SourceLocation, SourceDirection);
		OnShoot.Broadcast();
	}
}

void UGunComponent::PressTrigger()
{
	if (GunStyle)
	{
		bTriggerPressed = true;
		GunStyle->PressTriggerImpl();
	}
}

void UGunComponent::ReleaseTrigger()
{
	if (GunStyle)
	{
		GunStyle->ReleaseTriggerImpl();
		bTriggerPressed = false;
	}
}

bool UGunComponent::IsTriggerPressed() const
{
	return bTriggerPressed;
}
