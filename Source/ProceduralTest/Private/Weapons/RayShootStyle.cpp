// TODO: Copyright

#include "Weapons/RayShootStyle.h"
#include "Weapons/GunComponent.h"
#include "CollisionQueryParams.h"
#include "Engine/World.h"

void URayShootStyle::ShootImpl(const FVector& Source, const FVector& Direction)
{
	FHitResult HitResult;

	// Get ray info
	FVector Start = Source;
	FVector End = Source + FMath::VRandCone(Direction, RayAngleDispersion) * RayLength;

	// Get ignored actors
	AActor* Owner = nullptr;
	if (GunComponentOwner)
	{
		Owner = GunComponentOwner->GetOwner();
	}
	AActor* ParentOwner = nullptr;
	if (Owner)
	{
		ParentOwner = Owner->GetParentActor();
	}

	// Add ignored actors
	FCollisionQueryParams QueryParams;
	QueryParams.bIgnoreTouches = true;
	QueryParams.bFindInitialOverlaps = false;
	if (Owner)
	{
		QueryParams.AddIgnoredActor(Owner);
	}
	if (ParentOwner)
	{
		QueryParams.AddIgnoredActor(ParentOwner);
	}

	// The line trace
	bool HitSomething = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_GameTraceChannel2, QueryParams);
	// DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 0.5f);

	// Notify the hit
	if (HitSomething)
	{
		FHitInfo HitInfo;
		HitInfo.ActorHit = HitResult.GetActor();
		HitInfo.ComponentHit = HitResult.GetComponent();
		HitInfo.LocationHit = HitResult.Location;
		NotifyHit(HitInfo);
	}
}
