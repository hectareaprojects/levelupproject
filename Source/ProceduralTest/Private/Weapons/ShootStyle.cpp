// TODO: Copyright

#include "Weapons/ShootStyle.h"
#include "Weapons/GunComponent.h"

void UShootStyle::NotifyHit(struct FHitInfo& HitInfo)
{
	if (GunComponentOwner)
	{
		GunComponentOwner->OnHit.Broadcast(HitInfo);
	}
}
