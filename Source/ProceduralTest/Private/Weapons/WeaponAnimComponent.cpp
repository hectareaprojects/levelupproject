

#include "Weapons/WeaponAnimComponent.h"
#include "Components/MeshComponent.h"


UWeaponAnimComponent::UWeaponAnimComponent() : HipFireOffset(0.0), AimingFireOffset(0.0), TrailOffset(0.0), ShootingOffset(0.0) {

	ShootingAnim.Init(this, &UWeaponAnimComponent::ShootingAnimation, 0.1f);
	ToAimingAnim.Init(this, &UWeaponAnimComponent::ToAimingAnimation, 0.1f);

	PrimaryComponentTick.bCanEverTick = true;
	//PrimaryComponentTick.bStartWithTickEnabled = false;

}

void UWeaponAnimComponent::BeginPlay(){

	Super::BeginPlay();

	TrailOffset = HipFireOffset;

}


void UWeaponAnimComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* THisTickFunction){

	if (!ShootingAnim.HasEnded())
		ShootingAnim.Call(DeltaTime);

	if (!ToAimingAnim.HasEnded())
		ToAimingAnim.Call(DeltaTime);

	GetOwner()->SetActorRelativeLocation(TrailOffset+ShootingOffset);

}

void UWeaponAnimComponent::ShootingAnimation(float CurrentTime){

	static float WeaponDistance = 15.f;


	if (CurrentTime < 0.3f) {
		ShootingOffset = - WeaponDistance * FVector(1.f, 0.f, 0.f) * CurrentTime / 0.3f;
	}
	else {
		ShootingOffset = - WeaponDistance * FVector(1.f, 0.f, 0.f) * (1.f - CurrentTime) / 0.7f;
	}

}


void UWeaponAnimComponent::ToAimingAnimation(float CurrentTime){

	TrailOffset = CurrentTime * AimingFireOffset + (1.f - CurrentTime) * HipFireOffset;

}

void UWeaponAnimComponent::SetIsEnabled(bool bEnabled){
	
	SetComponentTickEnabled(bEnabled);

}

void UWeaponAnimComponent::SetHipFireOffset(FVector InHipFireOffset){

	HipFireOffset = InHipFireOffset;
	TrailOffset = HipFireOffset;

}

void UWeaponAnimComponent::SetAimingFireOffset(FVector InAimingFireOffset){

	AimingFireOffset = InAimingFireOffset;

}


void UWeaponAnimComponent::TriggerShootingAnimation(){

	ShootingAnim.Reset();

}

void UWeaponAnimComponent::TriggerToAimingAnimation() {

	bool bIsPreviouslyReversed = ToAimingAnim.IsReversed();
	ToAimingAnim.SetIsReversed(false);
	if (ToAimingAnim.HasEnded() && bIsPreviouslyReversed)
		ToAimingAnim.Reset();

}

void UWeaponAnimComponent::TriggerToHipAnimation() {

	bool bIsPreviouslyReversed = ToAimingAnim.IsReversed();
	ToAimingAnim.SetIsReversed(true);
	if (ToAimingAnim.HasEnded() && !bIsPreviouslyReversed)
		ToAimingAnim.Reset();

}