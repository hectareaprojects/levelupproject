// TODO: Copyright

#include "World/WorldUtilsLibrary.h"

FDynamicDelegateHandle UWorldUtilsLibrary::AddOnActorSpawned(UObject* WorldContext, const FOnWorldActorSpawned& OnActorSpawned)
{
	check(WorldContext);
	UWorld* World = WorldContext->GetWorld();
	FDelegateHandle DelegateHandle = World->AddOnActorSpawnedHandler(FOnActorSpawned::FDelegate::CreateLambda([&OnActorSpawned](AActor* SpawnedActor) {
		OnActorSpawned.ExecuteIfBound(SpawnedActor);
	}));
	return FDynamicDelegateHandle{ DelegateHandle };
}

void UWorldUtilsLibrary::RemoveOnActorSpawned(UObject* WorldContext, const FDynamicDelegateHandle& DelegateHandle)
{
	check(WorldContext)
	UWorld* World = WorldContext->GetWorld();
	World->RemoveOnActorSpawnedHandler(DelegateHandle.DelegateHandle);
}
