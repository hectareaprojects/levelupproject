// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProceduralTest.h"
#include "Modules/ModuleManager.h"


// void ProceduralTestModule::StartupModule(){
// 	MissionAssetTypeActions = MakeShared<FMissionAssetTypeActions>();
// 	FAssetToolsModule::GetModule().Get().RegisterAssetTypeActions(MissionAssetTypeActions.ToSharedRef());
// }

// void ProceduralTestModule::ShutdownModule(){
// 	if (!FModuleManager::Get().IsModuleLoaded("AssetTools")) return;
// 	FAssetToolsModule::GetModule().Get().UnregisterAssetTypeActions(MissionAssetTypeActions.ToSharedRef());
// }


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProceduralTest, "ProceduralTest" );
 