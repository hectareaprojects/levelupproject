// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "GameFramework/GameModeBase.h"
//#include "Missions/MissionManager.h"
//#include "ProceduralTestGameMode.generated.h"

// UCLASS(minimalapi)
// class AProceduralTestGameMode : public AGameModeBase
// {
// 	GENERATED_BODY()

// private:
// 	/**
// 	 * @brief The mission manager
// 	 */
// 	UPROPERTY()
// 	UMissionManager* MissionManager;

// 	/**
// 	 * @brief
// 	 */
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProceduralTestGameMode", meta = (AllowPrivateAccess = true))
// 	TSubclassOf<UMissionManager> MissionManagerClass;

// public:
// 	AProceduralTestGameMode();

// 	/**
// 	 * @brief MissionManager getter
// 	 */
// 	UFUNCTION(BlueprintCallable, Category = ProceduralTestGameMode)
// 	UMissionManager* GetMissionManager() const;

// protected:
// 	virtual void PostInitProperties() override;

// 	/**
// 	 * @brief The BeginPlay method
// 	 */
// 	virtual void BeginPlay() override;
// };

// /**
//  * @brief
//  */
// UCLASS()
// class PROCEDURALTEST_API UProceduralTestGameModeFunctionLibrary : public UBlueprintFunctionLibrary
// {
// 	GENERATED_BODY()

// 	/**
// 	 * @brief Returns the procedural test game mode object
// 	 */
// 	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralTestGameMode", meta = (WorldContext = "WorldContextObject"))
// 	static AProceduralTestGameMode* GetProceduralTestGameMode(const UObject* WorldContextObject);

// 	/**
// 	 * @brief
// 	 */
// 	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralTestGameMode", meta = (WorldContext = "WorldContextObject"))
// 	static UMissionManager* GetProceduralTestMissionManager(const UObject* WorldContextObject);
// };

// ES POSIBLE QUE SE NECESITE UN MODULO ONLY EDITOR

// /**
//  * @brief
//  */
// UCLASS(Config = Game, defaultconfig, meta = (DisplayName = "Mission Manager Settings"))
// class PROCEDURALTEST_API UMissionManagerSettings : public UDeveloperSettings
// {
// 	GENERATED_BODY()

// public:
// 	/**
// 	 * @brief
// 	 */
// 	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "General")
// 	TSoftObjectPtr<UMissionWidget> MissionWidget;
// };
