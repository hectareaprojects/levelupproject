// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WrapperValues.h"
#include "Activables/ActivatorComponent.h"
#include "ActivableComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGetActivationState, const FBool&, IsActivated);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnActivation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeactivation);

UCLASS()
class PROCEDURALTEST_API UActivableBaseComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActivableComponent")
	bool bStartActivated;

protected:
	/**
	 * @brief Represents the function to be called when an activator is activated
	 */
	UPROPERTY()
	FOnActivatedFunc OnActivatorActivated;

	/**
	 * @brief Represents the function to be called when an activator is deactivated
	 */
	UPROPERTY()
	FOnDeactivatedFunc OnActivatorDeactivated;

public:
	/**
	 * @brief Represents the function to be called when this component activates
	 */
	UPROPERTY(BlueprintAssignable, Category = "ActivableComponent")
	FOnActivation OnActivation;

	/**
	 * @brief Represents the function to be called when this component deactivates
	 */
	UPROPERTY(BlueprintAssignable, Category = "ActivableComponent")
	FOnDeactivation OnDeactivation;

private:
	/**
	 * @brief Updates the activation state of this component
	 */
	UFUNCTION()
	void UpdateActivationState();

protected:
	/**
	 * @brief Called when the activation state could change. Returns the current activation state
	 */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "ActivableComponent")
	bool GetActivationState();

	virtual void BeginPlay() override;
};

// ---------------------------------------------------------------------------------------------------------

/**
 * @brief An activable component that can use only an activator
 */
UCLASS(Blueprintable)
class PROCEDURALTEST_API USingleActivableComponent : public UActivableBaseComponent
{
	GENERATED_BODY()

public:
	/**
	 * @brief The activator that can activate this component
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActivableComponent")
	class AActor* Activator;

	/**
	 * @brief The activator component that can activate this component
	 */
	UPROPERTY()
	UActivatorComponent* ActivatorComponent;

protected:
	/**
	 * @brief The BeginPlay method.
	 */
	virtual void BeginPlay() override;
};

// ---------------------------------------------------------------------------------------------------------

/**
 * @brief An activable component that can use multiple activators
 */
UCLASS(Blueprintable)
class PROCEDURALTEST_API UMultiActivableComponent : public UActivableBaseComponent
{
	GENERATED_BODY()

public:
	/**
	 * @brief The activators that can activate this component
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActivableComponent")
	TArray<class AActor*> Activators;

	/**
	 * @brief The activators components that can activate this component
	 */
	UPROPERTY()
	TArray<UActivatorComponent*> ActivatorsComponents;

protected:
	/**
	 * @brief The BeginPlay method.
	 */
	virtual void BeginPlay() override;
};

// ----------------------------------------------------------------------------------------------------------

/**
 * @brief The component blueprints should use
 */
UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UActivableComponent : public USingleActivableComponent
{
	GENERATED_BODY()

protected:
	virtual bool GetActivationState_Implementation() override;
};
