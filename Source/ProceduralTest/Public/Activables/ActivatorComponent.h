// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActivatorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnActivated);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeactivated);
DECLARE_DYNAMIC_DELEGATE(FOnActivatedFunc);
DECLARE_DYNAMIC_DELEGATE(FOnDeactivatedFunc);

/**
 * @brief Adds activator functionality. Always starts deactivativated.
 */
UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UActivatorComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	/**
	 * @brief List of delegates to call when the component is activated
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintAssignable, Category = "ActivatorComponent", meta = (AllowPrivateAccess = true))
	FOnActivated OnActivated;

	/**
	 * @brief List of delegates to call when the component is deactivated
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintAssignable, Category = "ActivatorComponent", meta = (AllowPrivateAccess = true))
	FOnDeactivated OnDeactivated;

	/**
	 * @brief Indicates whether the component is activated
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActivatorComponent", meta = (AllowPrivateAccess = true))
	bool bActivated;

public:
	UActivatorComponent();

	/**
	 * @brief Adds a delegate to be notified when the component is activated
	 */
	UFUNCTION(BlueprintCallable, Category = "ActivatorComponent")
	void AddOnActivated(const FOnActivatedFunc& OnActivatedFunc);

	/**
	 * @brief Adds a delegate to be notified when the component is deactivated
	 */
	UFUNCTION(BlueprintCallable, Category = "ActivatorComponent")
	void AddOnDeactivated(const FOnDeactivatedFunc& OnDeactivatedFunc);

	/**
	 * @brief Activates the component if it is not already
	 */
	UFUNCTION(BlueprintCallable, Category = "ActivatorComponent")
	void ActivateAndNotify();

	/**
	 * @brief Deactivates the component if it is not already
	 */
	UFUNCTION(BlueprintCallable, Category = "ActivatorComponent")
	void DeactivateAndNotify();

	/**
	 * @brief Checks whether the component is activated
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ActivatorComponent")
	bool IsActivated() const;
};
