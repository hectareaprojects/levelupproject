// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Activables/ActivableComponent.h"
#include "LogicActivableComponent.generated.h"

/**
 * @brief An activable that must have all the activators activated
 */
UCLASS()
class PROCEDURALTEST_API UAndActivableComponent : public UMultiActivableComponent
{
	GENERATED_BODY()
protected:
	virtual bool GetActivationState_Implementation() override;
};

// -----------------------------------------------------------------------------------------------------------

/**
 * @brief An activable that must have at least one activator activated
 */
UCLASS()
class PROCEDURALTEST_API UOrActivableComponent : public UMultiActivableComponent
{
	GENERATED_BODY()

protected:
	virtual bool GetActivationState_Implementation() override;
};

// ----------------------------------------------------------------------------------------------------------

/**
 * @brief
 */
UCLASS()
class PROCEDURALTEST_API UNotActivableComponent : public USingleActivableComponent
{
	GENERATED_BODY()

protected:
	virtual bool GetActivationState_Implementation() override;
};
