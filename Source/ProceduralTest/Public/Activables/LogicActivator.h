// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LogicActivator.generated.h"

UCLASS(NotBlueprintable)
class PROCEDURALTEST_API ALogicActivator : public AActor
{
	GENERATED_BODY()

protected:
	/**
	 * @brief Makes this actor activable
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LogicActivator", meta = (AllowPrivateAccess = true))
	class UActivableBaseComponent* ActivableComponent;

	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LogicActivator", meta = (AllowPrivateAccess = true))
	class UActivatorComponent* ActivatorComponent;

public:
	ALogicActivator();	
	
protected:
	/**
	 * @brief The BeginPlay method.
	 */
	virtual void BeginPlay() override;

private:
	/**
	 * @brief Called when this actor is activated
	 */
	UFUNCTION()
	void OnActivation();

	/**
	 * @brief Called when this actor is deactivated
	 */
	UFUNCTION()
	void OnDeactivation();
};

// ------------------------------------------------------------------------------------------------

UCLASS(Blueprintable)
class PROCEDURALTEST_API AAndActivator : public ALogicActivator
{
	GENERATED_BODY()

public:
	AAndActivator();
};

// --------------------------------------------------------------------------------------------------

/**
 * @brief
 */
UCLASS(Blueprintable)
class PROCEDURALTEST_API AOrActivator : public ALogicActivator
{
	GENERATED_BODY()

public:
	AOrActivator();
};

// --------------------------------------------------------------------------------------------------

/**
 * @brief
 */
UCLASS(Blueprintable)
class PROCEDURALTEST_API ANotActivator : public ALogicActivator
{
	GENERATED_BODY()

public:
	ANotActivator();
};
