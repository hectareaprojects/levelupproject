// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "Tickable.h"
#include "AsynchronousInterp.generated.h"

template <typename T>
struct FInterpInfo
{
	T Start;
	T End;

	T GetValue(float Alpha);
};

template <typename T>
T FInterpInfo<T>::GetValue(float Alpha)
{
	return Start * (1.f - Alpha) + End * Alpha;
}

// -------------------------------------------------------------------------------------------------------------

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAsyncInterpFloatOutput, float, Value);

UCLASS()
class PROCEDURALTEST_API UAsyncInterpFloat : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FAsyncInterpFloatOutput Update;

	UPROPERTY(BlueprintAssignable)
	FAsyncInterpFloatOutput Completed;

	FInterpInfo<float> InterpInfo;

	UPROPERTY()
	float Time;

	UPROPERTY()
	float Current;

	UFUNCTION(BlueprintCallable, Category = "AsyncInterpolation", meta = (BlueprintInternalUseOnly = "true", InEnd = 1.f, InTime = 1.f))
	static UAsyncInterpFloat* InterpolationFloat(float InStart, float InEnd, float InTime);

	// Begin UBlueprintasyncactionbase interface
	virtual void Activate() override;
	// End UBlueprintasyncactionbase interface

private:
	UFUNCTION()
	bool UpdateValue(float DeltaTime);
};

// -------------------------------------------------------------------------------------------------------------

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAsyncInterpVectorOutput, FVector, Value);

UCLASS()
class PROCEDURALTEST_API UAsyncInterpVector : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FAsyncInterpVectorOutput Update;

	UPROPERTY(BlueprintAssignable)
	FAsyncInterpVectorOutput Completed;

	FInterpInfo<FVector> InterpInfo;

	UPROPERTY()
	float Time;

	UPROPERTY()
	float Current;

	UFUNCTION(BlueprintCallable, Category = "AsyncInterpolation", meta = (BlueprintInternalUseOnly = "true", InTime = 1.f))
	static UAsyncInterpVector* InterpolationVector(const FVector& InStart, const FVector& InEnd, float InTime);

	// Begin UBlueprintasyncactionbase interface
	virtual void Activate() override;
	// End UBlueprintasyncactionbase interface

private:
	UFUNCTION()
	bool UpdateValue(float DeltaTime);
};

// -------------------------------------------------------------------------------------------------------------

struct FInterpInfoFQuat
{
	FQuat	Start;
	FQuat	End;
	FVector Axis;
	float	Angle;

	FQuat GetValue(float Alpha);
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAsyncInterpQuatOutput, const FQuat&, Value);

UCLASS()
class PROCEDURALTEST_API UAsyncInterpQuat : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FAsyncInterpQuatOutput Update;

	UPROPERTY(BlueprintAssignable)
	FAsyncInterpQuatOutput Completed;

	FInterpInfoFQuat InterpInfo;

	UPROPERTY()
	float Time;

	UPROPERTY()
	float Current;
	
	UFUNCTION(BlueprintCallable, Category = "AsyncInterpolation", meta = (BlueprintInternalUseOnly = "true", InTime = 1.f))
	static UAsyncInterpQuat* InterpolationQuat(const FQuat& InStart, const FQuat& InEnd, float InTime);

	// Begin UBlueprintasyncactionbase interface
	virtual void Activate() override;
	// End UBlueprintasyncactionbase interface

private:
	UFUNCTION()
	bool UpdateValue(float DeltaTime);
};
