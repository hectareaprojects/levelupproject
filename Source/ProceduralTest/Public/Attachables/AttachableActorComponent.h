// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttachableActorComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAttachable, Warning, Warning);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttachment, class USceneComponent*, AttachedToComponent, class AActor*, AttachedToActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDetachment, USceneComponent*, DetachedFromComponent, AActor*, DetachedFromActor);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UAttachableActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/**
	 * @brief The actor's relative location when being attached to
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttachableActorComponent")
	FVector RelativeLocation;

	/**
	 * @brief The actor's relative rotation when being attached to
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttachableActorComponent")
	FRotator RelativeRotation;

public:
	/**
	 * @brief Indicates whether the owner is attached to another actor
	 */
	UFUNCTION(BlueprintCallable, Category = "AttachableActorComponent")
	bool IsActorAttached();

	/**
	 * @brief Attaches the owner to the given component
	 */
	UFUNCTION(BlueprintCallable, Category = "AttachableActorComponent")
	void AttachToComponent(USceneComponent* InComponentToAttach);

	/**
	 * @brief Attaches the owner to the root component of the given actor
	 */
	UFUNCTION(BlueprintCallable, Category = "AttachableActorComponent")
	void AttachToActor(AActor* ActorToAttach);

	/**
	 * @brief Detaches the owner from the actor it is attached to
	 */
	UFUNCTION(BlueprintCallable, Category = "AttachableActorComponent")
	void Detach();

public:
	/**
	 * @brief Called when the owner is attached to another actor
	 */
	UPROPERTY(BlueprintAssignable, Category = "AttachableActorComponent")
	FOnAttachment OnAttachment;

	/**
	 * @brief Called when the owner is detached from another actor
	 */
	UPROPERTY(BlueprintAssignable, Category = "AttachableActorComponent")
	FOnDetachment OnDetachment;
};
