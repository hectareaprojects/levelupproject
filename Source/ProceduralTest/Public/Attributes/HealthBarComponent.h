// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "HealthBarComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChanged,float,HealthPercent);

/**
 * 
 */
UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UHealthBarComponent : public UWidgetComponent{

	GENERATED_BODY()

private:

	UPROPERTY()
	class UHealthComponent* HealthComponent;

public:

	UPROPERTY(BlueprintAssignable, Category="Attributes | Health Bar")
	FOnHealthChanged OnHealthChanged;

public:

	UHealthBarComponent();

protected:

	virtual void BeginPlay() override;

private:

	UFUNCTION()
	void OnHealthChangedCaller(float HealthDiff);
	
};
