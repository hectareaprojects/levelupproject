// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDamaged, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealed, float, Health);

/**
 * A health attribute for an actor. When health reaches 0, the actor is destroyed.
 * However, this behaviour can be overriden.
 */
UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UHealthComponent : public UActorComponent
{

	GENERATED_BODY()

private:
	/**
	 * Max health
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attributes | Health", meta = (ClampMin = "1", AllowPrivateAccess = true))
	int MaxHealth;

	/**
	 * Current health
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attributes | Health", meta = (ClampMin = "0", AllowPrivateAccess = true))
	int CurrentHealth;

public:
	// Sets default values for this component's properties
	UHealthComponent();

	UFUNCTION()
	int GetMaxHealth();

	UFUNCTION()
	int GetCurrentHealth();

	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HealthComponent")
	float GetHealthPercent() const;

	/**
	 * Apply some damage
	 */
	UFUNCTION(BlueprintCallable, Category = "Attributes | Health")
	virtual void ApplyDamage(int damage);

	/**
	 * Heal
	 */
	UFUNCTION(BlueprintCallable, Category = "Attributes | Health")
	virtual void Heal(int Health);

	/**
	 * Called when health reaches 0
	 */
	UPROPERTY(BlueprintAssignable, Category = "Attributes | Health")
	FOnDeathSignature OnDeath;

	UPROPERTY(BlueprintAssignable, Category = "Attributes | Health")
	FOnDamaged OnDamaged;

	UPROPERTY(BlueprintAssignable, Category = "Attributes | Health")
	FOnHealed OnHealed;
};

inline int UHealthComponent::GetMaxHealth()
{
	return MaxHealth;
}

inline int UHealthComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

inline float UHealthComponent::GetHealthPercent() const
{
	return static_cast<float>(CurrentHealth)/static_cast<float>(MaxHealth);	
}

inline void UHealthComponent::ApplyDamage(int Damage)
{
	float PreviousHealth = CurrentHealth;
	CurrentHealth = (CurrentHealth < Damage) ? 0 : (CurrentHealth - Damage);
	OnDamaged.Broadcast(PreviousHealth - CurrentHealth);
	if (CurrentHealth == 0)
		OnDeath.Broadcast();
}

inline void UHealthComponent::Heal(int Health)
{
	float PreviousHealth = CurrentHealth;
	CurrentHealth = (Health > (MaxHealth - CurrentHealth)) ? MaxHealth : (CurrentHealth + Health);
	OnHealed.Broadcast(CurrentHealth - PreviousHealth);
}
