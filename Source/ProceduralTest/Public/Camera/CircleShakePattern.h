// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "SimpleCameraShakePattern.h"
#include "CircleShakePattern.generated.h"

UCLASS()
class PROCEDURALTEST_API UCircleShakePattern : public USimpleCameraShakePattern
{
	GENERATED_BODY()

public:
	//UCircleShakePattern(const FObjectInitializer& ObjInit);

	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Location", meta=(UIMin=0.f,ClampMin=0.f))
	float LocationRadius = 1.f;

	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Location", meta=(UIMin=0.f,ClampMin=0.f))
	float LocationFrequency = 1.f;

	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Location")
	float InitialLocationCircleAngle = 0.f;
	
	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Rotation")
	float RotationAngle = 0.f;

	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Rotation")
	float RotationFrequency = 1.f;

	/**
	* @brief
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CircleShakePattern|Rotation")
	float InitialRotationCircleAngle = 0.f;
	
private:
	/**
	* @brief
	*/
	UPROPERTY()
	float CurrentLocationDelta = 0.f;

	/**
	* @brief
	*/
	UPROPERTY()
	float CurrentRotationDelta = 0.f;
	
private:
	// UCameraShakePattern interface
	virtual void UpdateShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult) override;
	virtual void ScrubShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult) override;

	FVector GetLocation();
	FRotator GetRotation();
	void UpdateDeltas(float Delta);
	void UpdateCircles(FCameraShakeUpdateResult& OutResult);
};
