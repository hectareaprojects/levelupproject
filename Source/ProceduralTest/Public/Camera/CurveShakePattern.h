// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraShake.h"
#include "Containers/Map.h"
#include "CurveShakePattern.generated.h"

DECLARE_DYNAMIC_DELEGATE(FShakeNotifier);

/* USTRUCT(BlueprintType) */
/* struct PROCEDURALTEST_API FTimeNotifier */
/* { */
/* 	GENERATED_BODY() */

/* 	UPROPERTY() */
/* 	float Time; */

/* 	UPROPERTY() */
/* 	FShakeNotifier Notifier; */

/* 	FTimeNotifier() = default; */
/* 	FTimeNotifier(float InTime, const FShakeNotifier& InNotifier) */
/* 		: Time(InTime), Notifier(InNotifier) {} */
/* }; */

/** A wave oscillator for a single number. */
UCLASS(BlueprintType)
class PROCEDURALTEST_API UCurvePattern : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CurveShakePattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Amplitude;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CurveShakePattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Frequency;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CurveShakePattern", meta = (UIMin = 0.f, ClampMin = 0.f, UIMax = 1.f, ClampMax = 1.f))
	float Offset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CurveShakePattern")
	class UCurveFloat* Curve;

	UPROPERTY(BlueprintReadWrite, Category = "CurveShakePattern")
	float CurrentDelta;

	/* UPROPERTY() */
	TArray<TPair<float, FShakeNotifier>> Notifiers;

	UPROPERTY()
	int NextNotifierIndex;

	UCurvePattern()
		: Amplitude(1.f)
		, Frequency(1.f)
		, Curve(nullptr)
		, CurrentDelta(0.f)
	{
	}

	void Initialize();

	bool IsValid();

	void Update(float Delta);

	float GetCurveValue();

	float GetCurveValueAt(float Delta);

	void ResetIterator();

	void TryCallNotifiers();

	UFUNCTION(BlueprintCallable, Category = "CurveShakePattern")
	void AddNotifier(float Time, const FShakeNotifier& Notifier);
};

UCLASS(Blueprintable, BlueprintType)
class PROCEDURALTEST_API UCurveShakePattern : public UPlayerCameraShakePattern
{
	GENERATED_BODY()

public:
	UCurveShakePattern(const FObjectInitializer& ObjInit);

	/** Amplitude multiplier for all location curve */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Location)
	float LocationAmplitudeMultiplier = 1.f;

	/** Frequency multiplier for all location curve */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Location)
	float LocationFrequencyMultiplier = 1.f;

	/** Curve in the X axis. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Location)
	UCurvePattern* X;

	/** Curve in the Y axis. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Location)
	UCurvePattern* Y;

	/** Curve in the Z axis. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Location)
	UCurvePattern* Z;

	/** Amplitude multiplier for all rotation curve */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rotation)
	float RotationAmplitudeMultiplier = 1.f;

	/** Frequency multiplier for all rotation curve */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rotation)
	float RotationFrequencyMultiplier = 1.f;

	/** Pitch curve. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Rotation)
	UCurvePattern* Pitch;

	/** Yaw curve. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Rotation)
	UCurvePattern* Yaw;

	/** Roll Curve. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Instanced, Category = Rotation)
	UCurvePattern* Roll;

	/** FOV curve. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Instanced, Category = FOV)
	UCurvePattern* FOV;

private:
	// UCameraShakePattern interface
	virtual void StartPlayerShakePatternImpl(const FCameraShakeStartParams& Params) override;
	virtual void UpdatePlayerShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult) override;
	virtual void ScrubPlayerShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult) override;
	virtual bool IsPlayerShakeFinishedImpl() const override { return false; }

	void InitializeCurves();
	void UpdateCurves(float DeltaTime, FCameraShakeUpdateResult& OutResult);
};
