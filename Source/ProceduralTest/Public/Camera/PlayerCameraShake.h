// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShakeBase.h"
#include "SimpleCameraShakePattern.h"
#include "PlayerCameraShake.generated.h"

UCLASS(Blueprintable, HideCategories = (CameraShakePattern))
class PROCEDURALTEST_API UPlayerCameraShake : public UCameraShakeBase
{
	GENERATED_BODY()

	friend class UPlayerCameraShakePattern;

public:
	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "PlayerCameraShake")
	class UPlayerCameraShakePattern* PlayerCameraShakePattern;

public:
	/** Create a new instance of a player camera shake */
	UPlayerCameraShake(const FObjectInitializer& ObjectInitializer);

	/**
	 * @brief Retrieves the PlayerController using the camera shake
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerCameraShake")
	APlayerController* GetControllerOwner();

	/**
	 * @brief Retrieves the Pawn controlled by the controller owner
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerCameraShake")
	APawn* GetPawnOwner();

#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& e);
#endif

protected:
	/**
	 * @brief Initialize the camera shake
	 */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "PlayerCameraShake", meta = (DisplayName = Initialize))
	void ReceiveInitialize();

	/**
	 * @brief Updates the camera shake
	 */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "PlayerCameraShake", meta = (DisplayName = Update))
	void ReceiveUpdate();

	/**
	 * @brief
	 */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "PlayerCameraShake", meta = (DisplayName = Stop))
	void ReceiveStop();

	/* private: */
	/* 	void DoStartShake(const FCameraShakeStartParams& Params); */
	/* 	void DoUpdateShake(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult); */
	/* 	void DoScrubShake(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult); */
	/* 	bool DoGetIsFinished() const; */
	/* 	void DoStopShake(const FCameraShakeStopParams& Params); */
	/* 	void DoTeardownShake(); */
};

UCLASS(Abstract, BlueprintType, EditInlineNew)
class PROCEDURALTEST_API UPlayerCameraShakePattern : public USimpleCameraShakePattern
{
	GENERATED_BODY()

private:
	virtual void StartShakePatternImpl(const FCameraShakeStartParams& Params) override final;
	virtual void UpdateShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult) override final;
	virtual void ScrubShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult) override final;
	virtual bool IsFinishedImpl() const override final;
	virtual void StopShakePatternImpl(const FCameraShakeStopParams& Params) override final;
	virtual void TeardownShakePatternImpl() override final;

protected:
	virtual void StartPlayerShakePatternImpl(const FCameraShakeStartParams& Params) {}
	virtual void UpdatePlayerShakePatternImpl(const FCameraShakeUpdateParams& Params, FCameraShakeUpdateResult& OutResult) {}
	virtual void ScrubPlayerShakePatternImpl(const FCameraShakeScrubParams& Params, FCameraShakeUpdateResult& OutResult) {}
	virtual bool IsPlayerShakeFinishedImpl() const { return true; }
	virtual void StopPlayerShakePatternImpl(const FCameraShakeStopParams& Params) {}
	virtual void TeardownPlayerShakePatternImpl() {}
};
