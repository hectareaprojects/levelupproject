// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProceduralPointsOfInterest.generated.h"

/**
 * @brief
 */
USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FRoomPointsOfInterest
{
	GENERATED_BODY()

	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProceduralLevelGameMode")
	TArray<struct FPointOfInterestInfo> RoomPointsOfInterest;
};

UCLASS()
class PROCEDURALTEST_API UProceduralPointsOfInterest : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/**
	 * @brief Retrieves the points of interest of this level (SplineCameraMarkers)
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralPointsOfInterest")
	static TArray<FRoomPointsOfInterest> GetLevelPointsOfInterest(class ALevelGenerator* LevelGenerator);

	/**
	* @brief
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category="ProceduralPointsOfInterest")
	static TArray<FPointOfInterestInfo> GetRoomPointsOfInterest(const TArray<FRoomPointsOfInterest>& PointsOfInterest, int Index);
	
	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralPointsOfInterest")
	static TArray<FPointOfInterestInfo> GetBeginningTravelPointsOfInterest(const TArray<FRoomPointsOfInterest>& PointsOfInterest, int NumberOfRooms = 4);

	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralPointsOfInterest")
	static TArray<FPointOfInterestInfo> GetEndingTravelPointsOfInterest(const TArray<FRoomPointsOfInterest>& PointsOfInterest, int NumberOfRooms = 4);

	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ProceduralPointsOfInterest")
	static TArray<FPointOfInterestInfo> GetShowAllLevelPointsOfInterest(const TArray<FRoomPointsOfInterest>& PointsOfInterest, float AngleDeg = 10.f);
};
