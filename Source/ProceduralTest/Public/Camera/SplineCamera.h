// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "Components/ArrowComponent.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SplineCamera.generated.h"

/**
 * @brief Extra information that can change the behaviour of the spline camera
 */
USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FPointOfInterestExtraInfo
{
	GENERATED_BODY()

	/**
	 * @brief Indicates the order the points of interes should be taken in
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera")
	int Priority;

	/**
	 * @brief The time dilation the camera has while crossing this point of interest
	 *        More dilation more time spent.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera")
	float TimeDilation = 1.f;
};

/**
 * @brief Indicates how the spline camera should behave
 */
USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FPointOfInterestInfo
{
	GENERATED_BODY()

	/**
	 * @brief A point the camera should pass through
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera")
	FTransform Transform;

	/**
	 * @brief Additional info that can change the behaviour of the camera
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera")
	FPointOfInterestExtraInfo ExtraInfo;
};

/**
 * @brief An empty mark that should be used by level infos to indicate the camera should go through it
 */
UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UPointOfInterestComponent : public UArrowComponent
{
	GENERATED_BODY()

public:
	/**
	 * @brief Additional info to the point of interest
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera")
	FPointOfInterestExtraInfo PointOfInterestExtraInfo;

	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, Category = "SplineCamera")
	FPointOfInterestInfo GetPointOfInterestInfo();
};

DECLARE_DELEGATE(FOnTravelEnded)

// THIS SHOULD BE CALLED APointsOfInterestCamera
/**
 * @brief Camera that shows certain points of interest in a fancy way
 */
UCLASS()
class PROCEDURALTEST_API ASplineCamera : public ACameraActor
{
	GENERATED_BODY()

private:
	/**
	 * @brief The spline component indicating the current path
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	class USplineComponent* SplineComponent;

	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	USplineComponent* ForwardSplineComponent;

	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	USplineComponent* UpSplineComponent;

	/**
	 * @brief The curve to modify the velocity of the travel (must be defined between 0 and 1)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	class UCurveFloat* PositionCurve;

	/**
	 * @brief The points of interest the camera should show
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	TArray<FPointOfInterestInfo> PointsOfInterest;

	/**
	 * @brief
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SplineCamera", meta = (AllowPrivateAccess = true))
	float Duration;

	/**
	 * @brief
	 */
	UPROPERTY()
	float SplineLength;

	/**
	 * @brief Indicates if the camera is traveling
	 */
	UPROPERTY()
	bool bPlaying;

	/**
	 * @brief The current position in the spline (between 0 and 1)
	 */
	UPROPERTY()
	float CurrentPosition;

public:
	/**
	 * @brief Called when the camera travel ends
	 */
	FOnTravelEnded OnTravelEnded;

public:
	ASplineCamera();

protected:
	virtual void Tick(float DeltaSeconds) override;

public:
	/**
	 * @brief PointsOfInterest getter
	 */
	UFUNCTION(BlueprintCallable, Category = SplineCamera)
	const TArray<FPointOfInterestInfo>& GetPointsOfInterest() const;

	/**
	 * @brief PointsOfInterest setter
	 */
	UFUNCTION(BlueprintCallable, Category = SplineCamera)
	void SetPointsOfInterest(const TArray<FPointOfInterestInfo>& InPointsOfInterest);

private:
	/**
	 * @brief Updates the spline using some points of interest
	 */
	UFUNCTION()
	void UpdateSpline(const TArray<FPointOfInterestInfo>& NewPointsOfInterest);

public:
	/**
	 * @brief
	 */
	UFUNCTION(BlueprintCallable, Category = "SplineCamera")
	USplineComponent* GetSplineComponent();

	/**
	 * @brief Duration getter
	 */
	UFUNCTION(BlueprintCallable, Category = SplineCamera)
	float GetDuration() const;

	/**
	 * @brief Duration setter
	 */
	UFUNCTION(BlueprintCallable, Category = SplineCamera)
	void SetDuration(float InDuration);

	/**
	 * @brief Resets the travel
	 */
	UFUNCTION(BlueprintCallable, Category = "SplineCamera")
	void ResetTravel();

	/**
	 * @brief Pauses the travelling of the camera
	 */
	UFUNCTION(BlueprintCallable, Category = "SplineCamera")
	void PauseTravel();

	/**
	 * @brief Starts the movement of the camera
	 */
	void PlayTravel(const FOnTravelEnded& OnTravelEndedFunc = FOnTravelEnded());
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayTravelResult);

UCLASS(MinimalAPI)
class UAsyncPlayTravel : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

private:
	ASplineCamera* SplineCamera;
	float Duration;

public:
	// Called when the travel ends
	UPROPERTY(BlueprintAssignable)
	FOnPlayTravelResult Completed;

	// Shows the login UI for the currently active online subsystem, if the subsystem supports a login UI.
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true"), Category = "SplineCamera")
	static UAsyncPlayTravel* PlayTravel(ASplineCamera* SplineCamera, float Duration);

	// UBlueprintAsyncActionBase interface
	virtual void Activate() override;
	// End of UBlueprintAsyncActionBase interface

private:
	// Internal callback when the login UI closes, calls out to the public success/failure callbacks
	void OnTravelEnded();
};
