// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DebugActorComponent.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UDebugActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY()
	mutable int LogCounter = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugActorComponent")
	int DebugProperty;

	UDebugActorComponent()
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("UDebugActorComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
	}

	virtual void OnRep_IsActive() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnRep_IsActive: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnRep_IsActive();
	}
	virtual void Activate(bool bReset = false) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("Activate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::Activate(bReset);
	}
	virtual void Deactivate() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("Deactivate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::Deactivate();
	}
	virtual void SetActive(bool bNewActive, bool bReset = false) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SetActive: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::SetActive(bNewActive, bReset);
	}
	virtual void ToggleActive() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ToggleActive: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::ToggleActive();
	}
	virtual void SetAutoActivate(bool bNewAutoActivate) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SetAutoActivate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::SetAutoActivate(bNewAutoActivate);
	}
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ReplicateSubobjects: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	}
	virtual ELifetimeCondition GetReplicationCondition() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetReplicationCondition: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetReplicationCondition();
	}
	virtual void PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PreReplication: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PreReplication(ChangedPropertyTracker);
	}
	virtual bool GetComponentClassCanReplicate() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetComponentClassCanReplicate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		return Super::GetComponentClassCanReplicate();
	}
	virtual bool IsEditorOnly() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsEditorOnly: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsEditorOnly();
	}
	virtual void MarkAsEditorOnlySubobject() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("MarkAsEditorOnlySubobject: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::MarkAsEditorOnlySubobject();
	}
	virtual void OnEndOfFrameUpdateDuringTick() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnEndOfFrameUpdateDuringTick: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::OnEndOfFrameUpdateDuringTick();
	}
	virtual void OnPreEndOfFrameSync() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnPreEndOfFrameSync: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnPreEndOfFrameSync();
	}
	virtual bool ShouldActivate() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ShouldActivate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::ShouldActivate();
	}
	virtual void OnRegister() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnRegister: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnRegister();
	}
	virtual void OnUnregister() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnUnregister: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnUnregister();
	}
	virtual bool ShouldCreateRenderState() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ShouldCreateRenderState: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::ShouldCreateRenderState();
	}
	virtual void CreateRenderState_Concurrent(FRegisterComponentContext* Context) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("CreateRenderState_Concurrent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::CreateRenderState_Concurrent(Context);
	}
	virtual void SendRenderTransform_Concurrent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SendRenderTransform_Concurrent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::SendRenderTransform_Concurrent();
	}
	virtual void SendRenderDynamicData_Concurrent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SendRenderDynamicData_Concurrent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::SendRenderDynamicData_Concurrent();
	}
	virtual void SendRenderInstanceData_Concurrent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SendRenderInstanceData_Concurrent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::SendRenderInstanceData_Concurrent();
	}
	virtual void DestroyRenderState_Concurrent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("DestroyRenderState_Concurrent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::DestroyRenderState_Concurrent();
	}
	virtual void OnCreatePhysicsState() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnCreatePhysicsState: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnCreatePhysicsState();
	}
	virtual void OnDestroyPhysicsState() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnDestroyPhysicsState: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnDestroyPhysicsState();
	}
	virtual bool ShouldCreatePhysicsState() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ShouldCreatePhysicsState: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::ShouldCreatePhysicsState();
	}
	virtual bool HasValidPhysicsState() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("HasValidPhysicsState: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::HasValidPhysicsState();
	}
	virtual void RegisterComponentTickFunctions(bool bRegister) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RegisterComponentTickFunctions: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::RegisterComponentTickFunctions(bRegister);
	}
	virtual void InitializeComponent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("InitializeComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::InitializeComponent();
	}
	virtual void ReadyForReplication() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ReadyForReplication: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::ReadyForReplication();
	}
	virtual void BeginPlay() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("BeginPlay: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::BeginPlay();
	}
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("EndPlay: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::EndPlay(EndPlayReason);
	}
	virtual void UninitializeComponent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("UninitializeComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::UninitializeComponent();
	}
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("TickComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	}
	virtual void AsyncPhysicsTickComponent(float DeltaTime, float SimTime) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("AsyncPhysicsTickComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::AsyncPhysicsTickComponent(DeltaTime, SimTime);
	}
	virtual void SetComponentTickEnabled(bool bEnabled) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SetComponentTickEnabled: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::SetComponentTickEnabled(bEnabled);
	}
	virtual void SetComponentTickEnabledAsync(bool bEnabled) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SetComponentTickEnabledAsync: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::SetComponentTickEnabledAsync(bEnabled);
	}
	virtual bool IsComponentTickEnabled() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsComponentTickEnabled: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsComponentTickEnabled();
	}
	virtual bool IsReadyForOwnerToAutoDestroy() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsReadyForOwnerToAutoDestroy: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		return Super::IsReadyForOwnerToAutoDestroy();
	}
	virtual void InvalidateLightingCacheDetailed(bool bInvalidateBuildEnqueuedLighting, bool bTranslationOnly) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("InvalidateLightingCacheDetailed: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::InvalidateLightingCacheDetailed(bInvalidateBuildEnqueuedLighting, bTranslationOnly);
	}
	virtual void CheckForErrors() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("CheckForErrors: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::CheckForErrors();
	}
	virtual bool GetEditorPreviewInfo(float DeltaTime, FMinimalViewInfo& ViewOut) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetEditorPreviewInfo: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetEditorPreviewInfo(DeltaTime, ViewOut);
	}
	virtual TSharedPtr<SWidget> GetCustomEditorPreviewWidget() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetCustomEditorPreviewWidget: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		return Super::GetCustomEditorPreviewWidget();
	}
	virtual TSubclassOf<class UHLODBuilder> GetCustomHLODBuilderClass() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetCustomHLODBuilderClass: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetCustomHLODBuilderClass();
	}
	virtual void GetActorDescProperties(FPropertyPairsMap& PropertyPairsMap) const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetActorDescProperties: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::GetActorDescProperties(PropertyPairsMap);
	}
	virtual void UpdateComponentToWorld(EUpdateTransformFlags UpdateTransformFlags = EUpdateTransformFlags::None, ETeleportType Teleport = ETeleportType::None) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("UpdateComponentToWorld: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::UpdateComponentToWorld(UpdateTransformFlags, Teleport);
	}
	virtual bool RequiresGameThreadEndOfFrameUpdates() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RequiresGameThreadEndOfFrameUpdates: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		return Super::RequiresGameThreadEndOfFrameUpdates();
	}
	virtual bool RequiresGameThreadEndOfFrameRecreate() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RequiresGameThreadEndOfFrameRecreate: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		return Super::RequiresGameThreadEndOfFrameRecreate();
	}
	virtual bool RequiresPreEndOfFrameSync() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RequiresPreEndOfFrameSync: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::RequiresPreEndOfFrameSync();
	}
	virtual void OnActorEnableCollisionChanged() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnActorEnableCollisionChanged: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::OnActorEnableCollisionChanged();
	}
	virtual FString GetReadableName() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetReadableName: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetReadableName();
	}
	virtual UObject const* AdditionalStatObject() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("AdditionalStatObject: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::AdditionalStatObject();
	}
	virtual TStructOnScope<FActorComponentInstanceData> GetComponentInstanceData() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetComponentInstanceData: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetComponentInstanceData();
	}
	virtual void PostApplyToComponent() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostApplyToComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostApplyToComponent();
	}
	virtual void GetComponentChildElements(TArray<FTypedElementHandle>& OutElementHandles, const bool bAllowCreate = true) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetComponentChildElements: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::GetComponentChildElements(OutElementHandles, bAllowCreate);
	}
	virtual void BeginDestroy() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("BeginDestroy: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::BeginDestroy();
	}
	virtual bool NeedsLoadForClient() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("NeedsLoadForClient: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::NeedsLoadForClient();
	}
	virtual bool NeedsLoadForServer() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("NeedsLoadForServer: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::NeedsLoadForServer();
	}
	virtual bool NeedsLoadForEditorGame() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("NeedsLoadForEditorGame: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::NeedsLoadForEditorGame();
	}
	virtual bool IsNameStableForNetworking() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsNameStableForNetworking: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsNameStableForNetworking();
	}
	virtual bool IsSupportedForNetworking() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsSupportedForNetworking: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsSupportedForNetworking();
	}
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetLifetimeReplicatedProps: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	}
	virtual int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetFunctionCallspace: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetFunctionCallspace(Function, Stack);
	}
	virtual bool CallRemoteFunction(UFunction* Function, void* Parameters, FOutParmRec* OutParms, FFrame* Stack) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("CallRemoteFunction: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::CallRemoteFunction(Function, Parameters, OutParms, Stack);
	}
	virtual void PostInitProperties() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostInitProperties: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostInitProperties();
	}
	virtual void PostLoad() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostLoad: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostLoad();
	}
	virtual bool Rename(const TCHAR* NewName = NULL, UObject* NewOuter = NULL, ERenameFlags Flags = REN_None) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("Rename: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::Rename(NewName, NewOuter, Flags);
	}
	virtual void PostRename(UObject* OldOuter, const FName OldName) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostRename: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostRename(OldOuter, OldName);
	}
	virtual void Serialize(FArchive& Ar) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("Serialize: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::Serialize(Ar);
	}
	virtual bool Modify(bool bAlwaysMarkDirty = true) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("Modify: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::Modify(bAlwaysMarkDirty);
	}
	virtual bool CanEditChange(const FProperty* InProperty) const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("CanEditChange: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::CanEditChange(InProperty);
	}
	virtual void PreEditChange(FProperty* PropertyThatWillChange) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PreEditChange: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PreEditChange(PropertyThatWillChange);
	}
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostEditChangeProperty: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostEditChangeProperty(PropertyChangedEvent);
	}
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostEditChangeChainProperty: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::PostEditChangeChainProperty(PropertyChangedEvent);
	}
	virtual void PreEditUndo() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PreEditUndo: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PreEditUndo();
	}
	virtual void PostEditUndo() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("PostEditUndo: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::PostEditUndo();
	}
	virtual bool IsSelectedInEditor() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsSelectedInEditor: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsSelectedInEditor();
	}
	virtual void SetPackageExternal(bool bExternal, bool bShouldDirty) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("SetPackageExternal: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::SetPackageExternal(bExternal, bShouldDirty);
	}
	virtual FBox GetStreamingBounds() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetStreamingBounds: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetStreamingBounds();
	}
	virtual void AddAssetUserData(UAssetUserData* InUserData) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("AddAssetUserData: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::AddAssetUserData(InUserData);
	}
	virtual void RemoveUserDataOfClass(TSubclassOf<UAssetUserData> InUserDataClass) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RemoveUserDataOfClass: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::RemoveUserDataOfClass(InUserDataClass);
	}
	virtual UAssetUserData* GetAssetUserDataOfClass(TSubclassOf<UAssetUserData> InUserDataClass) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("GetAssetUserDataOfClass: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::GetAssetUserDataOfClass(InUserDataClass);
	}
	virtual void DestroyComponent(bool bPromoteChildren = false) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("DestroyComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::DestroyComponent(bPromoteChildren);
	}
	virtual void OnComponentCreated() override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnComponentCreated: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnComponentCreated();
	}
	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("OnComponentDestroyed: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::OnComponentDestroyed(bDestroyingHierarchy);
	}
	virtual void AddTickPrerequisiteActor(AActor* PrerequisiteActor) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("AddTickPrerequisiteActor: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::AddTickPrerequisiteActor(PrerequisiteActor);
	}
	virtual void AddTickPrerequisiteComponent(UActorComponent* PrerequisiteComponent) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("AddTickPrerequisiteComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::AddTickPrerequisiteComponent(PrerequisiteComponent);
	}
	virtual void RemoveTickPrerequisiteActor(AActor* PrerequisiteActor) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RemoveTickPrerequisiteActor: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::RemoveTickPrerequisiteActor(PrerequisiteActor);
	}
	virtual void RemoveTickPrerequisiteComponent(UActorComponent* PrerequisiteComponent) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("RemoveTickPrerequisiteComponent: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}

		Super::RemoveTickPrerequisiteComponent(PrerequisiteComponent);
	}
	virtual void ApplyWorldOffset(const FVector& InOffset, bool bWorldShift) override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("ApplyWorldOffset: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		Super::ApplyWorldOffset(InOffset, bWorldShift);
	}
	virtual bool IsNavigationRelevant() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsNavigationRelevant: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsNavigationRelevant();
	}
	virtual bool IsHLODRelevant() const override
	{
		if (this != ThisClass::StaticClass()->GetDefaultObject())
		{
			UE_LOG(LogTemp, Warning, TEXT("IsHLODRelevant: %i, Counter: %i"), DebugProperty, LogCounter);
			LogCounter++;
		}
		return Super::IsHLODRelevant();
	}
};
