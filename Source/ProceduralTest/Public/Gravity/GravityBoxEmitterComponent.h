// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GravityBoxEmitterComponent.generated.h"

USTRUCT()
struct FArrowInfo
{
	GENERATED_BODY()

	FVector Location;
	FVector Direction;
};

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UGravityBoxEmitterComponent : public UBoxComponent
{
	GENERATED_BODY()

	friend class FGravityBoxEmitterSceneProxy;

private:
	UPROPERTY()
	TArray<class UArrowComponent*> GravityArrows;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityBoxEmitter")
	int ArrowsPerDim = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityBoxEmitter")
	int Priority;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "GravityBoxEmitter")
	class UGravityGenerator* GravityGenerator;

public:
	UGravityBoxEmitterComponent();

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

protected:
	virtual void BeginPlay() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:
	TArray<FArrowInfo> CreateGravityArrows();

private:
	UFUNCTION()
	void HandleBeginComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void HandleEndComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
