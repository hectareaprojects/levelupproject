// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GravityGenerator.generated.h"

UCLASS(EditInlineNew, Abstract)
class PROCEDURALTEST_API UGravityGenerator : public UObject
{
	GENERATED_BODY()

private:
	UPROPERTY()
	class UGravityBoxEmitterComponent* GravityEmitterOwner;

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GravityGenerator")
	FVector GetGravity(const FVector& Point) const;

	UFUNCTION()
	void SetGravityEmitterOwner(UGravityBoxEmitterComponent* NewGravityEmitterOwner);

	UFUNCTION()
	UGravityBoxEmitterComponent* GetGravityEmitterOwner() const;

	virtual class FPrimitiveSceneProxy* CreateSceneProxy(UGravityBoxEmitterComponent* InComponent);
};

// -----------------------------------------------------------------------------------------------------

UCLASS()
class PROCEDURALTEST_API UConstantGravityGenerator : public UGravityGenerator
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityGenerator")
	FVector GravityDirection = FVector(0.f, 0.f, -1.f);

public:
	virtual FVector GetGravity_Implementation(const FVector& Point) const override;
};

// -----------------------------------------------------------------------------------------------------

UCLASS(Abstract)
class PROCEDURALTEST_API UPointGravityGenerator : public UGravityGenerator
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityGenerator")
	FVector PointAnchor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualHelp")
	bool bDrawVisualHelp = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualHelp", meta = (UIMin = 5.f, ClampMin = 5.f))
	float SphereRadius = 100.f;

public:
	FVector GetCenterOfGravity(bool& bSuccess) const;
};

UCLASS()
class PROCEDURALTEST_API UPointAtractorGravityGenerator : public UPointGravityGenerator
{
	GENERATED_BODY()

public:
	virtual FVector GetGravity_Implementation(const FVector& Point) const override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy(UGravityBoxEmitterComponent* InComponent) override;
};

UCLASS()
class PROCEDURALTEST_API UPointRetractorGravityGenerator : public UPointGravityGenerator
{
	GENERATED_BODY()

public:
	virtual FVector GetGravity_Implementation(const FVector& Point) const override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy(UGravityBoxEmitterComponent* InComponent) override;
};

// -----------------------------------------------------------------------------------------------------

UCLASS(Abstract)
class PROCEDURALTEST_API ULineGravityGenerator : public UGravityGenerator
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GravityGenerator")
	FVector LineAnchor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualHelp")
	bool bDrawVisualHelp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualHelp", meta = (UIMin = 3, ClampMin = 3))
	int NumCircles = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualHelp", meta = (UIMin = 10.f, ClampMin = 10.f))
	float CircleRadius = 10.f;

protected:
	FVector GetPointProjection(const FVector& Point, bool& bSuccess) const;
};

UCLASS()
class PROCEDURALTEST_API ULineAtractorGravityGenerator : public ULineGravityGenerator
{
	GENERATED_BODY()

public:
	virtual FVector GetGravity_Implementation(const FVector& Point) const override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy(UGravityBoxEmitterComponent* InComponent) override;
};

UCLASS()
class PROCEDURALTEST_API ULineRetractorGravityGenerator : public ULineGravityGenerator
{
	GENERATED_BODY()

public:
	virtual FVector GetGravity_Implementation(const FVector& Point) const override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy(UGravityBoxEmitterComponent* InComponent) override;
};
