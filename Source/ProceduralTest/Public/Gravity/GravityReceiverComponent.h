// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Templates/Function.h"
#include "GravityReceiverComponent.generated.h"

USTRUCT()
struct PROCEDURALTEST_API FGravityInfo
{
	GENERATED_BODY()

	UPROPERTY()
	int Priority;

	UPROPERTY()
	class UGravityGenerator* GravityGenerator;
};


UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UGravityReceiverComponent : public USphereComponent
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TArray<FGravityInfo> GravityInfos;

	UPROPERTY()
	mutable FVector LastCalculatedGravity = FVector(0.f, 0.f, -1.f);

public:
	UGravityReceiverComponent();

	UFUNCTION(BlueprintCallable, Category = "GravityReceiverComponent")
	FVector GetGravity() const;

	UFUNCTION(BlueprintCallable, Category = "GravityReceiverComponent")
	void SetDefaultGravity(const FVector& DefaultGravity);

	void AddGravityInfo(class UGravityGenerator* GravityGenerator, int Priority);

	void RemoveGravityInfo(class UGravityGenerator* GravityGenerator);
};
