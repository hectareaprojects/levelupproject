// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "Damage.generated.h"


USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FDamageInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	int Damage;	
};

UCLASS(BlueprintType, Blueprintable)
class PROCEDURALTEST_API UDamage : public UObject
{
	GENERATED_BODY()

public:
	/**
	 * @brief The damage info
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (AllowPrivateAccess = true))
	FDamageInfo Damage;
};
