// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "HittableInterface.generated.h"

UINTERFACE(MinimalAPI)
class UHittableInterface : public UInterface
{
	GENERATED_BODY()
};

class PROCEDURALTEST_API IHittableInterface : public IInterface
{
	GENERATED_BODY()

public:
	/**
	* @brief
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="HittableInterface")
	void Hit(class AActor* Instigator, class AActor* Source, const struct FDamageInfo& Damage);	
};
