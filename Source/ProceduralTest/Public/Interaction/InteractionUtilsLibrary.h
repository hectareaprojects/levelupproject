// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "InteractionUtilsLibrary.generated.h"

UCLASS()
class PROCEDURALTEST_API UInteractionUtilsLibrary: public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/**
	* @brief
	*/
	UFUNCTION(BlueprintCallable, Category="InteractionUtilsLibrary")
	static void AdjustLookAtBoxToActorBounds(class ULookAtBoxComponent* LookAtBox, class AActor* Item);
};
