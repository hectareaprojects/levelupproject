// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "InteractiveBoxComponent.generated.h"

/**
 * Component that will enable or disable the input for interacting depending on the target view and where it is looking at.
 */
UCLASS(Blueprintable, ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UInteractiveBoxComponent : public UBoxComponent
{

	GENERATED_BODY()

private:
	/**
	 * The InteractiveBox that is displaying a widget
	 */
	static UInteractiveBoxComponent* ScreenOwner;

	/**
	 * The actor interacting with the InteractiveBox
	 */
	UPROPERTY()
	AActor* InteractingActor;

	/**
	 * The Widgets will be shown when a TargetView looks at a LookAtBox.
	 */
	UPROPERTY()
	TArray<class ULookAtBoxComponent*> LookAtBoxComponents;

	/**
	 * The Camera Component of a TargetView that has overlapped this InteractiveBox
	 */
	UPROPERTY()
	class UCameraComponent* CameraComponent;

	/**
	 * @brief The interaction mapping context that will be enabled when the interaction can occur
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "InteractiveBoxComponent", meta = (AllowPrivateAccess = true))
	class UInputMappingContext* InputMapping;

	/**
	 * Class of the Widget that will appear when the TargetView looks at a LookAtBox
	 */
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
	TSubclassOf<UUserWidget> WidgetClass;

	/**
	 * The Widget of type WidgetClass that will be shown.
	 */
	UPROPERTY()
	class UUserWidget* Widget;

	/**
	 * Indicates whether a TargetView is overlapping this component.
	 */
	UPROPERTY()
	bool bInside;

	/**
	 * Indicates whether the Widget is displayed.
	 */
	UPROPERTY()
	bool bDisplayed;

	/**
	 * Indicates whether this InteractiveBox is enabled
	 */
	UPROPERTY()
	bool bEnabled;

	/**
	 * The maximum distance the player should be from a LookAtBox.
	 */
	UPROPERTY()
	float MaxDistFromLookAtBox;

public:
	/**
	 * Indicates the priority of this InteractiveBox. InteractiveBox with
	 * higher priority will be then screen owner even another InteractiveBox
	 * is already the owner.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction", meta = (ClampMin = 0))
	int Priority;

	/**
	 * Constructor
	 */
	UInteractiveBoxComponent();

private:
	/**
	 * Enables the mapping context and shows the widget
	 */
	UFUNCTION()
	void Show();

	/**
	 * Disables the mapping context and hides the widget
	 */
	UFUNCTION()
	void Hide();

	/**
	 * Checks whether this InteractiveBox is the screen owner.
	 */
	UFUNCTION()
	bool IsScreenOwner() const;

	/**
	 * Checks whether the screen is owned.
	 */
	UFUNCTION()
	static bool IsScreenOwned();

	/**
	 * Checks whether this InteractiveBox can be the screen owner.
	 */
	UFUNCTION()
	bool CanBeScreenOwner() const;

	/**
	 * Set this InteractiveBox as the screen owner.
	 */
	UFUNCTION()
	void SetScreenOwner();

public:
	/**
	 * BeginPlay method
	 */
	virtual void BeginPlay() override;

	/**
	 * Tick method
	 */
	virtual void TickComponent(float Delta, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * Enables this component
	 */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void Enable();

	/**
	 * Diasables this component
	 */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void Disable();

	/**
	 * Checks whether this InteractiveBox is enabled.
	 */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	bool IsEnabled() const;

protected:
	/**
	 * Checks if this InteractiveBox can show a widget.
	 * By default, it returns true.
	 * WARNING: This function is called on Tick.
	 */
	UFUNCTION(BlueprintPure, BlueprintCallable, BlueprintNativeEvent, Category = "Interaction")
	bool CanInteract() const;

public:
	/**
	 * Retrieves the actor that is interacting with the InteractiveBox
	 */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	AActor* GetInteractingActor() const;

	/**
	 * Event that is called when an actor overlaps this component
	 */
	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/**
	 * Event that is called when an actor does not overlap this component anymore
	 */
	UFUNCTION()
	void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
