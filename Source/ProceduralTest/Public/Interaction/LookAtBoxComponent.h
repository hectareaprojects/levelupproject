// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "LookAtBoxComponent.generated.h"

class UInteractiveBoxComponent;

/**
 * 
 */
UCLASS(ClassGroup = "Interaction", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API ULookAtBoxComponent : public UBoxComponent{

	GENERATED_BODY()

private:

	/**
	* Indicates whether the Widget can be displayed.
	*/
	UPROPERTY()
	bool bEnabled;
	

public:

	/**
	* Constructor
	*/
	ULookAtBoxComponent();

public:

	/**
	* Enables the possiblity of interacting with this component
	*/
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void EnableInteraction();

	/**
	* Disables the possibility of interacting with this component
	*/
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void DisableInteraction();

	/**
	* Checks whether this LookAtBox is enabled. 
	*/
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	bool IsEnabled();

	
};


inline void ULookAtBoxComponent::EnableInteraction() {
	bEnabled = true;
}

inline void ULookAtBoxComponent::DisableInteraction() {
	bEnabled = false;
}

inline bool ULookAtBoxComponent::IsEnabled() {
	return bEnabled;
}