
#pragma once

#include <functional>


/**
 * @brief Function that is parametrized with one float parameter that goes from zero to one.
 * @tparam T The type that implements the actual function.
*/
template<typename T>
class FInterpolationFunction {

private:

    /**
     * @brief The current value of the interpolation parameter passed to the function.
    */
    float CurrentTime;

    /**
     * @brief Times that this animation can be done in one second.
    */
    float Frequency;

    /**
     * @brief Indicates if the interpolation should be done in reverse order.
    */
    bool bReversed;

    /**
     * @brief The object that will call the function.
    */
    T* Object;

    /**
     * @brief The actual function.
    */
    void (T::* Function)(float);

public:

    /**
     * @brief Default constructor. Don't use it. This needs to be public 'because Unreal'.  
     *        Use Init to initialize the function.
    */
    FInterpolationFunction() = default;

    /**
     * @brief Initialize the InterpolationFunction.
     * @param NewObject The object that has the actual function.
     * @param NewFunction The actual function that is a function member of NewObject.
     * @param Duration How much time, in seconds, will last the animation.
    */
    void Init(T* NewObject, void (T::* NewFunction)(float), float Duration, bool bInReversed = false);

    /**
     * @brief Call the actual function with the parameter value incremented by DeltaTime.
     *        It does nothing if animation has ended.
     *        Use this method in a Tick function. 
     * @param DeltaTime This should be the time that has passed since the last time Call was called.
    */
    void Call(float DeltaTime);

    /**
     * @brief Set if the interpolation should be done in reverse order.
     * @param bInReversed 
    */
    void SetIsReversed(bool bInReversed);

    /**
     * @brief Checks whether the interpolation is in reverse order.
     * @return true if the interpolation is in reverse order.
    */
    bool IsReversed() const;

    /**
     * @brief Sets the parameter value of the animation to zero.
    */
    void Reset();

    /**
     * @brief Checks whether the animation has ended.
     * @return true if the animation has ended.
    */
    bool HasEnded() const;

};


// ------ Implementation ------

template<typename T>
void FInterpolationFunction<T>::Init(T* NewObject, void (T::* NewFunction)(float), float Duration, bool bInReversed) {

    Object = NewObject; 
    Function = NewFunction;
    CurrentTime = -1.f;
    Frequency = (1.f / Duration);
    bReversed = bInReversed;

}

template<typename T>
void FInterpolationFunction<T>::Call(float DeltaTime) {

    if (!HasEnded()) {
        (Object->*Function)(CurrentTime);
        if (bReversed)
            CurrentTime -= DeltaTime * Frequency;
        else
            CurrentTime += DeltaTime * Frequency;
    }

}

template<typename T>
void FInterpolationFunction<T>::SetIsReversed(bool bInReversed) {

    bReversed = bInReversed;

}

template<typename T>
bool FInterpolationFunction<T>::IsReversed() const {

    return bReversed;

}

template<typename T>
void FInterpolationFunction<T>::Reset() {

    CurrentTime = bReversed ? 1.f : 0.f;

}

template<typename T>
bool FInterpolationFunction<T>::HasEnded() const{

    return (CurrentTime < 0.f) || (CurrentTime > 1.f);

}