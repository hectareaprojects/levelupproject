#pragma once

#include "CoreMinimal.h"
#include "Item.generated.h"

/**
 * @brief
*/
UCLASS( )
class PROCEDURALTEST_API UItem : public UObject {

    GENERATED_BODY()

private:

    /**
     * @brief The name of the item
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item", meta=(AllowPrivateAccess = true, BindWidget))
    FName Name;

    /**
     * @brief The description of the item
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item", meta=(AllowPrivateAccess = true, BindWidget))
    FString Description;

    /**
     * @brief
    */
    // UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item", meta=(AllowPrivateAccess = true, BindWidget))
    // UImage* Icon;

public:
    /**
     * @brief The UItem constructor.
    */
    //UItem();

};