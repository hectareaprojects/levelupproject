// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelSpace.generated.h"

UCLASS(Abstract)
class PROCEDURALTEST_API ALevelSpace : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="LevelSpace", meta=(AllowPrivateAccess = true))
	class UShapeComponent* SpaceComponent = nullptr;
};

// ----------------------------------------------------------------------------------------------------------

UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API ALevelBoxSpace : public ALevelSpace
{
	GENERATED_BODY()

public:
	ALevelBoxSpace();
};

// ----------------------------------------------------------------------------------------------------------

UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API ALevelCapsuleSpace : public ALevelSpace
{
	GENERATED_BODY()

public:
	ALevelCapsuleSpace();
};

// ---------------------------------------------------------------------------------------------------------

UCLASS(ClassGroup = "Custom", meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API ALevelSphereSpace : public ALevelSpace
{
	GENERATED_BODY()

public:
	ALevelSphereSpace();
};
