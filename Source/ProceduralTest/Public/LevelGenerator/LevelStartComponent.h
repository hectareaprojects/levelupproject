// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "LevelStartComponent.generated.h"

/**
 *
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class ULevelStartComponent : public UArrowComponent
{
	GENERATED_BODY()

public:
	ULevelStartComponent();	
};
