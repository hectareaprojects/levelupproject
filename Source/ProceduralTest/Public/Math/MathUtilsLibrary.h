// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MathUtilsLibrary.generated.h"

UCLASS()
class PROCEDURALTEST_API UMathUtilsLibrary: public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/**
	* @brief Returns the max value of an array 
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Max(const TArray<float>& X);
	
	/**
	* @brief Returns the min value of an array 
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Min(const TArray<float>& X);
	
	/**
	* @brief Returns the range of an array (maximum - minimum)
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Range(const TArray<float>& X);

	/**
	* @brief Returns E(X)
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Mean(const TArray<float>& X);

	/**
	* @brief Returns the mean of several Vectors
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static FVector MeanVector(const TArray<FVector>& X);

	/**
	* @brief Returns Var(X)=E(X^2)-E(X)^2
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Variance(const TArray<float>& X);

	/**
	* @brief Returns Cov(X,Y)=E(XY)-E(X)E(Y)
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static float Covariance(const TArray<float>& X, const TArray<float>& Y);

	/**
	* @brief Returns the regression line of Y over X 
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static void RegressionLine(const TArray<float>& X, const TArray<float>& Y, float& OutXOrigin, float& OutYOrigin, float& Slope);

	/**
	* @brief Returns the regression line of a list of 2D points
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static void RegressionLine2D(const TArray<FVector2D>& P, FVector2D& OutOrigin, FVector2D& OutDirection);

	/**
	* @brief Returns the regression line of a list of 3D points
	*/
	UFUNCTION(BlueprintCallable, Category="MathUtilsLibrary")
	static void RegressionLine3D(const TArray<FVector>& P, FVector& OutOrigin, FVector& OutDirection);
};
