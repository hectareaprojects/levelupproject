#pragma once

#include "CoreMinimal.h"
#include <type_traits>

/** Facility macros to declare and define mixin attributes */

/** Declares a mixin attribute in the mixin class */
#define DECLARE_MIXIN_PROPERTY(I,M,T)                                                       \
    virtual T I##_Get##M() = 0;                                                             \
    using MixinType_##M = T;                                                                \
    T Get##M(){ return I##_Get##M(); }                                              

// MSVC patch
#define EXPAND(X) X

/** Default parameter trick seen here: https://stackoverflow.com/questions/3046889/optional-parameters-with-c-macros */
#define DEFINE_MIXIN_SHORT(I,M)                                                             \
    EXPAND(virtual I::MixinType_##M I##_Get##M () override { return M; })

#define DEFINE_MIXIN_LONG(I,M,V)                                                            \
    EXPAND(virtual I::MixinType_##M I##_Get##M () override { return V; })

#define GET_4TH_ARG(arg1, arg2, arg3, arg4, ...) arg4

#define DEFINE_MIXIN_CHOOSER(...)                                                           \
    EXPAND(GET_4TH_ARG(__VA_ARGS__, DEFINE_MIXIN_LONG, DEFINE_MIXIN_SHORT))

/** Defines a mixin attribute in a class inheriting from a mixin */
#define DEFINE_MIXIN_PROPERTY(...) EXPAND(DEFINE_MIXIN_CHOOSER(__VA_ARGS__)(__VA_ARGS__))

/** Inherits a mixin property declared in a parent interface */
#define INHERIT_MIXIN_PROPERTY(I,P,M)                                                       \
    DECLARE_MIXIN_PROPERTY(I,M,decltype((std::declval<P>().*std::declval<decltype(&P::P##_Get##M)>())()))                                       \
    DEFINE_MIXIN_PROPERTY(P,M,Get##M())