// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "SecondOrderDynamicsComponent.generated.h"

// T must implement +(T) and *(float) operators
// Also, the values must not be circular (like angles in Rotators)
template <typename T>
struct PROCEDURALTEST_API FSecondOrderDynamicValue
{
	T LastTargetValue;
	T Velocity;
	T Value;

	T ComputeNewValue(T TargetValue, float Delta, float K1, float K2, float K3);
};

template <typename T>
T FSecondOrderDynamicValue<T>::ComputeNewValue(T TargetValue, float Delta, float K1, float K2, float K3)
{
	// Credits to t3ssel8r  https://youtu.be/KPoeNZZ6H4s?list=PLiq5J0OvtRF3RNQU2sz-nFfGUwk-DawD7&t=795
	T DiffValue = TargetValue + LastTargetValue * -1.f;
	Value = Value + Velocity * Delta;
	float K2_Stable = FMath::Max(K2, 1.1f * Delta * (Delta / 4.f + K1 / 2.f));
	Velocity = Velocity
		+ (TargetValue + (Value + Velocity * K1) * -1.f) * (Delta / K2_Stable)
		+ DiffValue * (K3 / K2_Stable);
	LastTargetValue = TargetValue;
	return Value;
}

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FSecondOrderDynamicInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Frequency = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Damping = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent")
	float InitialResponse = 0.f;

	void GetKValues(float& K1, float& K2, float& K3);
};

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API USecondOrderDynamicsComponent : public USceneComponent
{
	GENERATED_BODY()

private:
	FSecondOrderDynamicValue<FVector> SecondOrderLocation;
	FSecondOrderDynamicValue<FVector> SecondOrderRotation;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent")
	bool bLockLocation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent", meta = (EditCondition = "!bLockLocation"))
	FSecondOrderDynamicInfo LocationParameters;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent")
	bool bLockRotation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SecondOrderDynamicsComponent", meta = (EditCondition = "!bLockRotation"))
	FSecondOrderDynamicInfo RotationParameters;

public:
	USecondOrderDynamicsComponent();

private:
	UFUNCTION()
	void ComputeNewTransform(float Delta);

protected:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void BeginPlay() override;
};
