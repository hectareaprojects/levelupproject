
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SimpleMovementComponent.generated.h"

/**
 * @brief
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API USimpleMovementComponent : public UActorComponent
{

	GENERATED_BODY()

private:
	UPROPERTY()
	float CurrentPitch = 0.f;

public:
	/**
	 * @brief Moves the owner
	 */
	UFUNCTION(BlueprintCallable, Category = "SimpleMovementComponent")
	void Move(const FVector2D& Direction);

	/**
	 * @brief Rotates the owner looking direction
	 */
	UFUNCTION(BlueprintCallable, Category = "SimpleMovementComponent")
	void AddLookRotation(const FVector2D& Direction);

	/**
	 * @brief Makes the owner jump
	 */
	UFUNCTION(BlueprintCallable, Category = "SimpleMovementComponent")
	void Jump();
};
