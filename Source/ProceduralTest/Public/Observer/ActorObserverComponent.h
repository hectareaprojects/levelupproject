// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActorObserverComponent.generated.h"

UCLASS()
class PROCEDURALTEST_API UActorObserverComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;	
};
