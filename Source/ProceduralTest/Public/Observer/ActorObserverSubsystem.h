// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "ActorObserverSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAllActorsDestroyed);

class AActor;

UCLASS()
class PROCEDURALTEST_API UActorObserverSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TArray<AActor*> ObservedActors;

public:
	UPROPERTY(BlueprintAssignable, Category = "ActorObserverSubsystem")
	FOnAllActorsDestroyed OnAllActorsDestroyed;

	UFUNCTION(BlueprintCallable, Category = "ActorObserverSubsystem")
	void Observe(AActor* Actor);

private:
	UFUNCTION()
	void HandleOnActorDestroyed(AActor* DestroyedActor);
};
