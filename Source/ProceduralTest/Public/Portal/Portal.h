// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

UCLASS()
class PROCEDURALTEST_API APortal : public AActor{

	GENERATED_BODY()

private:

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Portal", meta=( AllowPrivateAccess=true ) )
	class UStaticMeshComponent* Plane;

	UPROPERTY()
	class USceneCaptureComponent2D* OtherPortalCamera;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Portal", meta=( AllowPrivateAccess=true ) )
	class UTextureRenderTarget2D* TextureRenderTarget;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Portal", meta = (AllowPrivateAccess = true ) )
	class UMaterialInterface* PlaneMaterial;

	UPROPERTY()
	class UMaterialInterface* DefaultPlaneMaterial;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Portal", meta=( AllowPrivateAccess=true ) )
	APortal* LinkedPortal;

	UPROPERTY()
	bool bEnabled;

	
public:	
	// Sets default values for this actor's properties
	APortal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION( BlueprintCallable, Category="Portal" )
	void SetPortalEnabled(bool bNewEnabled);

	UFUNCTION( BlueprintCallable, Category="Portal" )
	bool GetPortalEnabled();

};


inline bool APortal::GetPortalEnabled() {
	return bEnabled;
}