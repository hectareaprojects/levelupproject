// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "RayCast/SmartRayCastComponent.h"
#include "SingleRayCastPattern.generated.h"

UCLASS()
class PROCEDURALTEST_API USingleRayCastPattern : public URayCastPattern
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USingleRayCastPattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float RayLength = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USingleRayCastPattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float RayAngleDispersion = 0.f;

public:
	virtual TArray<FRayCastInfo> CreateRayCastInfo() override;
};
