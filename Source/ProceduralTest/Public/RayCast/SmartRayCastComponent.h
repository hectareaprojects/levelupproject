// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SmartRayCastComponent.generated.h"

class USceneComponent;

struct FRayCastInfo
{
	FVector Start;
	FVector End;

	FRayCastInfo(const FVector& InStart, const FVector& InEnd)
		: Start(InStart), End(InEnd) {}
};

UCLASS(Abstract, EditInlineNew)
class PROCEDURALTEST_API URayCastPattern : public UObject
{
	GENERATED_BODY()

	friend class USmartRayCastComponent;

private:
	UPROPERTY()	
	USmartRayCastComponent* RayCastComponentOwner;

protected:
	USmartRayCastComponent* GetRayCastComponentOwner() { return RayCastComponentOwner; }
	USceneComponent*		GetRayCastSource();

public:
	virtual TArray<FRayCastInfo> CreateRayCastInfo() { return TArray<FRayCastInfo>(); }
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnRayCast, const FVector&, Start, const FVector&, End, bool, HitSomething, const FHitResult&, HitResult);

UCLASS(meta=(BlueprintSpawnableComponent))
class PROCEDURALTEST_API USmartRayCastComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartRayCastComponent")
	TArray<AActor*> IgnoredActors;

	UPROPERTY(BlueprintReadWrite, Category = "USmartRayCastComponent")
	USceneComponent* RayCastSource;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartRayCastComponent")
	TEnumAsByte<ECollisionChannel> CollisionChannel;

	UPROPERTY(EditAnywhere, Instanced, Category = "USmartRayCastComponent")
	URayCastPattern* RayCastPattern;

	UPROPERTY(BlueprintAssignable)
	FOnRayCast OnRayCast;

public:
	UFUNCTION(BlueprintCallable, Category = "USmartRayCastComponent")
	void Trace();

protected:
	virtual void BeginPlay() override;
	virtual void OnRegister() override;
};
