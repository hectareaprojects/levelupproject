// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Shake/ComponentShakeSubsystem.h"
#include "ComponentReboundShakePattern.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FDeviatedCoordinate
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern", meta=(UIMin=0.f, ClampMin=0.f))
	float Deviation;
};

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FDeviatedVector
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate X;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate Y;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate Z;
};

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FDeviatedRotator
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate Pitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate Yaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedCoordinate Roll;
};

UCLASS(Blueprintable, BlueprintType)
class PROCEDURALTEST_API UComponentReboundShakePattern : public UComponentShakePattern
{
	GENERATED_BODY()

public:
	/** The turning point (between 0 and 1) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern", meta = (UIMax = 1.f, UIMin = 0.f, ClampMax = 1.f, ClampMin = 0.f))
	float TurningPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FDeviatedRotator Rotation;

private:
	UPROPERTY()
	float TotalTime;

	UPROPERTY()
	FTransform GoalTransform;

public:
	UComponentReboundShakePattern();	
	
protected:
	virtual void Initialize(float InTotalTime) override;
	virtual FTransform Update(float DeltaTime, float ElapsedTime) override;
};
