// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "ComponentShakeSubsystem.generated.h"

UCLASS(Abstract, Blueprintable)
class PROCEDURALTEST_API UComponentShakePattern : public UObject
{
	GENERATED_BODY()

	friend class UComponentShakeSubsystem;
	friend struct FComponentShakeInfo;

protected:
	virtual void Initialize(float TotalTime) {}
	virtual void Deinitialize() {}
	virtual FTransform Update(float DeltaTime, float ElapsedTime) { return FTransform(); }
	virtual void Restart() {}
};

DECLARE_DELEGATE(FOnShakeFinished);

USTRUCT()
struct PROCEDURALTEST_API FComponentShakeInfo
{
	GENERATED_BODY()

	FComponentShakeInfo() = default;
	FComponentShakeInfo(float InTotalTime, USceneComponent* InSceneComponent, UComponentShakePattern* InShakePattern);
	FComponentShakeInfo(float InTotalTime, USceneComponent* InSceneComponent, UComponentShakePattern* InShakePattern,
		const FOnShakeFinished& OnShakeCompletedFunc, const FOnShakeFinished& OnShakeRemovedFunc);

	UPROPERTY()
	float ElapsedTime = 0.f;

	UPROPERTY()
	float TotalTime;

	FOnShakeFinished OnShakeCompleted;
	FOnShakeFinished OnShakeRemoved;

	UPROPERTY()
	USceneComponent* SceneComponent;

	UPROPERTY()
	UComponentShakePattern* ShakePattern;

	void Update(float DeltaTime);
	void Restart();
	bool IsCompleted();
	void NotifyCompletion();
	void NotifyRemoval();
};

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FComponentShakeInfos
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FComponentShakeInfo> ComponentShakeInfos;
};

UCLASS()
class PROCEDURALTEST_API UComponentShakeSubsystem : public UTickableWorldSubsystem
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TMap<USceneComponent*, FComponentShakeInfos> ShakeInfos;

public:
	virtual void Tick(float DeltaTime);
	virtual TStatId GetStatId() const { return TStatId(); }

	void AddShake(float TotalTime, USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass, const FOnShakeFinished& OnShakeCompletedFunc, const FOnShakeFinished& OnShakeRemovedFunc);

	FComponentShakeInfo* FindShakeInfo(USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass);

	UFUNCTION(BlueprintCallable, Category = "ComponentShakeSubsystem")
	bool HasShake(USceneComponent* SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass);

	/** Removes all the shakes from the given component */
	UFUNCTION(BlueprintCallable, Category = "ComponentShakeSubsystem")
	void RemoveShake(USceneComponent* const& SceneComponent, TSubclassOf<UComponentShakePattern> ShakePatternClass, bool& bSuccess);
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOutputPin);

UCLASS()
class PROCEDURALTEST_API UAddAsyncShakeNode : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOutputPin Completed;

	UPROPERTY(BlueprintAssignable)
	FOutputPin Removed;

	UPROPERTY()
	UComponentShakeSubsystem* ShakeSubsystem;

	UPROPERTY()
	float TotalTime;

	UPROPERTY()
	TSubclassOf<UComponentShakePattern> ShakePatternClass;

	UPROPERTY()
	USceneComponent* SceneComponent;

	UFUNCTION()
	void OnCompletion();

	UFUNCTION()
	void OnRemoval();

	UFUNCTION(BlueprintCallable, Category = "ComponentShakeSubsystem", meta = (TotalTime = 1.f))
	static UAddAsyncShakeNode* AddShake(UComponentShakeSubsystem* const& InShakeSubsystem, float InTotalTime, USceneComponent* const& InSceneComponent, TSubclassOf<UComponentShakePattern> InShakePatternClass);

protected:
	virtual void Activate() override;
};
