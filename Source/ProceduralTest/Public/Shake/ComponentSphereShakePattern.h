// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Shake/ComponentShakeSubsystem.h"
#include "ComponentSphereShakePattern.generated.h"

UCLASS()
class PROCEDURALTEST_API UComponentSphereShakePattern: public UComponentShakePattern
{
	GENERATED_BODY()

private:
	FVector Start;
	FVector End;
	float Alpha;

protected:

private:
};
