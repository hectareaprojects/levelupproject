// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Shake/SmartShakeComponent.h"
#include "SmartBoomerangShakePattern.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FBoomerangDeviatedCoordinate
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Deviation;
};

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FBoomerangDeviatedVector
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate X;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate Y;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate Z;
};

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FBoomerangDeviatedRotator
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate Pitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate Yaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComponentReboundShakePattern")
	FBoomerangDeviatedCoordinate Roll;
};

UCLASS()
class PROCEDURALTEST_API USmartBoomerangShakePattern : public USmartShakePattern
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartBoomerangShakePattern")
	float TotalTime;

	/** The turning point (between 0 and 1). Indicates when the extreme point will be reached. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartBoomerangShakePattern", meta = (UIMax = 1.f, UIMin = 0.f, ClampMax = 1.f, ClampMin = 0.f))
	float TurningPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartBoomerangShakePattern")
	FBoomerangDeviatedVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USmartBoomerangShakePattern")
	FBoomerangDeviatedRotator Rotation;

private:
	UPROPERTY()
	FTransform GoalTransform;

public:
	USmartBoomerangShakePattern();

protected:
	virtual void Start() override;
	virtual bool Update(const FShakeInfo& ShakeInfo, FShakeResult& ShakeResult) override;
};
