// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SmartShakeComponent.generated.h"

struct FShakeInfo
{
	float DeltaTime;
	float ElapsedTime;
};

struct FShakeResult
{
	FTransform Transform;
};

UCLASS(Abstract, Blueprintable)
class PROCEDURALTEST_API USmartShakePattern : public UObject
{
	GENERATED_BODY()

	friend class USmartShakeComponent;

private:
	USmartShakeComponent* ShakeComponentOwner;

protected:
	USmartShakeComponent* GetShakeComponentOwner();

	/**
	 * Returns true when the shake must continue. False to stop.
	 */
	virtual bool Update(const FShakeInfo& ShakeInfo, FShakeResult& Result) { return false; }
	virtual void Start() {}
	virtual void Restart() {}
	virtual void Stop() {}
};

DECLARE_DELEGATE(FShakeDelegate);

UCLASS()
class PROCEDURALTEST_API USmartShakeComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	bool  bActive;
	float ElapsedTime;

	FShakeDelegate OnRestarted;
	FShakeDelegate OnCompleted;
	FShakeDelegate OnStopped;

public:
	UPROPERTY()
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, Instanced, Category = "USmartShakeComponent")
	USmartShakePattern* SmartShakePattern;

public:
	USmartShakeComponent();

protected:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OnRegister() override;

private:
	void UpdateSceneComponent(const FShakeResult& ShakeResult);
	void StartShake();

public:
	void StartShake(const FShakeDelegate& InOnRestarted, const FShakeDelegate& InOnCompleted,
		const FShakeDelegate& InOnStopped);

	UFUNCTION(BlueprintCallable, Category = "USmartShakeComponent")
	void StopShake();

	UFUNCTION(BlueprintCallable, Category = "USmartShakeComponent")
	bool IsActivated();

	UFUNCTION(BlueprintCallable, Category = "USmartShakeComponent")
	void SetComponentToShake(USceneComponent* InSceneComponent);
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStartShakeOutputPin);

UCLASS()
class PROCEDURALTEST_API UAsyncStartShakeNode : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY()
	USmartShakeComponent* ShakeComponent;

	UPROPERTY(BlueprintAssignable)
	FStartShakeOutputPin Completed;

	UPROPERTY(BlueprintAssignable)
	FStartShakeOutputPin Stopped;

	UPROPERTY(BlueprintAssignable)
	FStartShakeOutputPin Restarted;

	UFUNCTION()
	void OnCompleted();

	UFUNCTION()
	void OnStopped();

	UFUNCTION()
	void OnRestarted();

	UFUNCTION(BlueprintCallable, Category = "SmartShakeComponent")
	static UAsyncStartShakeNode* StartShake(USmartShakeComponent* const& InShakeComponent);

protected:
	virtual void Activate() override;
};
