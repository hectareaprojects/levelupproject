// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "SmartSpawner/SmartSpawnerComponent.h"
#include "SimpleSpawnInfoPattern.generated.h"

UCLASS()
class PROCEDURALTEST_API USimpleSpawnInfoPattern: public USpawnInfoPattern
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="USimpleSpawnInfoPattern")
	TSubclassOf<AActor> ActorClass;

	virtual TArray<FSpawnInfo> GetSpawnInfo_Implementation();
};
