// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "SmartSpawnerComponent.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FSpawnInfo
{
	GENERATED_BODY()

	FSpawnInfo() = default;
	FSpawnInfo(TSubclassOf<AActor> InActorClass);
	FSpawnInfo(TSubclassOf<AActor> InActorClass, const FTransform& InActorTransform);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnInfo")
	TSubclassOf<AActor> ActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnInfo")
	FTransform ActorTransform;
};

UCLASS(Blueprintable, Abstract, EditInlineNew)
class PROCEDURALTEST_API USpawnInfoPattern : public UObject
{
	GENERATED_BODY()

	friend class USmartSpawnerComponent;

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "USpawnInfoPattern")
	TArray<FSpawnInfo> GetSpawnInfo();
};

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API USmartSpawnerComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "USmartSpawnerComponent")
	USpawnInfoPattern* SpawnInfo;

public:
	USmartSpawnerComponent();

	UFUNCTION(BlueprintCallable, Category = "USmartSpawnerComponent")
	TArray<AActor*> SpawnActors();

protected:
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;
	virtual FBoxSphereBounds	  CalcBounds(const FTransform& LocalToWorld) const override;
};
