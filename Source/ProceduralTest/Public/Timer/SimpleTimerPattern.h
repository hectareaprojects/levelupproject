// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Timer/SmartTimerComponent.h"
#include "Containers/Ticker.h"
#include "SimpleTimerPattern.generated.h"

UCLASS()
class PROCEDURALTEST_API USimpleTimerPattern : public UTimerPattern
{
	GENERATED_BODY()

private:
	FTimerHandle			   TimerHandle;
	FTSTicker::FDelegateHandle TickerHandle;
	bool					   bUsingTicker = false;
	bool					   bUsingTimer = false;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "USimpleTimerPattern", meta = (UIMin = 0.0f, ClampMin = 0.0f))
	float Time = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "USimpleTimerPattern")
	bool bLoop = true;

protected:
	virtual void InitTimer() override;
	virtual void ClearTimer() override;

public:
	/** Sets the time of the timer. It will be clamped to be greater or equal than 0. */
	UFUNCTION(BlueprintCallable, Category = "USimpleTimerPattern")
	void SetTime(float InTime);

	UFUNCTION(BlueprintCallable, Category = "USimpleTimerPattern")
	void SetLoop(bool bInLoop);

private:
	bool TickerNotify(float Delta);
	void ActivateTicker();
	void DeactivateTicker();
	void ActivateTimer();
	void DeactivateTimer();
};
