// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SmartTimerComponent.generated.h"

UCLASS(Abstract, EditInlineNew)
class PROCEDURALTEST_API UTimerPattern : public UObject
{
	GENERATED_BODY()

	friend class USmartTimerComponent;

private:
	UPROPERTY()
	USmartTimerComponent* SmartTimerComponent;

protected:
	USmartTimerComponent* GetTimerComponent() { return SmartTimerComponent; }
	void				  Notify();

	virtual void InitTimer() {}
	virtual void ClearTimer() {}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnNotify);

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API USmartTimerComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	bool bTimerInitialized = false;

public:
	UPROPERTY(EditAnywhere, Instanced, Category = "USmartTimerComponent")
	UTimerPattern* TimerPattern;

	UPROPERTY(BlueprintAssignable)
	FOnNotify OnNotify;

protected:
	virtual void OnRegister() override;	
	
public:
	UFUNCTION(BlueprintCallable, Category = "USmartTimerComponent")
	void InitializeTimer();

	UFUNCTION(BlueprintCallable, Category = "USmartTimerComponent")
	void ClearTimer();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "USmartTimerComponent")
	bool IsTimerInitialized() const;
};
