// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Trail/TrailComponent.h"
#include "SingleTrailPattern.generated.h"

UCLASS()
class PROCEDURALTEST_API USingleTrailPattern : public UTrailPattern
{
	GENERATED_BODY()

private:
	bool	bFirstLine = true;
	FVector LastLocation = FVector::ZeroVector;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USingleTrailPattern")
	FLinearColor TrailColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USingleTrailPattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float TrailThickness;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "USingleTrailPattern", meta = (UIMin = 0.f, ClampMin = 0.f))
	float Deviation;

public:
	virtual FBatchLineInfo CreateBatchLines() override;
};
