// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "TrailComponent.generated.h"

struct FLineInfo
{
	FVector		 Start;
	FVector		 End;
	FLinearColor Color;
	float		 Thickness;

	FLineInfo() = default;
	FLineInfo(const FVector& InStart, const FVector& InEnd, const FLinearColor& InColor, float InThickness)
		: Start(InStart), End(InEnd), Color(InColor), Thickness(InThickness) {}
};

struct FBatchLineInfo
{
	TArray<FLineInfo> Lines;
	void*			  UserData = nullptr;

	FBatchLineInfo() = default;
	FBatchLineInfo(const TArray<FLineInfo>& InLines)
		: Lines(InLines) {}
};

UCLASS(Abstract, Blueprintable, EditInlineNew)
class PROCEDURALTEST_API UTrailPattern : public UObject
{
	GENERATED_BODY()

	friend class UTrailComponent;

private:
	UPROPERTY()
	UTrailComponent* TrailComponentOwner;

protected:
	UTrailComponent* GetTrailComponentOwner() { return TrailComponentOwner; }

public:
	virtual FBatchLineInfo CreateBatchLines() { return FBatchLineInfo(); }
	virtual void		   UpdateBatchLines(FBatchLineInfo& BatchLines) {}
	virtual void		   DestroyBatchLines(FBatchLineInfo& BatchLines) {}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTrailEnded);

UCLASS(meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UTrailComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

private:
	TArray<FBatchLineInfo> Lines;
	int					   NextIndex = 0;
	int					   CurrentLength = 0;
	FTimerHandle		   TimerHandle;
	bool				   bIsTrailActivated = false;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UTrailComponent")
	bool bStartActivated = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UTrailComponent", meta = (UIMin = 0.01f, ClampMin = 0.01f))
	float GenerationTime = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UTrailComponent", meta = (UIMin = 1, ClampMin = 1))
	int TrailLength = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced, Category = "UTrailComponent")
	UTrailPattern* TrailPattern;

	UPROPERTY(BlueprintAssignable)
	FOnTrailEnded OnTrailEnded;

public:
	UFUNCTION(BlueprintCallable, Category = "UTrailComponent")
	void ActivateTrail();

	UFUNCTION(BlueprintCallable, Category = "UTrailComponent")
	void DeactivateTrail();

	UFUNCTION(BlueprintCallable, Category = "UTrailComponent")
	bool IsTrailActive();

protected:
	virtual void				  OnRegister() override;
	virtual void				  BeginPlay() override;
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;
	virtual FBoxSphereBounds	  CalcBounds(const FTransform& LocalToWorld) const override;

private:
	void UpdateLines();
};
