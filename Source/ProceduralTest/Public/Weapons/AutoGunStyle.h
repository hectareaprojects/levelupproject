// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Weapons/GunStyle.h"
#include "AutoGunStyle.generated.h"

UCLASS()
class PROCEDURALTEST_API UAutoGunStyle : public UGunStyle
{
	GENERATED_BODY()

private:
	/**
	* @brief A timer handle
	*/
	UPROPERTY()
	FTimerHandle TimerHandle;	
	
public:
	/**
	* @brief The fire rate of the gun
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AutoGunStyle")
	float FireRate = 2.f;
	
protected:
	/** Press the trigger of the gun. Should notify shoots. See NotifyShoot */
	virtual void PressTriggerImpl() override;

	/** Release the trigger of the gun. Should notify shoots. See NotifyShoot */
	virtual void ReleaseTriggerImpl() override;
};
