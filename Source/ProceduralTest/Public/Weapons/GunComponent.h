// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GunComponent.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FHitInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	AActor* ActorHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	class UPrimitiveComponent* ComponentHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	FVector LocationHit;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHit, const FHitInfo&, HitInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShoot);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UGunComponent : public UActorComponent
{
	GENERATED_BODY()

	friend class UShootStyle;
	friend class UGunStyle;

public:
	/**
	 * @brief The source of the ray cast
	 */
	UPROPERTY(BlueprintReadWrite, Category = "RayGunComponent")
	class USceneComponent* ShootSource;

	/**
	 * @brief The class of the shoot style
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunComponent")
	TSubclassOf<class UShootStyle> ShootStyleClass;

	/**
	 * @brief The class of the gun style
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunComponent")
	TSubclassOf<class UGunStyle> GunStyleClass;

	/**
	 * @brief The shoot style
	 */
	UPROPERTY(BlueprintReadOnly, Category = "GunComponent")
	UShootStyle* ShootStyle;

	/**
	 * @brief The gun style
	 */
	UPROPERTY(BlueprintReadOnly, Category = "GunComponent")
	UGunStyle* GunStyle;

private:
	/**
	 * @brief Is the trigger pressed
	 */
	UPROPERTY()
	bool bTriggerPressed;

protected:
	/**
	 * @brief The BeginPlay method
	 */
	virtual void BeginPlay() override;

private:
	void Shoot();

public:
	/**
	 * @brief Starts shooting
	 */
	UFUNCTION(BlueprintCallable, Category = "GunComponent")
	void PressTrigger();

	/**
	 * @brief Stops shooting
	 */
	UFUNCTION(BlueprintCallable, Category = "GunComponent")
	void ReleaseTrigger();

	/**
	 * @brief Checks whether the trigger is pressed
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GunComponent")
	bool IsTriggerPressed() const;

public:
	/**
	 * @brief Delegate called when an actor is hit.
	 */
	UPROPERTY(BlueprintAssignable, Category = "GunComponent")
	FOnHit OnHit;

	/**
	 * @brief Delegate called when shoot
	 */
	UPROPERTY(BlueprintAssignable, Category = "GunComponent")
	FOnShoot OnShoot;
};
