// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GunStyle.generated.h"

UCLASS(Abstract, Blueprintable, BlueprintType)
class PROCEDURALTEST_API UGunStyle : public UObject
{
	GENERATED_BODY()

	friend class UGunComponent;

protected:
	/** Assigned by the GunComponent itself */	
	UGunComponent* GunComponentOwner;

protected:
	/** Notifies the gun should shoot */
	void NotifyShoot();

	/** Initialize the shoot style object */
	virtual void StartGunStyle() {}

	/** Press the trigger of the gun. Should notify shoots. See NotifyShoot */
	virtual void PressTriggerImpl() {}

	/** Release the trigger of the gun. Should notify shoots. See NotifyShoot */
	virtual void ReleaseTriggerImpl() {}
};
