// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Weapons/ShootStyle.h"
#include "RayShootStyle.generated.h"

UCLASS()
class PROCEDURALTEST_API URayShootStyle : public UShootStyle
{
	GENERATED_BODY()
	
public:
	/**
	* @brief The length of the ray
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="RayShootStyle")
	float RayLength;
	
	/**
	* @brief Angle dispersion of the ray
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="RayShootStyle")
	float RayAngleDispersion;	
	
protected:
	virtual void ShootImpl(const FVector& Source, const FVector& Direction) override;
};
