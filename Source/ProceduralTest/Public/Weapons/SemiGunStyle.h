// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Weapons/GunStyle.h"
#include "SemiGunStyle.generated.h"

UCLASS()
class PROCEDURALTEST_API USemiGunStyle : public UGunStyle
{
	GENERATED_BODY()

protected:
	/** Press the trigger of the gun. Should notify shoots. See NotifyShoot */
	virtual void PressTriggerImpl() override;
};
