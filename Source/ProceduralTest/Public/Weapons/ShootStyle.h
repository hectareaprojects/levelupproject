// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "ShootStyle.generated.h"

UCLASS(Abstract, BlueprintType)
class PROCEDURALTEST_API UShootStyle : public UObject
{
	GENERATED_BODY()

	friend class UGunComponent;

protected:
	/** Assigned by GunComponent itself */	
	UGunComponent* GunComponentOwner;

protected:
	/** Notifies that an actor was hit */
	void NotifyHit(struct FHitInfo& HitInfo);

	/** Initialize the shoot style object */
	virtual void StartShootStyle() {}

	/** Implementation of a shoot. Should notify the hits. See NotifyHit */
	virtual void ShootImpl(const FVector& Source, const FVector& Direction) {}
};
