
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InterpolationFunction/InterpolationFunction.h"
#include "WeaponAnimComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTryingInteract);


/**
 * @brief Move and performs animations overwriting the relative position of the owner.
 *        This is not a good idea since the mesh can have already its own animation.
 *	      However, I'm not an Animator, so this is OK to muddle through.
*/
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALTEST_API UWeaponAnimComponent : public UActorComponent {

	GENERATED_BODY()

private:

	/**
	 * @brief The shooting function animation.
	*/
	FInterpolationFunction<UWeaponAnimComponent> ShootingAnim;

	/**
	 * @brief The animation to begin aiming.
	*/
	FInterpolationFunction<UWeaponAnimComponent> ToAimingAnim;

public:

	/**
	 * @brief The hip fire offset of the owner.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta=(AllowPrivateAccess=true))
		FVector HipFireOffset;

	/**
	 * @brief The aiming fire offset of the owner.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		FVector AimingFireOffset;

	/**
	 * @brief
	*/
	UPROPERTY(BlueprintAssignable, Category= "Weapons")
	FOnTryingInteract OnTryingInteract;

private:

	/**
	 * @brief The current offset between HipFireOffset and AimingFireOffset
	*/
	UPROPERTY()
		FVector TrailOffset;

	/**
	 * @brief The current offset due to a shoot.
	*/
	UPROPERTY()
		FVector ShootingOffset;

public:

	/**
	 * @brief Constructor.
	*/
	UWeaponAnimComponent();

protected:

	virtual void BeginPlay() override;

	/**
	 * @brief The Tick function.
	*/
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* THisTickFunction);
	

private:

	/**
	 * @brief The weapon animation.
	*/
	UFUNCTION()
		void ShootingAnimation(float CurrentTime);

	/**
	 * @brief The animation of starting to aim.
	*/
	UFUNCTION()
		void ToAimingAnimation(float CurrentTime);

public:

	UFUNCTION(BlueprintCallable, Category = "Weapons")
	void SetIsEnabled(bool bEnabled);

	UFUNCTION()
	void SetHipFireOffset(FVector InHipFireOffset);

	UFUNCTION()
	void SetAimingFireOffset(FVector InAimingFireOffset);

	/**
	 * @brief Triggers the Shooting animation.
	*/
	UFUNCTION(BlueprintCallable, Category = "Weapons")
		void TriggerShootingAnimation();

	/**
	 * @brief Triggers the ToAiming animation.
	*/
	UFUNCTION(BlueprintCallable, Category = "Weapons")
		void TriggerToAimingAnimation();

	/**
	 * @brief Triggers the ToHip animation.
	*/
	UFUNCTION(BlueprintCallable, Category = "Weapons")
		void TriggerToHipAnimation();

};