#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WeaponInterface.generated.h"


//struct Prueba {};

/**
 * @brief
*/
UINTERFACE(Blueprintable)
class PROCEDURALTEST_API UWeaponInterface : public UInterface {

    GENERATED_BODY()

};

class PROCEDURALTEST_API IWeaponInterface : public IInterface/*, Prueba*/ {

    GENERATED_BODY()

public:

    /**
     * @brief Press the trigger of a weapon.
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WeaponInterface")
    void PressTrigger();

    /**
     * @brief
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WeaponInterface")
    void ReleaseTrigger();


};