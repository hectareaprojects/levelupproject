// TODO: Copyright

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/World.h"
#include "WorldUtilsLibrary.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FDynamicDelegateHandle
{
	GENERATED_BODY()

	FDelegateHandle DelegateHandle;
};

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnWorldActorSpawned, AActor*, SpawnedActor);

UCLASS()
class PROCEDURALTEST_API UWorldUtilsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	/**
	 * @brief Adds a delegate that will be called when an actor spawns
	 */
	UFUNCTION(BlueprintCallable, Category = "WorldUtilsLibrary", meta = (WorldContext = "WorldContext"))
	static FDynamicDelegateHandle AddOnActorSpawned(UObject* WorldContext, const FOnWorldActorSpawned& OnActorSpawned);

	/**
	 * @brief Removes a delegate
	 */
	UFUNCTION(BlueprintCallable, Category = "WorldUtilsLibrary", meta = (WorldContext = "WorldContext"))
	static void RemoveOnActorSpawned(UObject* WorldContext, const FDynamicDelegateHandle& DelegateHandle);
};
