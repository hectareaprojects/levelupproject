
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "WrapperValues.generated.h"

USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FInteger
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WrapperValues")
	int Value;

	int* pValue = &Value;

	void Set(int _Value) const
	{
		*const_cast<int*>(pValue) = _Value;
	}
};


USTRUCT(BlueprintType)
struct PROCEDURALTEST_API FBool
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WrapperValues")
	bool Value;

	bool* pValue = &Value;

	void Set(bool _Value) const
	{
		*const_cast<bool*>(pValue) = _Value;
	}
};


UCLASS()
class PROCEDURALTEST_API UWrapperValuesLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "WrapperValues")
	static void SetInteger(const FInteger& Integer, int Value)
	{
		Integer.Set(Value);
	}

	UFUNCTION(BlueprintCallable, Category = "WrapperValues")
	static void SetBool(const FBool& Bool, bool Value)
	{
		Bool.Set(Value);
	}
};
