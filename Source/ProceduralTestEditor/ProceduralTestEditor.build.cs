using UnrealBuildTool;

public class ProceduralTestEditor : ModuleRules
{
	public ProceduralTestEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "ProceduralTest" });
	}
}
